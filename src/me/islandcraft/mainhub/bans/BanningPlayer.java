package me.islandcraft.mainhub.bans;

import me.islandcraft.mainhub.managers.BanManager;
import me.islandcraft.mainhub.managers.TimeManager;

import org.bukkit.entity.Player;

public class BanningPlayer {
	private final Player p;
	private String banning;
	private String reason;
	private String time = "noTime";
	private BanType type;

	public BanningPlayer(Player p) {
		this.p = p;
	}

	public void setBanning(String banning) {
		this.banning = banning;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void setType(BanType type) {
		this.type = type;
	}

	public String getBanning() {
		return this.banning;
	}
	
	public String getReason(){
		return this.reason;
	}

	public boolean ban() {
		Time t = !this.time.equalsIgnoreCase("noTime") ? TimeManager
				.getFinalTime(this.time) : null;
		return BanManager.ban(this.type, this.banning, t, this.reason,
				this.p.getName());
	}

}
