package me.islandcraft.mainhub.bans;

import java.util.HashMap;

import com.google.common.collect.Maps;

public class Time {
	private final int second;
	private final int minute;
	private final int hour;
	private final int day;
	private final int month;
	private final int year;

	public Time(int second, int minute, int hour, int day, int month, int year) {
		this.second = second;
		this.minute = minute;
		this.hour = hour;
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getSecond() {
		return this.second;
	}

	public int getMinute() {
		return this.minute;
	}

	public int getHour() {
		return this.hour;
	}

	public int getDay() {
		return this.day;
	}

	public int getMonth() {
		return this.month;
	}

	public int getYear() {
		return this.year;
	}

	public boolean isBefore(Time time) {
		int second = time.getSecond();
		int minute = time.getMinute();
		int hour = time.getHour();
		int day = time.getDay();
		int month = time.getMonth();
		int year = time.getYear();
		System.out.println("Unban ; Real");
		System.out.println(this.year + " ; " + year);
		System.out.println(this.month + " ; " + month);
		System.out.println(this.day + " ; " + day);
		System.out.println(this.hour + " ; " + hour);
		System.out.println(this.minute + " ; " + minute);
		System.out.println(this.second + " ; " + second);
		if (this.year < year) {
			return true;
		} else if (this.year > year) {
			return false;
		}
		if (this.month < month) {
			return true;
		} else if (this.month > month) {
			return false;
		}
		if (this.day < day) {
			return true;
		} else if (this.day > day) {
			return false;
		}
		if (this.hour < hour) {
			return true;
		} else if (this.hour > hour) {
			return false;
		}
		if (this.minute < minute) {
			return true;
		} else if (this.minute > minute) {
			return false;
		}
		if (this.second < second) {
			return true;
		} else if (this.second > second) {
			return false;
		}
		return false;
	}

	public String toString() {
		return second + "," + minute + "," + hour + "," + day + "," + month
				+ "," + year;
	}

	public String toMessage() {
		HashMap<Integer, String> months = Maps.newHashMap();
		months.put(0, "janeiro");
		months.put(1, "fevreiro");
		months.put(2, "mar�o");
		months.put(3, "abril");
		months.put(4, "maio");
		months.put(5, "junho");
		months.put(6, "julho");
		months.put(7, "agosto");
		months.put(8, "setembro");
		months.put(9, "outubro");
		months.put(10, "novembro");
		months.put(11, "dezembro");

		return "dia " + day + " de " + months.get(month) + " de " + year
				+ ", �s " + hour + ":" + minute + ":" + second;
	}

}
