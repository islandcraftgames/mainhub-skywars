package me.islandcraft.mainhub.bans;

public enum Reasons {
	KILLAURA("1y", BanType.BAN, "kill aura/force field"), FLY("1y",
			BanType.BAN, "fly"), AUTO_SOUP("1y", BanType.BAN, "auto soup"), SPEED(
			"1y", BanType.BAN, "speed"), SPIDER("1y", BanType.BAN, "spider"), NUKE(
			"1y", BanType.BAN, "nuke"), OTHER_HACKS("1y", BanType.BAN,
			"inappropriated hacks/mods"), SPAMMING("7d", BanType.MUTE, "spamming"), BLOCKGLITCHING(
			"7d", BanType.BAN, "block glitching"), ADVERTISING("1y",
			BanType.MUTE, "advertising"), TPASPAMMING("7d", BanType.BAN,
			"tpa spamming"), INA_LANG("7d", BanType.MUTE,
			"inappropriate language/insults"), OFENCIVEUSERNAME("30d", BanType.BAN,
			"offensive username"), FAKEMESSAGES("3d", BanType.MUTE,
			"fake messages"), ABUSINGSTAFF("7d", BanType.MUTE, "abusing staff"), SCAMMING(
			"7d", BanType.BAN, "scamming"), GRIEFING("30d", BanType.BAN,
			"griefing"), DEATHTRAP("7d", BanType.BAN, "death trap"), LAGSERVER(
			"5d", BanType.BAN, "lag server"), OTHER("1y", BanType.BAN, "inappropriated behavior"), ABUSINGOP(
			"1d", BanType.BAN, "abusing op");

	private String duration;
	private BanType type;
	private String string;

	private Reasons(String duration, BanType type, String name) {
		this.duration = duration;
		this.type = type;
		this.string = name;
	}

	public String getDuration() {
		return this.duration;
	}

	public BanType getType() {
		return this.type;
	}

	public static Reasons getReason(String id) {
		for (Reasons r : Reasons.values()) {
			if (r.toString().equalsIgnoreCase(id)) {
				return r;
			}
		}
		return OTHER;
	}
	
	public static Reasons getReasonNull(String id) {
		for (Reasons r : Reasons.values()) {
			if (r.toString().equalsIgnoreCase(id)) {
				return r;
			}
		}
		return null;
	}
	
	public String toString(){
		return this.string;
	}

}
