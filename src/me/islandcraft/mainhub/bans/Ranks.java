package me.islandcraft.mainhub.bans;

public abstract class Ranks{
	 public static final int OWNER = 5;
	    public static final int ADMIN = 4;
	    public static final int BUILDER = 3;
	    public static final int MODERATOR = 2;
	    public static final int HELPER = 1;

}
