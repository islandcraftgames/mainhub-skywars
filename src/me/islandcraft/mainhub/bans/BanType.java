package me.islandcraft.mainhub.bans;

public enum BanType {

	BAN, MUTE, NULL;

	public static BanType getType(String id) {
		if (id == null)
			return NULL;

		for (BanType r : BanType.values()) {
			if (r.toString().equalsIgnoreCase(id)) {
				return r;
			}
		}
		return NULL;
	}

}
