package me.islandcraft.mainhub.commands;

import me.islandcraft.mainhub.language.LanguageMain;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ReloadLangCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		if (s.hasPermission("admin"))
			LanguageMain.resetup();
		return true;
	}

}