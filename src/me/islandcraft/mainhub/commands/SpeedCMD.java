package me.islandcraft.mainhub.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpeedCMD implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage("Only Players...");
		}

		Player p = (Player) s;

		if (!p.hasPermission("main.speed"))
			return false;
		
		if (p.isFlying()) {
			p.setFlySpeed(getRealMoveSpeed(getMoveSpeed(args[0]), true));
			System.out.println("Player " + p.getName() + " changed fly speed to " + args[0]);
		} else {
			p.setWalkSpeed(getRealMoveSpeed(getMoveSpeed(args[0]), false));
			System.out.println("Player " + p.getName() + " changed walk speed to " + args[0]);
		}

		return false;
	}

	private float getMoveSpeed(String moveSpeed) {
		float userSpeed;
		try {
			userSpeed = Float.parseFloat(moveSpeed);
			if (userSpeed > 10.0F) {
				userSpeed = 10.0F;
			} else if (userSpeed < 1.0E-004F) {
				userSpeed = 1.0E-004F;
			}
		} catch (NumberFormatException e) {
			return 0;
		}
		return userSpeed;
	}

	private float getRealMoveSpeed(float userSpeed, boolean isFly) {
		float defaultSpeed = isFly ? 0.1F : 0.2F;
		float maxSpeed = 1.0F;
		if (userSpeed < 1.0F) {
			return defaultSpeed * userSpeed;
		}
		float ratio = (userSpeed - 1.0F) / 9.0F * (maxSpeed - defaultSpeed);
		return ratio + defaultSpeed;
	}

}
