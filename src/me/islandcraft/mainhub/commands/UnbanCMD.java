package me.islandcraft.mainhub.commands;

import me.islandcraft.mainhub.bans.BanType;
import me.islandcraft.mainhub.managers.FileManager;
import me.islandcraft.mainhub.player.IslandPlayer;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class UnbanCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		if (s.hasPermission("main.ban")) {
			if (args.length > 0) {
				String p = args[0];
				if (FileManager.getPlayerFile(p) == null) {
					s.sendMessage(ChatColor.RED
							+ "That player has never played on IslandCraft!");
					return false;
				}
				IslandPlayer ip = new IslandPlayer(p);
				if (ip.getBanType() == BanType.NULL) {
					s.sendMessage(ChatColor.RED
							+ "That player isn't banned or muted!");
				}
				ip.setBanType(BanType.NULL);
				ip.setBanner(null);
				ip.setExpires(null);
				ip.setReason("");
				ip.save();
				s.sendMessage(ChatColor.GREEN + "Player '" + p
						+ "' unbanned/unmuted with success!");
			} else {
				s.sendMessage(ChatColor.RED + "Use: /unban <player>");
			}
		} else {
			s.sendMessage(ChatColor.RED + "No permission");
		}
		return false;
	}

}
