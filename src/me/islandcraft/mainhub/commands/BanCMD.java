package me.islandcraft.mainhub.commands;

import java.util.HashMap;
import java.util.List;

import me.islandcraft.mainhub.bans.BanType;
import me.islandcraft.mainhub.bans.BanningPlayer;
import me.islandcraft.mainhub.bans.Reasons;
import me.islandcraft.mainhub.events.PlayerSpeakEvent;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.managers.BanManager;
import me.islandcraft.mainhub.managers.TimeManager;
import me.islandcraft.mainhub.player.IslandPlayer;
import me.islandcraft.mainhub.player.PlayerManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class BanCMD implements CommandExecutor, Listener {

	private static HashMap<Player, BanningPlayer> banning = Maps.newHashMap();
	private static List<Player> typingReason = Lists.newArrayList();
	private static List<Player> typingTime = Lists.newArrayList();

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		if (!(s instanceof Player)) {
			if (s.hasPermission("main.ban")) {
				if (args.length > 1) {
					String reason = "";
					for (int i = 1; i < args.length; i++) {
						reason += args[i] + " ";
					}
					if (BanManager.ban(BanType.BAN, args[0], null, reason,
							"Rexcantor64")) {
						s.sendMessage(ChatColor.GREEN + "Player " + args[0]
								+ " banned with success!");
					} else {
						s.sendMessage(ChatColor.RED
								+ "That player has never played on IslandCraft!");
					}

				} else {
					s.sendMessage(ChatColor.RED + "Use: /ban <player> <reason>");
				}
			} else {
				s.sendMessage(ChatColor.RED + "No permission!");
			}
			return true;
		} else {
			if (s.hasPermission("main.ban")) {
				if (args.length > 0) {
					Player p = (Player) s;
					if (p.getName().equalsIgnoreCase(args[0])) {
						p.sendMessage(ChatColor.RED + "You can't ban yourself!");
						return false;
					}
					banning.put(p, new BanningPlayer(p));
					banning.get(p).setBanning(args[0]);
					openReasonInventory(p);
				} else {
					s.sendMessage(ChatColor.RED + "Use: /ban <player>");
					return false;
				}
			} else {
				s.sendMessage(ChatColor.RED + "No permission!");
				return false;
			}
			return true;
		}
	}

	/*** REASON ***/

	private static void openReasonInventory(Player p) {
		Inventory inv = Bukkit.createInventory(null, 54,
				"Ban: " + banning.get(p).getBanning());
		int i = 0;
		for (Reasons r : Reasons.values()) {
			ItemStack item = new ItemStack(Material.WOOL);
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(r.toString());
			item.setItemMeta(itemMeta);
			if (r.getType() == BanType.BAN) {
				item.setDurability((short) 14);
			} else if (r.getType() == BanType.MUTE) {
				item.setDurability((short) 5);
			}
			inv.setItem(i++, item);
		}
		ItemStack item = new ItemStack(Material.WOOL);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName("Custom");
		item.setItemMeta(itemMeta);
		item.setDurability((short) 4);
		inv.setItem(53, item);
		p.openInventory(inv);
	}

	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		if (!e.getInventory().getName().startsWith("Ban: ")) {
			return;
		}
		e.setCancelled(true);

		Player p = (Player) e.getWhoClicked();

		if (e.getCurrentItem() == null) {
			return;
		}

		if (e.getCurrentItem().getType() != Material.WOOL) {
			return;
		}

		if (!e.getCurrentItem().hasItemMeta()) {
			return;
		}

		if (e.getCurrentItem().getItemMeta().getDisplayName() == null) {
			return;
		}

		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equalsIgnoreCase("Custom")) {
			p.sendMessage("Please enter your reason:");
			typingReason.add(p);
			p.closeInventory();
			return;
		}

		for (Reasons r : Reasons.values()) {
			if (r.toString().equalsIgnoreCase(
					e.getCurrentItem().getItemMeta().getDisplayName())) {
				BanningPlayer bp = banning.get(p);
				bp.setReason(r.toString());
				bp.setTime(r.getDuration());
				bp.setType(r.getType());
				if (bp.ban()) {
					p.sendMessage(ChatColor.GREEN + "Player " + bp.getBanning()
							+ " banned for \"" + r.toString()
							+ "\" with success!");
				} else {
					e.getWhoClicked()
							.sendMessage(
									ChatColor.RED
											+ "This player has never played on IslandCraft!");
				}
				p.closeInventory();
			}
		}
	}

	/*** TIME ***/

	private static void openTempInventory(Player p) {
		Inventory inv = Bukkit.createInventory(null, 54, "Time: "
				+ banning.get(p).getBanning());
		String[] s = { "1h", "12h", "1d", "3d", "5d", "7d", "14d", "1mo",
				"2mo", "1y" };
		int i = 0;
		for (String a : s) {
			ItemStack item = new ItemStack(Material.WOOL);
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(a);
			item.setItemMeta(itemMeta);
			item.setDurability((short) 5);
			inv.setItem(i++, item);
		}
		ItemStack item = new ItemStack(Material.WOOL);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName("Custom");
		item.setItemMeta(itemMeta);
		item.setDurability((short) 4);
		inv.setItem(53, item);
		ItemStack item2 = new ItemStack(Material.WOOL);
		ItemMeta itemMeta2 = item2.getItemMeta();
		itemMeta2.setDisplayName("Permanent");
		item2.setItemMeta(itemMeta2);
		item2.setDurability((short) 14);
		inv.setItem(52, item2);
		p.openInventory(inv);
	}

	@EventHandler
	public void onInvClickTime(InventoryClickEvent e) {
		if (!e.getInventory().getName().startsWith("Time: ")) {
			return;
		}
		e.setCancelled(true);

		Player p = (Player) e.getWhoClicked();
		BanningPlayer bp = banning.get(p);

		if (e.getCurrentItem() == null) {
			return;
		}

		if (e.getCurrentItem().getType() != Material.WOOL) {
			return;
		}

		if (!e.getCurrentItem().hasItemMeta()) {
			return;
		}

		if (e.getCurrentItem().getItemMeta().getDisplayName() == null) {
			return;
		}

		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equalsIgnoreCase("Custom")) {
			p.sendMessage("Please enter your time (s/m/h/d/mo/u):");
			typingTime.add(p);
			p.closeInventory();
			return;
		}

		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equalsIgnoreCase("Permanent")) {
			e.getWhoClicked().closeInventory();
			p.closeInventory();
			openTypeInventory(p);
			return;
		}

		String[] s = { "1h", "12h", "1d", "3d", "5d", "7d", "14d", "1mo",
				"2mo", "1y" };

		for (String a : s) {
			if (a.equalsIgnoreCase(e.getCurrentItem().getItemMeta()
					.getDisplayName())) {
				bp.setTime(a);
				p.closeInventory();
				openTypeInventory(p);
				break;
			}
		}
	}

	/*** TYPE ***/

	private static void openTypeInventory(Player p) {
		Inventory inv = Bukkit.createInventory(null, 9,
				"Type: " + banning.get(p).getBanning());
		ItemStack ban = new ItemStack(Material.WOOL);
		ItemMeta banMeta = ban.getItemMeta();
		banMeta.setDisplayName("Ban");
		ban.setItemMeta(banMeta);
		ban.setDurability((short) 14);
		ItemStack mute = new ItemStack(Material.WOOL);
		ItemMeta muteMeta = mute.getItemMeta();
		muteMeta.setDisplayName("Mute");
		mute.setItemMeta(muteMeta);
		mute.setDurability((short) 5);
		inv.setItem(1, ban);
		inv.setItem(7, mute);
		p.openInventory(inv);
	}

	@EventHandler
	public void onTypeClick(InventoryClickEvent e) {
		if (!e.getInventory().getName().startsWith("Type: ")) {
			return;
		}
		e.setCancelled(true);

		Player p = (Player) e.getWhoClicked();
		BanningPlayer bp = banning.get(p);

		if (e.getSlot() == 1) {

			bp.setType(BanType.BAN);

			if (bp.ban()) {
				p.sendMessage(ChatColor.GREEN + "Player " + banning
						+ " banned for \"" + bp.getReason()
						+ "\" with success!");
			} else {
				e.getWhoClicked()
						.sendMessage(
								ChatColor.RED
										+ "This player has never played on IslandCraft!");
			}
			p.closeInventory();
		}

		if (e.getSlot() == 7) {

			bp.setType(BanType.MUTE);

			if (bp.ban()) {
				p.sendMessage(ChatColor.GREEN + "Player " + bp.getBanning()
						+ " banned for \"" + bp.getReason()
						+ "\" with success!");
			} else {
				e.getWhoClicked()
						.sendMessage(
								ChatColor.RED
										+ "This player has never played on IslandCraft!");
			}
			p.closeInventory();
		}
	}

	/*** CHAT ***/

	@EventHandler
	public void onReasonType(AsyncPlayerChatEvent e) {
		if (typingReason.contains(e.getPlayer())
				&& !typingTime.contains(e.getPlayer())) {
			Player p = e.getPlayer();
			typingReason.remove(p);

			e.setCancelled(true);

			BanningPlayer bp = banning.get(p);
			bp.setReason(e.getMessage());

			openTempInventory(p);
		} else if (typingTime.contains(e.getPlayer())) {
			Player p = e.getPlayer();
			typingTime.remove(p);

			e.setCancelled(true);

			BanningPlayer bp = banning.get(p);
			bp.setTime(e.getMessage());

			openTypeInventory(p);

			return;
		}
	}

	@EventHandler
	public void onMutedPlayerSpeak(PlayerSpeakEvent e) {
		IslandPlayer ip = PlayerManager.get(e.getPlayer());
		if (ip.getBanType() == BanType.MUTE) {
			e.setCancelled(true);
			if (ip.getExpires() == null) {
				e.getPlayer().sendMessage(
						LanguageMain.get(e.getPlayer(), "main.ban.mute.perm",
								new String[] { ip.getExpires().toMessage() }));
			} else {
				if (ip.getExpires().isBefore(
						TimeManager.fromGregorianToTime(TimeManager
								.getCurrentTime()))) {
					ip.setBanType(BanType.NULL);
					ip.setExpires(null);
					ip.setReason("");
					return;
				}
				e.getPlayer().sendMessage(
						LanguageMain.get(
								e.getPlayer(),
								"main.ban.mute",
								new String[] { ip.getExpires().toMessage(),
										ip.getReason() }));
			}
		}
	}

}
