package me.islandcraft.mainhub.commands;

import me.islandcraft.mainhub.language.LanguageMain;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class KickCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		if (s.hasPermission("main.ban")) {
			if (args.length > 1) {
				if (Bukkit.getPlayer(args[0]) != null) {
					String reason = "";
					for (int i = 1; i < args.length; i++) {
						reason += args[i] + " ";
					}
					Bukkit.getPlayer(args[0]).kickPlayer(
							LanguageMain.get(Bukkit.getPlayer(args[0]),
									"main.ban.kick", new String[] { reason }));
				} else {
					s.sendMessage("'" + args[0] + "' is not online!");
				}
			} else {
				s.sendMessage(ChatColor.RED + "Use: /kick <player> <reason>");
			}
		} else {
			s.sendMessage(ChatColor.RED + "No permission!");
		}
		return true;
	}

}
