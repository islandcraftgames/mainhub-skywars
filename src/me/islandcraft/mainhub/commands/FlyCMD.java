package me.islandcraft.mainhub.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (!(s instanceof Player)) {
			return false;
		}

		Player p = (Player) s;

		if (!p.hasPermission("vip")) {
			return false;
		}
		
		p.setAllowFlight(!p.getAllowFlight());
		
		p.sendMessage(ChatColor.GREEN + "�lFly: " + ChatColor.GOLD + p.getAllowFlight());

		return false;
	}

}
