package me.islandcraft.mainhub.commands;

import java.io.File;

import me.islandcraft.mainhub.Main;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;

public class VoteCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		File f = new File(Main.get().getDataFolder(), "vote.yml");
		YamlConfiguration config = YamlConfiguration.loadConfiguration(f);
		s.sendMessage(ChatColor.GREEN + "----------VOTE----------");
		for(String st : config.getStringList("links")){
			s.sendMessage(ChatColor.translateAlternateColorCodes('&', st));
		}
		s.sendMessage(ChatColor.GREEN + "----------VOTE----------");
		return false;
	}

}