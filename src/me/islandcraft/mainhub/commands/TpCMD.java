package me.islandcraft.mainhub.commands;

import me.islandcraft.mainhub.language.LanguageMain;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (s instanceof Player) {
			Player p = (Player) s;
			if (!p.hasPermission("main.admin")) {
				p.sendMessage(ChatColor.RED + LanguageMain.get(p, "sw.noperm"));
				return false;
			}
			if (args.length == 0) {
				p.sendMessage(ChatColor.RED + "/tp <player> [player]");
				return false;
			} else if (args.length == 1) {
				Player p2 = Bukkit.getPlayer(args[0]);
				if(p2 == null){
					//tell notonline
					return false;
				}
				if (TptoogleCMD.tpt.get(p2) == true
						|| TptoogleCMD.tpt.get(p2) == null) {
					p.teleport(p2);
					//tell succefully tp
					return true;
				}else{
					//tell tp disabled
					return false;
				}
			} else {

			}
			return true;
		}

		return false;
	}

}
