package me.islandcraft.mainhub.commands;

import java.io.File;

import me.islandcraft.mainhub.managers.FileManager;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class PstatsCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (!s.hasPermission("main.stats"))
			return false;

		if (args.length == 0) {
			s.sendMessage("/stats <player>");
			return false;
		}

		String p = args[0];

		File f = FileManager.getPlayerFile(p);

		if (f == null) {
			s.sendMessage("That player has never played on IslandCraftGames!");
			return false;
		}

		YamlConfiguration config = YamlConfiguration.loadConfiguration(f);

		String name = config.getString("name");
		boolean online = isOnline(p);
		int xp = config.getInt("xp");
		double money = config.getDouble("money");
		String banned = config.getString("ban.type");
		String expires = config.getString("ban.expires");
		String reason = config.getString("ban.reason");
		String banner = config.getString("ban.banner");
		int swW = config.getInt("skywars.wins");
		int swP = config.getInt("skywars.played");
		int swD = config.getInt("skywars.deaths");
		int swK = config.getInt("skywars.kills");
		boolean[] swKits = { config.getBoolean("skywars.kits.1"),
				config.getBoolean("skywars.kits.2"),
				config.getBoolean("skywars.kits.3"),
				config.getBoolean("skywars.kits.4"),
				config.getBoolean("skywars.kits.5"),
				config.getBoolean("skywars.kits.6"),
				config.getBoolean("skywars.kits.7"),
				config.getBoolean("skywars.kits.8"),
				config.getBoolean("skywars.kits.9"),
				config.getBoolean("skywars.kits.10"),
				config.getBoolean("skywars.kits.11") };
		int ctfW = config.getInt("capturetheflag.wins");
		int ctfP = config.getInt("capturetheflag.played");
		int ctfD = config.getInt("capturetheflag.deaths");
		int ctfK = config.getInt("capturetheflag.kills");
		boolean[] ctfKits = { config.getBoolean("capturetheflag.k1"),
				config.getBoolean("capturetheflag.k2"),
				config.getBoolean("capturetheflag.k3"),
				config.getBoolean("capturetheflag.k4"), };
		int crW = config.getInt("cr.wins");
		int crP = config.getInt("cr.played");
		int crD = config.getInt("cr.deaths");
		int crK = config.getInt("cr.kills");
		int x1W = config.getInt("1v1.wins");
		int x1P = config.getInt("1v1.played");
		int x1D = config.getInt("1v1.deaths");
		int x1K = config.getInt("1v1.kills");
		
		s.sendMessage("---[" + name + "'s stats]---");
		s.sendMessage("Online: " + online);
		s.sendMessage("XP: " + xp);
		s.sendMessage("Money: " + money);
		s.sendMessage("Ban Type: " + banned);
		s.sendMessage("Expires: " + expires);
		s.sendMessage("Reason: " + reason);
		s.sendMessage("Banner: " + banner);
		s.sendMessage("---[SkyWars]---");
		s.sendMessage("Wins: " + swW);
		s.sendMessage("Played: " + swP);
		s.sendMessage("Deaths: " + swD);
		s.sendMessage("Kills: " + swK);
		s.sendMessage("Kits: " + swKits.toString());
		s.sendMessage("---[CaptureTheFlag]---");
		s.sendMessage("Wins: " + ctfW);
		s.sendMessage("Played: " + ctfP);
		s.sendMessage("Deaths: " + ctfD);
		s.sendMessage("Kills: " + ctfK);
		s.sendMessage("Kits: " + ctfKits.toString());
		s.sendMessage("---[CrazyRace]---");
		s.sendMessage("Wins: " + crW);
		s.sendMessage("Played: " + crP);
		s.sendMessage("Deaths: " + crD);
		s.sendMessage("Kills: " + crK);
		s.sendMessage("---[1v1]---");
		s.sendMessage("Wins: " + x1W);
		s.sendMessage("Played: " + x1P);
		s.sendMessage("Deaths: " + x1D);
		s.sendMessage("Kills: " + x1K);

		return false;
	}

	public boolean isOnline(String name) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.getName().equalsIgnoreCase(name))
				return true;
		}
		return false;
	}

}
