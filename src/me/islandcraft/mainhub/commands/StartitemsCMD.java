package me.islandcraft.mainhub.commands;

import me.islandcraft.mainhub.items.ItemsMain;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StartitemsCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage(ChatColor.RED + "Only players...");
		}
		Player p = (Player) s;
		ItemsMain.setStartItems(p, false);
		return false;
	}

}
