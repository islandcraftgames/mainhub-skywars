package me.islandcraft.mainhub.commands;

import me.islandcraft.mainhub.Main;
import me.islandcraft.mainhub.language.LanguageMain;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class SkylobbyCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage("Only players");
		}

		Player p = (Player) s;

		FileConfiguration storage = Main.get().getConfig();

		World world = Bukkit.getWorld(storage.getString("skylobby").split(", ")[3]);
		int x = Integer.parseInt(storage.getString("skylobby").split(", ")[0]);
		int y = Integer.parseInt(storage.getString("skylobby").split(", ")[1]);
		int z = Integer.parseInt(storage.getString("skylobby").split(", ")[2]);

		p.teleport(new Location(world, x + 0.5, y + 0.5, z + 0.5));
		p.sendMessage(LanguageMain.get(p, "main.tpsuccess", new String[] { "Sky Lobby" }));

		return false;
	}

}
