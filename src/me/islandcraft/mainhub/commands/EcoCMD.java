package me.islandcraft.mainhub.commands;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.skywars.SkyWars;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EcoCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		if (s instanceof Player) {
			Player p = (Player) s;
			Player p2 = p;
			String amount = "";
			if (!p.hasPermission("admin")) {
				p.sendMessage(ChatColor.RED + LanguageMain.get(p, "sw.noperm"));
				return false;
			}
			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("reset")) {
					if (args.length == 2) {
						if (Bukkit.getPlayer(args[1]) != null) {
							p2 = Bukkit.getPlayer(args[1]);
						} else {
							p.sendMessage(ChatColor.RED
									+ LanguageMain.get(p, "main.cmd.notonline",
											new String[] { args[1] }));
							return false;
						}
					}
					SkyWars.getEconomy().setMoney(p2.getName(), 0);
					p.sendMessage(ChatColor.GOLD
							+ LanguageMain.get(
									p,
									"main.cmd.ecosuccess",
									new String[] {
											p2.getDisplayName(),
											Double.toString(SkyWars
													.getEconomy().getBalance(
															p2.getName())) }));
					return true;
				} else if (args.length < 2) {
					p.sendMessage(ChatColor.GOLD
							+ "/eco <give/take/reset/set> [player] <amount>");
					return false;
				} else if (args.length > 2) {
					if (Bukkit.getPlayer(args[1]) != null) {
						p2 = Bukkit.getPlayer(args[1]);
						amount = args[2];
					} else {
						p.sendMessage(ChatColor.RED
								+ LanguageMain.get(p, "main.cmd.notonline",
										new String[] { args[1] }));
						return false;
					}
				} else {
					amount = args[1];
				}
			} else {
				p.sendMessage(ChatColor.GOLD
						+ "/eco <give/take/reset/set> [player] <amount>");
				return false;
			}

			if (args[0].equalsIgnoreCase("give")) {
				try {
					SkyWars.getEconomy().depositPlayer(p2.getName(),
							Double.parseDouble(amount));
					p.sendMessage(ChatColor.GOLD
							+ LanguageMain.get(
									p,
									"main.cmd.ecosuccess",
									new String[] {
											p2.getDisplayName(),
											Double.toString(SkyWars
													.getEconomy().getBalance(
															p2.getName())) }));
					return true;
				} catch (NumberFormatException e) {
					p.sendMessage(ChatColor.RED
							+ LanguageMain.get(p, "main.cmd.invalidarg") + ": "
							+ amount);
					return false;
				}
			} else if (args[0].equalsIgnoreCase("take")) {
				try {
					if (!SkyWars.getEconomy().withdrawPlayer(p2.getName(),
							Double.parseDouble(amount)))
						SkyWars.getEconomy().setMoney(p2.getName(), 0);
					p.sendMessage(ChatColor.GOLD
							+ LanguageMain.get(
									p,
									"main.cmd.ecosuccess",
									new String[] {
											p2.getDisplayName(),
											Double.toString(SkyWars
													.getEconomy().getBalance(
															p2.getName())) }));
				} catch (NumberFormatException e) {
					p.sendMessage(ChatColor.RED
							+ LanguageMain.get(p, "main.cmd.invalidarg") + ": "
							+ amount);
					return false;
				}
			} else if (args[0].equalsIgnoreCase("set")) {
				try {
					if (SkyWars.getEconomy().setMoney(p2.getName(),
							Double.parseDouble(amount))) {

						p.sendMessage(ChatColor.GOLD
								+ LanguageMain
										.get(p,
												"main.cmd.ecosuccess",
												new String[] {
														p2.getDisplayName(),
														Double.toString(SkyWars
																.getEconomy()
																.getBalance(
																		p2.getName())) }));
						return true;
					} else {
						p.sendMessage(ChatColor.RED
								+ LanguageMain.get(p, "main.cmd.invalidarg")
								+ ": " + amount);
						return false;
					}
				} catch (NumberFormatException e) {
					p.sendMessage(ChatColor.RED
							+ LanguageMain.get(p, "main.cmd.invalidarg") + ": "
							+ amount);
					return false;
				}
			} else {
				p.sendMessage(ChatColor.GOLD
						+ "/eco <give/take/reset/set> [player] <amount>");
				return false;
			}
		} else {
			Player p2 = null;

			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("reset")) {
					if (args.length == 2) {
						if (Bukkit.getPlayer(args[1]) != null) {
							p2 = Bukkit.getPlayer(args[1]);
						} else {
							s.sendMessage(ChatColor.RED + args[1]
									+ " is not online!");
							return false;
						}
						SkyWars.getEconomy().setMoney(p2.getName(), 0);
						s.sendMessage(ChatColor.GOLD + "Success! "
								+ p2.getDisplayName() + "'s money: "
								+ SkyWars.getEconomy().getBalance(p2.getName()));
						return true;
					} else {
						s.sendMessage(ChatColor.GOLD
								+ "/eco <give/take/reset/set> <player> <amount>");
					}
				} else if (args.length < 3) {
					s.sendMessage(ChatColor.GOLD
							+ "/eco <give/take/reset/set> <player> <amount>");
					return false;
				} else if (args.length > 2) {
					if (Bukkit.getPlayer(args[1]) != null) {
						p2 = Bukkit.getPlayer(args[1]);
					} else {
						s.sendMessage(ChatColor.RED + args[1]
								+ " is not online!");
						return false;
					}
				}
			} else {
				s.sendMessage(ChatColor.GOLD
						+ "/eco <give/take/reset/set> <player> <amount>");
				return false;
			}

			if (args[0].equalsIgnoreCase("give")) {
				try {
					SkyWars.getEconomy().depositPlayer(p2.getName(),
							Double.parseDouble(args[2]));
					s.sendMessage(ChatColor.GOLD + "Success! "
							+ p2.getDisplayName() + "'s money: "
							+ SkyWars.getEconomy().getBalance(p2.getName()));
					return true;
				} catch (NumberFormatException e) {
					s.sendMessage(ChatColor.RED + "Invalid Argument: "
							+ args[2]);
					return false;
				}
			} else if (args[2].equalsIgnoreCase("take")) {
				try {
					if (!SkyWars.getEconomy().withdrawPlayer(p2.getName(),
							Double.parseDouble(args[2])))
						SkyWars.getEconomy().setMoney(p2.getName(), 0);
					s.sendMessage(ChatColor.GOLD + "Success! "
							+ p2.getDisplayName() + "'s money: "
							+ SkyWars.getEconomy().getBalance(p2.getName()));
				} catch (NumberFormatException e) {
					s.sendMessage(ChatColor.RED + "Invalid Argument: "
							+ args[2]);
					return false;
				}
			} else if (args[0].equalsIgnoreCase("set")) {
				try {
					if (SkyWars.getEconomy().setMoney(p2.getName(),
							Double.parseDouble(args[2]))) {

						s.sendMessage(ChatColor.GOLD + "Success! "
								+ p2.getDisplayName() + "'s money: "
								+ SkyWars.getEconomy().getBalance(p2.getName()));
						return true;
					} else {
						s.sendMessage(ChatColor.RED + "Invalid Argument: "
								+ args[2]);
						return false;
					}
				} catch (NumberFormatException e) {
					s.sendMessage(ChatColor.RED + "Invalid Argument: "
							+ args[2]);
					return false;
				}
			} else {
				s.sendMessage(ChatColor.GOLD
						+ "/eco <give/take/reset/set> <player> <amount>");
				return false;
			}
		}
		return false;
	}

}
