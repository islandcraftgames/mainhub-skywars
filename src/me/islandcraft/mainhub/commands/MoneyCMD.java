package me.islandcraft.mainhub.commands;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.skywars.SkyWars;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MoneyCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		if (args.length == 0) {
			if (!(s instanceof Player)) {
				s.sendMessage("/money <player>");
				return false;
			}
			Player p = (Player) s;
			p.sendMessage(ChatColor.GOLD
					+ LanguageMain.get(p, "main.cmd.money") + ": "
					+ SkyWars.getEconomy().getBalance(p.getName()));
			return true;
		} else {
			if (!(s instanceof Player)) {
				if (Bukkit.getPlayer(args[0]) == null) {
					s.sendMessage(ChatColor.RED + args[0] + " is not online!");
					return false;
				}
				s.sendMessage(Bukkit.getPlayer(args[0]).getDisplayName()
						+ ChatColor.GOLD + "'s money: "
						+ SkyWars.getEconomy().getBalance(args[0]));
				return true;
			}
			Player p = (Player) s;
			if (!p.hasPermission("admin")) {
				p.sendMessage(ChatColor.RED + LanguageMain.get(p, "sw.noperm"));
				return false;
			}
			if (Bukkit.getPlayer(args[0]) == null) {
				p.sendMessage(ChatColor.RED
						+ LanguageMain.get(p, "main.cmd.notonline",
								new String[] { args[0] }));
				return false;
			}
			Player tp = Bukkit.getPlayer(args[0]);
			p.sendMessage(ChatColor.GOLD
					+ LanguageMain.get(p, "main.cmd.money.others",
							new String[] { tp.getDisplayName() }) + ": "
					+ SkyWars.getEconomy().getBalance(tp.getName()));
		}
		return false;
	}

}
