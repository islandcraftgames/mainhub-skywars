package me.islandcraft.mainhub.commands;

import java.util.HashMap;

import me.islandcraft.mainhub.language.LanguageMain;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

public class TptoogleCMD implements CommandExecutor {

	public static HashMap<Player, Boolean> tpt = Maps.newHashMap();

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage("Only players");
		}

		Player p = (Player) s;

		if (tpt.get(p) == false) {
			tpt.put(p, true);
			p.sendMessage(ChatColor.RED + LanguageMain.get(p, "main.tpenable"));
		} else if (tpt.get(p) == true || tpt.get(p) == null) {
			p.sendMessage(ChatColor.RED + LanguageMain.get(p, "main.tpdisable"));
		}

		return false;
	}

}
