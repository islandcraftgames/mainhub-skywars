package me.islandcraft.mainhub.commands;

import java.util.List;

import me.islandcraft.gameapi.IGameController;
import me.islandcraft.gameapi.controllers.ClassController;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

public class IcStopCMD implements CommandExecutor, Runnable, Listener {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (!s.hasPermission("main.stop"))
			return false;

		if (canRestart()) {

		}

		return false;
	}

	@Override
	public void run() {

	}

	private boolean canRestart() {
		List<IGameController> igclist = ClassController.getAllGameControllers();
		boolean rs = true;

		for (IGameController gc : igclist) {
			if (!gc.canRestart())
				rs = false;
			gc.disableGames();
		}

		return rs;
	}

}
