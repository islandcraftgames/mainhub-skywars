package me.islandcraft.mainhub.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.islandcraft.mainhub.managers.BungeeManager;

public class KitpvpCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		if (s instanceof Player) {
			BungeeManager.sendToServer((Player) s, "kitpvp");
			return true;
		}
		s.sendMessage("Only players");
		return false;
	}

}
