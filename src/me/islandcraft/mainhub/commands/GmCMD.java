package me.islandcraft.mainhub.commands;

import me.islandcraft.mainhub.language.LanguageMain;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GmCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage(ChatColor.RED + "Only players");
			return false;
		}

		Player p = (Player) s;

		if (!p.hasPermission("admin")) {
			p.sendMessage(ChatColor.RED + LanguageMain.get(p, "sw.noperm"));
			return false;
		}

		if (args.length > 0) {
			switch (args[0]) {
			case "0":
				p.setGameMode(GameMode.SURVIVAL);
				return true;
			case "1":
				p.setGameMode(GameMode.CREATIVE);
				return true;
			case "2":
				p.setGameMode(GameMode.ADVENTURE);
				return true;
			case "3":
				p.setGameMode(GameMode.SPECTATOR);
				return true;
			}
		}

		switch (p.getGameMode()) {
		case ADVENTURE:
			p.setGameMode(GameMode.CREATIVE);
			break;
		case CREATIVE:
			p.setGameMode(GameMode.SURVIVAL);
			break;
		case SPECTATOR:
			p.setGameMode(GameMode.CREATIVE);
			break;
		case SURVIVAL:
			p.setGameMode(GameMode.CREATIVE);
			break;
		}

		return false;
	}
}
