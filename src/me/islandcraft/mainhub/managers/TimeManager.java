package me.islandcraft.mainhub.managers;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import me.islandcraft.mainhub.bans.Time;

public class TimeManager {

	public static GregorianCalendar getCurrentTime() {
		GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone("Europe/Lisbon"));
		cal.add(Calendar.MINUTE, -10);
		return cal;
	}

	public static GregorianCalendar getFinalGregorian(String args)
			throws NumberFormatException {
		GregorianCalendar cal = getCurrentTime();
		args.toLowerCase();
		if (args.endsWith("y")) {
			String a = args.replaceAll("y", "");
			cal.add(Calendar.YEAR, Integer.valueOf(a));
			return cal;
		} else if (args.endsWith("mo")) {
			String a = args.replaceAll("mo", "");
			cal.add(Calendar.MONTH, Integer.valueOf(a));
			return cal;
		} else if (args.endsWith("d")) {
			String a = args.replaceAll("d", "");
			cal.add(Calendar.DAY_OF_MONTH, Integer.valueOf(a));
			return cal;
		} else if (args.endsWith("h")) {
			String a = args.replaceAll("h", "");
			cal.add(Calendar.HOUR_OF_DAY, Integer.valueOf(a));
			return cal;
		} else if (args.endsWith("m")) {
			String a = args.replaceAll("m", "");
			cal.add(Calendar.MINUTE, Integer.valueOf(a));
			return cal;
		} else if (args.endsWith("s")) {
			String a = args.replaceAll("s", "");
			cal.add(Calendar.SECOND, Integer.valueOf(a));
			return cal;
		}
		GregorianCalendar calNull = new GregorianCalendar(0, 0, 0, 0, 0, 0);
		return calNull;
	}

	public static Time getFinalTime(String args) {

		return fromGregorianToTime(getFinalGregorian(args));
	}

	public static Time fromGregorianToTime(GregorianCalendar cal) {
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		return new Time(second, minute, hour, day, month, year);
	}

}
