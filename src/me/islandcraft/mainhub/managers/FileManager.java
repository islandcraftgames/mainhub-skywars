package me.islandcraft.mainhub.managers;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import me.islandcraft.mainhub.Main;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileManager {

	public static File getPlayerFile(UUID uuid) {
		File mainDirectory = Main.get().getDataFolder();
		File playersDirectory = new File(mainDirectory, "/players");
		
		if ((!playersDirectory.exists())) 
				if((!playersDirectory.mkdirs())) {
			System.out.println("Error creating players directory for player: "
					+ uuid.toString());
		}
		File playerFile = new File(playersDirectory, uuid.toString() + ".yml");
		if (!playerFile.exists()) {
			try {
				playerFile.createNewFile();
			} catch (IOException e) {
			}
		}
		return playerFile;
	}

	@SuppressWarnings("deprecation")
	public static File getPlayerFile(String name) {
		File mainDirectory = Main.get().getDataFolder();
		File playersDirectory = new File(mainDirectory, "/players");
		if (!playersDirectory.mkdirs()) {
			try {
				playersDirectory.createNewFile();
			} catch (IOException e) {
			}
		}
		for (File file : playersDirectory.listFiles()) {
			YamlConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			if (config.getString("name") == null) {
				file.delete();
			} else if (config.getString("name").equalsIgnoreCase(name))
				return file;
		}
		if (Bukkit.getOfflinePlayer(name).isOnline()) {
			return getPlayerFile(Bukkit.getPlayer(name).getUniqueId());
		}
		return null;
	}

	public static File getPlayerDirectory() {
		File mainDirectory = Main.get().getDataFolder();
		File playersDirectory = new File(mainDirectory, "/players");
		if (!playersDirectory.mkdirs()) {
			try {
				playersDirectory.createNewFile();
			} catch (IOException e) {
			}
		}
		return playersDirectory;
	}

}
