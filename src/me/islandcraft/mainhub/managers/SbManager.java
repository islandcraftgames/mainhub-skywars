package me.islandcraft.mainhub.managers;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.skywars.SkyWars;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class SbManager {

	public static void updateScoreboard(Player p) {
		Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective ob = sb.registerNewObjective(ChatColor.GREEN + "IslandCraft",
				"dummy");
		ob.getScore(
				ChatColor.GREEN + "" + ChatColor.BOLD
						+ LanguageMain.get(p, "main.sb.name")).setScore(8);
		ob.getScore(ChatColor.GOLD + p.getName()).setScore(7);
		ob.getScore("  ").setScore(6);

		ob.getScore(
				ChatColor.GREEN + "" + ChatColor.BOLD
						+ LanguageMain.get(p, "main.sb.level")).setScore(5);
		ob.getScore(ChatColor.GOLD + "" + p.getLevel()).setScore(4);
		ob.getScore(" ").setScore(3);

		ob.getScore(
				ChatColor.GREEN + "" + ChatColor.BOLD
						+ LanguageMain.get(p, "main.sb.money")).setScore(2);
		ob.getScore(
				ChatColor.GOLD + ""
						+ SkyWars.getEconomy().getBalance(p.getName()))
				.setScore(1);
		ob.setDisplaySlot(DisplaySlot.SIDEBAR);
		p.setScoreboard(sb);
	}

}
