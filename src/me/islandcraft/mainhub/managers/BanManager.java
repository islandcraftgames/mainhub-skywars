package me.islandcraft.mainhub.managers;

import me.islandcraft.mainhub.bans.BanType;
import me.islandcraft.mainhub.bans.Time;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.player.IslandPlayer;
import me.islandcraft.mainhub.player.PlayerManager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class BanManager {

	public static String getTime(Time unban) {
		return unban.getDay() + "/" + (unban.getMonth() + 1) + "/" + unban.getYear()
				+ " " + unban.getHour() + ":" + unban.getMinute();
	}

	public static boolean ban(BanType bt, String p, Time unban, String reason,
			String banner) {

		if (FileManager.getPlayerFile(p) == null)
			return false;

		IslandPlayer ip = PlayerManager.get(p);

		if (bt == null || bt == BanType.NULL) {
			ip.setBanType(BanType.NULL);
			return true;
		}

		if (bt == BanType.MUTE) {
			ip.setBanType(bt);
			ip.setExpires(unban);
			ip.setReason(reason);
			ip.setBanner(PlayerManager.getNull(banner));
			return true;
		}

		ip.setBanType(bt);
		ip.setExpires(unban);
		ip.setReason(reason);
		ip.setBanner(PlayerManager.getNull(banner));
		Player p2 = Bukkit.getPlayer(p);
		if (p2 != null) {
			if (unban == null) {
				p2.kickPlayer(LanguageMain.get(p2, "main.ban.permanent",
						new String[] { reason }));
				return true;
			}
			p2.kickPlayer(LanguageMain.get(p2, "main.ban.temp", new String[] {
					 getTime(unban),reason }));
		}
		return true;
	}
}
