package me.islandcraft.mainhub.language.langs;

import me.islandcraft.mainhub.language.Language;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

public class enUK extends Language {

	public enUK() {
		// setName("United Kingdom");
		setName("en_UK");
		ItemStack a = new ItemStack(Material.BANNER);
		BannerMeta banner = (BannerMeta) a.getItemMeta();
		banner.setBaseColor(DyeColor.BLUE);
		banner.addPattern(new Pattern(DyeColor.WHITE,
				PatternType.STRIPE_DOWNLEFT));
		banner.addPattern(new Pattern(DyeColor.WHITE,
				PatternType.STRIPE_DOWNRIGHT));
		banner.addPattern(new Pattern(DyeColor.WHITE, PatternType.STRIPE_MIDDLE));
		banner.addPattern(new Pattern(DyeColor.WHITE, PatternType.STRIPE_CENTER));
		banner.addPattern(new Pattern(DyeColor.RED, PatternType.CROSS));
		banner.addPattern(new Pattern(DyeColor.RED, PatternType.STRAIGHT_CROSS));
		banner.setDisplayName(ChatColor.GREEN + "English (UK)");
		a.setItemMeta(banner);
		setMeta(banner);
		setStack(a);

		
		/*lang.put("sw.kit0", "5 Snowballs");
		lang.put("sw.kit1", "10 Snowballs");
		lang.put("sw.kit2", "16 Snowballs");
		lang.put("sw.kit3", "1 Flint & Steel and 10 TNTs");
		lang.put("sw.kit4", "1 Fishing Rod");
		lang.put("sw.kit5", "1 Bow and 7 arrows");
		lang.put("sw.kit6", "10% out of 100% chance to get 3 golden apples");
		lang.put("sw.kit7", "1 Iron Pickaxe");
		lang.put("sw.kit8", "1 Ender Pearl");
		lang.put("sw.kit9", "2 Ender Pearls");
		lang.put("sw.kit10", "3 Ender Pearls");
		lang.put("sw.shop", "SkyWars store");
		lang.put("sw.noplaying", "You are not playing Skywars!");
		lang.put("sw.noperm", "You have no permission to perform this command");
		lang.put("sw.commandinfo", "Commands:");
		lang.put("sw.nodescription", "no comment");
		lang.put("sw.reloadsuccess", "Sucessfully Reloaded!");
		lang.put("sw.lobbyset", "sucessfully updatedpdated the lobby's pos!");
		lang.put("sw.notplaying", "You are not playing Skywars!");
		lang.put("sw.alreadystarted", "The game has alredy started!");
		lang.put("sw.insuficientplayers", "Not enough players!");
		lang.put("sw.creatingarena", "Creating the arena... Please wait.");
		lang.put("sw.joinarena", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " joined the match! (%2/%3)");
		lang.put("sw.leftingame", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " was afraid of death and left the match! %2 players left!");
		lang.put("sw.left", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " left the match! (%2/%3)");
		lang.put("sw.getcredits", "You won %1 credits!");
		lang.put("sw.die", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " was killed by %2" + ChatColor.GOLD + "! %3 players left!");
		lang.put("sw.lastdie", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " was killed by %2" + ChatColor.GOLD + "! %3"
				+ ChatColor.GOLD + " won the match!");
		lang.put("sw.suicide", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " couldn't handle the pressure! %2 players left!");
		lang.put("sw.lastsuicide", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " couldn't handle the pressure! %2" + ChatColor.GOLD
				+ " won the match!");
		lang.put("sw.sbplayer", "Players:");
		lang.put("sw.start", "The match has begun! Good luck and have fun!");
		lang.put("sw.win", "You won the match!");
		lang.put("sw.nominplayers", ChatColor.RED
				+ "The match needs atleast %1 players to start");
		lang.put("sw.starting", ChatColor.GREEN
				+ "The game will start in %1 %2!");
		lang.put("sw.leftitemname", "Leave the match");
		lang.put("sw.compassitemname", "Nearest Player");
		lang.put("sw.tellonlygame",
				"You can only send PMs to players in the same match!");
		lang.put("sw.nocmdarena", "That command is not allowed!");
		lang.put("sw.leftarena", "You left the arena, you fool...");
		lang.put("sw.noflyingame", "You can't fly while playing!");
		lang.put("sw.nogamemodeingame",
				"You can't change your gamemode while playing!");
		lang.put("sw.waitenderpearl", "Wait %1 %2 to trow another enderpearl");
		lang.put("sw.enderpearlwaiting",
				"You can't send an enderpearl before the game starts.");
		lang.put("sw.buykit", "You bought kit '%1'! Current Balance: ");
		lang.put("sw.nomoneykit", "You need " + ChatColor.GOLD + "%1"
				+ ChatColor.RED + " more credits to buy kit'%2'!");
		lang.put("sw.alreadykit", "You alredy have kit '%1'!");
		lang.put("sw.needkit",
				"You need to buy kit '%1' before buying this kit!");
		lang.put("sw.seconds", "seconds");
		lang.put("sw.second", "second");
		lang.put("menu.gamesplayed", "You played: ");
		lang.put("menu.gameswon", "You won: ");
		lang.put("menu.gameslosed", "You lost: ");
		lang.put("menu.kills", "You killed: ");
		lang.put("menu.deaths", "You died: ");
		lang.put("party.onlyownercanjoin",
				"Only the owner of your party can enter a game!");
		lang.put("main.tpsuccess", ChatColor.GREEN
				+ "You have been teleported to the " + ChatColor.GOLD + "%1"
				+ ChatColor.GREEN + "!");
		lang.put("main.tpenable", "Teleportation Enabled");
		lang.put("main.tpdisable", "Teleportation Disabled");
		lang.put("sw.waiting", "waiting");
		lang.put("sw.playing", "playing");
		lang.put("main.item.changelang", "Change Language");
		lang.put("main.inv.selectlang", "Select a language");
		lang.put("main.inv.travelto", "Select a destination:");
		lang.put("main.item.compass", "Travel");
		lang.put("main.item.vote", "Vote for rewards!");
		lang.put("main.cmd.invalidarg", "Invalid Argument");
		lang.put("main.cmd.ecosuccess", "Success! %1's money: %2");
		lang.put("main.cmd.notonline", "%1 is not online!");
		lang.put("main.cmd.money", "Money");
		lang.put("main.cmd.money.others", "'s money");
		lang.put("main.title.welcome", "Welcome to IslandCraft!");
		lang.put("main.sb.name", "Name:");
		lang.put("main.sb.level", "Level:");
		lang.put("main.sb.money", "Money:");
		lang.put("party.createsuccess", "Party created successfully!");
		lang.put("party.alreadyinaparty", "You are already in a party!");
		lang.put("party.notonline", "The player '%1' is not online!");
		lang.put("party.invitemeerr", "You can't invite yourself");
		lang.put("party.notinaparty", "You aren't in a party!");
		lang.put("party.notinaparty.others", ChatColor.RED
				+ "The player '%1' isn't in a party!");
		lang.put("party.onlyowner", "Only the owner can do that action!");
		lang.put("party.alreadyinaparty.others", ChatColor.RED
				+ "The player '%1' is already in a party!");
		lang.put("party.alreadyinvited.others", ChatColor.RED
				+ "You already invited '%1' to your party!");
		lang.put("party.invitesuccess", ChatColor.GREEN
				+ "Player '%1' invited successfully!");
		lang.put(
				"party.invited",
				"{text:\"You have been invited for %1's party! Click here to accept this invite!\","
						+ "clickEvent:{action:run_command,value:\"/party accept %2\"},"
						+ "hoverEvent:{action:show_text,value:\"/party accept %2\"},color:green}");
		lang.put("party.notthesame", ChatColor.RED
				+ "The player %1 isn't in your party!");
		lang.put("party.kick", ChatColor.GOLD
				+ "The player %1 has been kicked from your party by %2!");
		lang.put("party.delete", "Your party has been deleted!");
		lang.put("party.leave", "The player '%1' left your party!");
		lang.put("party.join", "The player '%1' joined your party!");
		lang.put("party.info.players", "Players: ");
		lang.put("party.info.yes", "YES");
		lang.put("party.info.no", "NO");
		lang.put("party.notinvited", ChatColor.RED
				+ "The player '%1' didn't invite you!");
		lang.put("party.onlyvip", "Only VIPs can use that command!");
		lang.put("1v1.winner", "Winner: ");
		lang.put("1v1.needsparty",
				"You need a party to play 1v1. Use /party to help.");
		lang.put("ctf.kit0", "No fall damage");
		lang.put("ctf.kit1", "64 cobblestones");
		lang.put("ctf.kit2", "Bomberman (1 TNT)");
		lang.put("ctf.kit3", "Bomberman EXTREME (5 TNTs)");
		lang.put("ctf.kills", "Kills:");
		lang.put("ctf.deaths", "Deaths:");
		lang.put("ctf.blueflag", "Blue Flag:");
		lang.put("ctf.redflag", "Red Flag:");
		lang.put("ctf.none", "At base");
		lang.put("ctf.die", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " was killed by %2" + ChatColor.GOLD + "!");
		lang.put("ctf.suicide", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " couldn't handle the pressure!");
		lang.put("main.ban.permanent", ChatColor.RED + "" + ChatColor.BOLD
				+ "You have been permanently banned from this server for: %1");
		lang.put("main.ban.temp", ChatColor.RED + "" + ChatColor.BOLD
				+ "You have been temporarely banned until %1 for: %2");
		lang.put("main.ban.kick", ChatColor.RED + "" + ChatColor.BOLD
				+ "You have been kicked from this server for: %1");
		lang.put("main.ban.mute", ChatColor.RED + "" + ChatColor.BOLD
				+ "You have been temporarely muted until %1 for: %2");
		lang.put("main.ban.mute.perm", ChatColor.RED + "" + ChatColor.BOLD
				+ "You have been permanently muted for: %1");
		lang.put("game.building", "We are configuring the arena. Please wait!");
		lang.put("main.commingsoon", "Comming Soon");
		lang.put("game.building.bar", "Average percentage: %1");*/
		loadMessages();
	}

}
