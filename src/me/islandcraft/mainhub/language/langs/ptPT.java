package me.islandcraft.mainhub.language.langs;

import me.islandcraft.mainhub.language.Language;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

public class ptPT extends Language {

	public ptPT() {
		// setName("Portugal");
		setName("pt_PT");
		ItemStack a = new ItemStack(Material.BANNER);
		BannerMeta banner = (BannerMeta) a.getItemMeta();
		banner.setBaseColor(DyeColor.RED);
		banner.addPattern(new Pattern(DyeColor.GREEN,
				PatternType.HALF_HORIZONTAL_MIRROR));
		banner.addPattern(new Pattern(DyeColor.YELLOW,
				PatternType.CIRCLE_MIDDLE));
		banner.setDisplayName(ChatColor.GREEN + "Portuguese (Portugal)");
		a.setItemMeta(banner);
		setMeta(banner);
		setStack(a);

		/*HashMap<String, String> lang = new HashMap<String, String>();
		lang.put("sw.kit0", "5 Bolas de Neve");
		lang.put("sw.kit1", "10 Bolas de Neve");
		lang.put("sw.kit2", "16 Bolas de Neve");
		lang.put("sw.kit3", "1 Isqueiro e 10 TNTs");
		lang.put("sw.kit4", "1 Cana de Pesca");
		lang.put("sw.kit5", "1 Arco e 7 Flechas");
		lang.put("sw.kit6", "10% de chance de 3 GoldenApple");
		lang.put("sw.kit7", "1 Picareta de Ferro");
		lang.put("sw.kit8", "1 Ender Pearl");
		lang.put("sw.kit9", "2 Ender Pearls");
		lang.put("sw.kit10", "3 Ender Pearls");
		lang.put("sw.shop", "Loja do SkyWars");
		lang.put("sw.noplaying", "Tu n�o est�s a jogar!");
		lang.put("sw.noperm", "Sem permiss�o");
		lang.put("sw.commandinfo", "Comandos:");
		lang.put("sw.nodescription", "Sem descri��o dispon�vel");
		lang.put("sw.reloadsuccess", "Reload com sucesso!");
		lang.put("sw.lobbyset", "Lobby atualizada com sucesso!");
		lang.put("sw.notplaying", "N�o est�s a jogar!");
		lang.put("sw.alreadystarted", "O jogo j� come�ou!");
		lang.put("sw.insuficientplayers", "Sem jogadores suficientes!");
		lang.put("sw.creatingarena", "A criar a arena... Aguarde.");
		lang.put("sw.joinarena", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " entrou no SkyWars! (%2/%3)");
		lang.put("sw.leftingame", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " saiu com medo de morrer! %2 jogadores restantes!");
		lang.put("sw.left", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " saiu da partida! (%2/%3)");
		lang.put("sw.getcredits", "Ganhaste %1 cr�ditos");
		lang.put("sw.die", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " foi morto por %2" + ChatColor.GOLD
				+ "! %3 jogadores restantes!");
		lang.put("sw.lastdie", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " foi morto por %2" + ChatColor.GOLD + "! %3"
				+ ChatColor.GOLD + " ganhou o SkyWars!");
		lang.put("sw.suicide", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " suicidou-se! %2 jogadores restantes!");
		lang.put("sw.lastsuicide", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " suicidou-se! %2" + ChatColor.GOLD + " ganhou o SkyWars!");
		lang.put("sw.sbplayer", "Jogadores:");
		lang.put("sw.start", "O jogo come�ou! Boa sorte a todos!");
		lang.put("sw.win", "Ganhaste!");
		lang.put(
				"sw.nominplayers",
				ChatColor.RED
						+ "S�o nessess�rios, pelo menos, %1 jogadores para iniciar o SkyWars");
		lang.put("sw.starting", ChatColor.GREEN
				+ "O jogo vai come�ar em %1 %2!");
		lang.put("sw.leftitemname", "Abandonar SkyWars");
		lang.put("sw.compassitemname", "Jogador mais pr�ximo");
		lang.put("sw.tellonlygame",
				"S� podes mandar mensagens privadas para jogadores na mesma arena que tu!");
		lang.put("sw.nocmdarena",
				"N�o s�o permitidos comandos dentro da arena!");
		lang.put("sw.leftarena", "Saiste da arena a meio do jogo!");
		lang.put("sw.noflyingame", "N�o podes voar enquanto jogas!");
		lang.put("sw.nogamemodeingame",
				"N�o podes mudar de gamemode enquanto jogas!");
		lang.put("sw.waitenderpearl",
				"Espera mais %1 %2 para lan�ar uma Ender Pearl");
		lang.put("sw.enderpearlwaiting",
				"N�o podes lan�ar uma Ender Pearl enquanto o jogo n�o come�ar");
		lang.put("sw.buykit", "O kit '%1' foi comprado! Dinheiro atual: ");
		lang.put("sw.nomoneykit", "Precisas de mais " + ChatColor.GOLD + "%1"
				+ ChatColor.RED + " cr�ditos para comparar o kit '%2'!");
		lang.put("sw.alreadykit", "J� tens o kit '%1'!");
		lang.put("sw.needkit",
				"Precisas de comprar o kit '%1' antes de comprar este kit!");
		lang.put("sw.seconds", "segundos");
		lang.put("sw.second", "segundo");
		lang.put("menu.gamesplayed", "Jogaste: ");
		lang.put("menu.gameswon", "Ganhaste: ");
		lang.put("menu.gameslosed", "Perdeste: ");
		lang.put("menu.kills", "Mataste: ");
		lang.put("menu.deaths", "Morreste: ");
		lang.put("party.onlyownercanjoin",
				"Apenas o dono da party pode entrar nos jogos!");
		lang.put("main.tpsuccess", ChatColor.GREEN
				+ "Foste teletransportado para o " + ChatColor.GOLD + "%1"
				+ ChatColor.GREEN + "!");
		lang.put("main.tpenable", "TP Ligado");
		lang.put("main.tpdisable", "TP Desligado");
		lang.put("sw.waiting", "� espera");
		lang.put("sw.playing", "a jogar");
		lang.put("main.item.changelang", "Mudar de idioma");
		lang.put("main.inv.selectlang", "Selecione um idioma");
		lang.put("main.inv.travelto", "Selecione um destino:");
		lang.put("main.item.compass", "Viajar");
		lang.put("main.item.vote", "Vote para recompensas!");
		lang.put("main.cmd.invalidarg", "Argumento inv�lido");
		lang.put("main.cmd.ecosuccess", "Sucesso! Dinheiro atual de %1: %2");
		lang.put("main.cmd.notonline", "%1 n�o est� online");
		lang.put("main.cmd.money", "Dinheiro atual");
		lang.put("main.cmd.money.others", "Dinheiro atual de %1");
		lang.put("main.title.welcome", "Bem-Vindo ao IslandCraft!");
		lang.put("main.sb.name", "Nome:");
		lang.put("main.sb.level", "Nivel:");
		lang.put("main.sb.money", "Dinheiro:");
		lang.put("party.createsuccess", "Party criada com sucesso!");
		lang.put("party.alreadyinaparty", "J� est�s numa party!");
		lang.put("party.notonline", "O jogador '%1" + ChatColor.RED
				+ "' n�o est� online!");
		lang.put("party.invitemeerr", "N�o podes convidar-te a ti mesmo!");
		lang.put("party.notinaparty", "N�o est�s numa party!");
		lang.put("party.notinaparty.others", ChatColor.RED
				+ "O jogador '%1' n�o est� numa party!");
		lang.put("party.onlyowner", "Apenas o dono da party pode fazer isso!");
		lang.put("party.alreadyinaparty.others", ChatColor.RED
				+ "O jogador '%1' j� est� numa party!");
		lang.put("party.alreadyinvited.others", ChatColor.RED
				+ "J� convidas-te '%1' para a tua party!");
		lang.put("party.invitesuccess", ChatColor.GREEN
				+ "Jogador '%1' convidado com sucesso!");
		lang.put(
				"party.invited",
				"{text:\"Foste convidado para a party de %1! Clica aqui para aceitares o convite!\","
						+ "clickEvent:{action:run_command,value:\"/party accept %2\"},"
						+ "hoverEvent:{action:show_text,value:\"/party accept %2\"},color:green}");
		lang.put("party.notthesame", ChatColor.RED
				+ "O jogador %1 n�o est� na tua party!");
		lang.put("party.kick", ChatColor.GOLD
				+ "O jogador %1 foi kickado por %2!");
		lang.put("party.delete", "A tua party foi apagada!");
		lang.put("party.leave", "O jogador '%1" + ChatColor.RED
				+ "' saiu da tua party!");
		lang.put("party.join", "O jogador '%1" + ChatColor.GREEN
				+ "' entrou na tua party!");
		lang.put("party.info.players", "Jogadores: ");
		lang.put("party.info.yes", "SIM");
		lang.put("party.info.no", "N�O");
		lang.put("party.notinvited", ChatColor.RED
				+ "O jogador '%1' n�o te convidou!");
		lang.put("party.onlyvip", "Apenas os VIPs podem usar este comando!");
		lang.put("1v1.winner", "Vencedor: ");
		lang.put("1v1.needsparty",
				"Precisas de uma party para jogar 1v1. Usa /party para mais ajuda.");
		lang.put("ctf.kit0", "Sem dano de queda");
		lang.put("ctf.kit1", "64 pedras");
		lang.put("ctf.kit2", "Bomberman (1 TNT)");
		lang.put("ctf.kit3", "Bomberman EXTREME (5 TNTs)");
		lang.put("ctf.kills", "Matou:");
		lang.put("ctf.deaths", "Morreu:");
		lang.put("ctf.blueflag", "Bandeira azul:");
		lang.put("ctf.redflag", "Bandeira vermelha:");
		lang.put("ctf.none", "Na base");
		lang.put("sw.die", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " foi morto por %2" + ChatColor.GOLD + "!");
		lang.put("sw.suicide", ChatColor.GOLD + "%1" + ChatColor.GOLD
				+ " suicidou-se!");
		lang.put("main.ban.permanent", ChatColor.RED + "" + ChatColor.BOLD
				+ "Foste banido permanentemente deste servidor por: %1");
		lang.put("main.ban.temp", ChatColor.RED + "" + ChatColor.BOLD
				+ "Foste banido temporariamente deste servidor at� %1 por: %2");
		lang.put("main.ban.kick", ChatColor.RED + "" + ChatColor.BOLD
				+ "Foste kickado deste servidor por: %1");
		lang.put("main.ban.mute", ChatColor.RED + "" + ChatColor.BOLD
				+ "Foste mutado temporariamente deste servidor at� %1 por: %2");
		lang.put("main.ban.mute.perm", ChatColor.RED + "" + ChatColor.BOLD
				+ "Foste mutado permanentemente deste servidor por: %1");
		lang.put("game.building",
				"Estamos a configurar a arena. Porfavor aguarde!");
		lang.put("main.commingsoon", "Brevemente");
		lang.put("game.building.bar", "Percentagem m�dia: %1");
		setMap(lang);*/
		loadMessages();
	}

}
