package me.islandcraft.mainhub.language.langs;

import me.islandcraft.mainhub.language.Language;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

public class esES extends Language {

	public esES() {
		setName("es_ES");
		ItemStack a = new ItemStack(Material.BANNER);
		BannerMeta banner = (BannerMeta) a.getItemMeta();
		banner.setBaseColor(DyeColor.YELLOW);
		banner.addPattern(new Pattern(DyeColor.RED,
				PatternType.STRIPE_LEFT));
		banner.addPattern(new Pattern(DyeColor.RED,
				PatternType.STRIPE_RIGHT));
		banner.setDisplayName(ChatColor.GREEN + "Spanish (Spain)");
		a.setItemMeta(banner);
		setMeta(banner);
		setStack(a);
		loadMessages();
	}

}
