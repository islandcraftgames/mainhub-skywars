package me.islandcraft.mainhub.language;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.BannerMeta;

import com.google.common.collect.Lists;

import me.islandcraft.gameapi.IGamePlayer;
import me.islandcraft.mainhub.language.langs.enUK;
import me.islandcraft.mainhub.language.langs.enUS;
import me.islandcraft.mainhub.language.langs.esES;
import me.islandcraft.mainhub.language.langs.ptBR;
import me.islandcraft.mainhub.language.langs.ptPT;
import me.islandcraft.mainhub.player.IslandPlayer;
import me.islandcraft.mainhub.player.PlayerManager;

public class LanguageMain {

	private static List<Language> langs = Lists.newArrayList();
	private static Language mainLang;
	private static boolean settedup = false;

	public static String get(String lang, String message, String[] complements) {
		if (!settedup)
			setup();

		for (Language l : langs) {
			if (lang.equalsIgnoreCase(l.getName())) {
				if (l.getMap().get(message) == null)
					return getFromMain(lang, message, complements);
				if (complements.length == 0) {
					return ChatColor.translateAlternateColorCodes('&', l.getMap().get(message));
				} else {
					String a = l.getMap().get(message);
					ChatColor b = null;
					if (a.substring(0, 1).equalsIgnoreCase("�")) {
						ChatColor.getByChar(a.substring(1, 2));
					}
					for (int i = 0; i < complements.length; i++) {
						String replacement = "%" + Integer.toString(i + 1);
						a = a.replaceAll(replacement, complements[i]
								+ (b != null ? b : ""));
					}
					return ChatColor.translateAlternateColorCodes('&', a);
				}
			}
		}

		return getFromMain(lang, message, complements);

	}

	private static String getFromMain(String lang, String message,
			String[] complements) {
		if (mainLang.getMap().get(message) == null)
			return "ERROR 404: Message not found: '" + message
					+ "'! Please notify the staff!";
		if (complements.length == 0) {
			return ChatColor.translateAlternateColorCodes('&', mainLang.getMap().get(message));
		} else {
			String a = mainLang.getMap().get(message);
			ChatColor b = null;
			if (a.substring(0, 1).equalsIgnoreCase("�")) {
				ChatColor.getByChar(a.substring(1, 2));
			}
			for (int i = 0; i < complements.length; i++) {
				String replacement = "%" + Integer.toString(i + 1);
				a = a.replaceAll(replacement, complements[i]
						+ (b != null ? b : ""));
			}
			return ChatColor.translateAlternateColorCodes('&', a);
		}
	}

	public static String get(IslandPlayer ip, String message,
			String[] complements) {
		return get(ip.getLang(), message, complements);
	}

	public static String get(IGamePlayer gp, String message,
			String[] complements) {
		return get(PlayerManager.get(gp.getBukkitPlayer()), message,
				complements);
	}

	public static String get(Player p, String message, String[] complements) {
		return get(PlayerManager.get(p), message, complements);
	}

	public static String get(IslandPlayer ip, String message) {
		return get(ip.getLang(), message, new String[0]);
	}

	public static String get(IGamePlayer gp, String message) {
		return get(PlayerManager.get(gp.getBukkitPlayer()), message,
				new String[0]);
	}

	public static String get(Player p, String message) {
		return get(PlayerManager.get(p), message, new String[0]);
	}

	public static String getCountry(Player p) {
		try {
			Object ep = getMethod("getHandle", p.getClass()).invoke(p,
					(Class<?>) null);
			Field f = ep.getClass().getDeclaredField("locale");
			f.setAccessible(true);
			return (String) f.get(ep);
		} catch (Exception localException) {
		}
		return "en_UK";
	}

	private static Method getMethod(String name, Class<?> clazz) {
		Method[] arrayOfMethod;
		int j = (arrayOfMethod = clazz.getDeclaredMethods()).length;
		for (int i = 0; i < j; i++) {
			Method m = arrayOfMethod[i];
			if (m.getName().equals(name))
				return m;
		}
		return null;
	}

	public static boolean isLangMessage(String name, String langCode) {
		for (Language selected : getAllLangs()) {
			if (selected.getMap().get(langCode).equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}

	public static List<Language> getAllLangs() {
		return langs;
	}

	public static BannerMeta getFlag(Player p) {
		return getFlag(PlayerManager.get(p));
	}

	public static BannerMeta getFlag(IslandPlayer p) {
		for (Language lang : getAllLangs()) {
			if (lang.getName().equalsIgnoreCase(p.getLang())) {
				return lang.getMeta();
			}
		}
		return mainLang.getMeta();
	}

	public static void setup() {
		if (settedup)
			return;
		settedup = true;

		langs.add(setMainLanguage(new enUK()));
		langs.add(new enUS());
		langs.add(new ptPT());
		langs.add(new ptBR());
		langs.add(new esES());
	}

	public static void resetup() {
		for (Language l : langs)
			l.loadMessages();
	}
	
	private static Language setMainLanguage(Language main){
		mainLang = main;
		return mainLang;
	}

}
