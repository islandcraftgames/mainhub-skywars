package me.islandcraft.mainhub.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import me.islandcraft.gameapi.IGamePlayer;

public class TagUtils {

	@SuppressWarnings("deprecation")
	public static void setPrefix(Player p, String prefix, List<Player> toSend) {
		for (Player p2 : toSend) {
			Scoreboard sb = p2.getScoreboard();
			Team team = sb.getTeam(p.getName());
			if (prefix.length() == 0 && team != null) {
				team.removePlayer(p);
				return;
			}
			if (team == null)
				team = sb.registerNewTeam(p.getName());
			team.addPlayer(p);
			team.setPrefix(ChatColor.translateAlternateColorCodes('&', prefix));
		}
	}

	public static void setPrefix(Player p, String prefix, ArrayList<IGamePlayer> toSend) {
		setPrefix(p, prefix, ListUtils.gpListToPlayerList(toSend));
	}

	public static void setPrefix(Player p, String prefix, Collection<IGamePlayer> toSend) {
		setPrefix(p, prefix, new ArrayList<IGamePlayer>(toSend));
	}

}
