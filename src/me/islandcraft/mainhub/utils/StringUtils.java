
package me.islandcraft.mainhub.utils;

public class StringUtils {

	public static String formatString(String s) {
		if (s.length() == 0)
			return s;
		if (s.length() == 1)
			return s.toUpperCase();
		return (s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase()).replace("_", " ");
	}

}
