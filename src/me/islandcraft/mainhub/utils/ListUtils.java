package me.islandcraft.mainhub.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import me.islandcraft.gameapi.IGamePlayer;

import org.bukkit.entity.Player;

@SuppressWarnings({"rawtypes", "unchecked"})
public class ListUtils {

	public static List<?> collectionToList(Collection<?> c) {
		List l = new ArrayList(c);
		return l;
	}
	
	public static List setToList(Set s){
		List l = new ArrayList();
		for(Object o : s){
			l.add(o);
		}
		return l;
	}
	
	public static List<Player> gpListToPlayerList(List<IGamePlayer> gpl) {
		List<Player> l = new ArrayList<Player>();
		for (IGamePlayer gp : gpl) {
			l.add(gp.getBukkitPlayer());
		}
		return l;
	}
	

}
