package me.islandcraft.mainhub.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import me.islandcraft.gameapi.VillagerShop;
import me.islandcraft.mainhub.Main;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityVillager;

public enum EntityTypes {
	//VILLAGER_SHOP("VillagerShop", 120, VillagerShop.class);
	;

	private EntityTypes(String name, int id, Class<? extends Entity> custom) {
		//addToMaps(custom, name, id);
	}

	public static Entity spawnEntity(final Entity entity, Location loc) {
		entity.setPositionRotation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		((EntityVillager) entity).getControllerLook().a(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(),
				loc.getPitch());
		((CraftWorld) loc.getWorld()).getHandle().addEntity(entity);
		Bukkit.getScheduler().runTaskLater(Main.get(), new Runnable() {
			
			@Override
			public void run() {
				((VillagerShop) entity).clearGoals();				
			}
		}, 1L);
		return entity;
	}

	/*private static void addToMaps(Class clazz, String name, int id) {

		((Map) ObjectUtils.getPrivateField("c", net.minecraft.server.v1_8_R3.EntityTypes.class, null)).put(name, clazz);
		((Map) ObjectUtils.getPrivateField("d", net.minecraft.server.v1_8_R3.EntityTypes.class, null)).put(clazz, name);
		((Map) ObjectUtils.getPrivateField("f", net.minecraft.server.v1_8_R3.EntityTypes.class, null)).put(clazz,
				Integer.valueOf(id));
	}*/
}