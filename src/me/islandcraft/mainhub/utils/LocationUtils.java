package me.islandcraft.mainhub.utils;

import org.bukkit.Location;

public class LocationUtils {

	public static boolean isBlockEquals(Location l1, Location l2) {
		if (l1.getWorld() != l2.getWorld())
			return false;
		if (l1.getBlockX() != l2.getBlockX())
			return false;
		if (l1.getBlockY() != l2.getBlockY())
			return false;
		if (l1.getBlockZ() != l2.getBlockZ())
			return false;
		return true;
	}

}
