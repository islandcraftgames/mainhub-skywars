package me.islandcraft.mainhub.utils;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ItemUtils {

	public static boolean removeFromInventory(Player p, ItemStack is) {
		if (!containsAtLeast(p.getInventory(), is.getType(), is.getAmount()))
			return false;
		Material m = is.getType();
		int leftAmount = is.getAmount();
		for (ItemStack s : p.getInventory().getContents()) {
			if (s == null)
				continue;
			if (s.getType() != m)
				continue;
			if (s.getAmount() > leftAmount) {
				s.setAmount(s.getAmount() - leftAmount);
			} else if (s.getAmount() == leftAmount) {
				p.getInventory().remove(s);
			} else {
				leftAmount -= s.getAmount();
				p.getInventory().remove(s);
			}
			if (leftAmount <= 0)
				break;
		}
		return true;
	}

	public static boolean containsAtLeast(Inventory inv, Material m, int amount) {
		int i = 0;
		for (ItemStack s : inv.getContents()) {
			if (s == null)
				continue;
			if (s.getType() != m)
				continue;
			i += s.getAmount();
		}
		if (i >= amount)
			return true;
		return false;
	}

}
