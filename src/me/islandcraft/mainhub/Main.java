package me.islandcraft.mainhub;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

import me.islandcraft.mainhub.commands.AdventurelobbyCMD;
import me.islandcraft.mainhub.commands.ArcadelobbyCMD;
import me.islandcraft.mainhub.commands.BanCMD;
import me.islandcraft.mainhub.commands.EcoCMD;
import me.islandcraft.mainhub.commands.FactionsCMD;
import me.islandcraft.mainhub.commands.FlyCMD;
import me.islandcraft.mainhub.commands.GmCMD;
import me.islandcraft.mainhub.commands.HubCMD;
import me.islandcraft.mainhub.commands.KickCMD;
import me.islandcraft.mainhub.commands.MoneyCMD;
import me.islandcraft.mainhub.commands.PstatsCMD;
import me.islandcraft.mainhub.commands.PvplobbyCMD;
import me.islandcraft.mainhub.commands.ReloadLangCMD;
import me.islandcraft.mainhub.commands.SkylobbyCMD;
import me.islandcraft.mainhub.commands.SpeedCMD;
import me.islandcraft.mainhub.commands.StartitemsCMD;
import me.islandcraft.mainhub.commands.TptoogleCMD;
import me.islandcraft.mainhub.commands.UnbanCMD;
import me.islandcraft.mainhub.commands.VoteCMD;
import me.islandcraft.mainhub.commands.KitpvpCMD;
import me.islandcraft.mainhub.economy.EcoMain;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.listeners.BlockListener;
import me.islandcraft.mainhub.listeners.BlockTimeEvent;
import me.islandcraft.mainhub.listeners.ChatEvent;
import me.islandcraft.mainhub.listeners.InvListener;
import me.islandcraft.mainhub.listeners.JoinEvent;
import me.islandcraft.mainhub.listeners.PrefixesListener;
import me.islandcraft.mainhub.listeners.QuitEvent;
import me.islandcraft.mainhub.listeners.VotifierListener;
import me.islandcraft.mainhub.player.PlayerManager;
import me.islandcraft.party.PartyMain;
import me.islandcraft.skywars.SkyWars;
import me.islandcraft.skywars.utilities.WorldGenerator;
import net.alpenblock.bungeeperms.BungeePerms;
import net.alpenblock.bungeeperms.User;
import net.islandcraftgames.merchantapi.SMerchantPlugin;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;

public class Main extends JavaPlugin {
	private static Main main;
	private static EcoMain eco;
	private static SkyWars skywars;

	@Override
	public void onEnable() {
		eco = new EcoMain();
		main = this;
		LanguageMain.setup();
		PlayerManager.start();
		skywars = new SkyWars();
		new PartyMain();
		new SMerchantPlugin();
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
		reloadConfig();
		getCommand("sw").setExecutor(new me.islandcraft.skywars.commands.MainCommand());
		Bukkit.getPluginManager().registerEvents(new JoinEvent(), this);
		Bukkit.getPluginManager().registerEvents(new QuitEvent(), this);
		Bukkit.getPluginManager().registerEvents(new BlockTimeEvent(), this);
		Bukkit.getPluginManager().registerEvents(new InvListener(), this);
		Bukkit.getPluginManager().registerEvents(new ChatEvent(), this);
		Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
		Bukkit.getPluginManager().registerEvents(new VotifierListener(), this);
		Bukkit.getPluginManager().registerEvents(new PrefixesListener(), this);
		BanCMD bancmd = new BanCMD();
		Bukkit.getPluginManager().registerEvents(bancmd, this);
		getCommand("party").setExecutor(new me.islandcraft.party.commands.MainCMD());
		getCommand("ban").setExecutor(bancmd);
		getCommand("unban").setExecutor(new UnbanCMD());
		getCommand("kick").setExecutor(new KickCMD());
		getCommand("hub").setExecutor(new HubCMD());
		getCommand("skylobby").setExecutor(new SkylobbyCMD());
		getCommand("adventurelobby").setExecutor(new AdventurelobbyCMD());
		getCommand("arcadelobby").setExecutor(new ArcadelobbyCMD());
		getCommand("pvplobby").setExecutor(new PvplobbyCMD());
		getCommand("tptoogle").setExecutor(new TptoogleCMD());
		getCommand("economy").setExecutor(new EcoCMD());
		getCommand("money").setExecutor(new MoneyCMD());
		getCommand("gm").setExecutor(new GmCMD());
		getCommand("startitems").setExecutor(new StartitemsCMD());
		getCommand("speed").setExecutor(new SpeedCMD());
		getCommand("fly").setExecutor(new FlyCMD());
		getCommand("pstats").setExecutor(new PstatsCMD());
		getCommand("vote").setExecutor(new VoteCMD());
		getCommand("reloadlang").setExecutor(new ReloadLangCMD());
		getCommand("factions").setExecutor(new FactionsCMD());
		getCommand("kitpvp").setExecutor(new KitpvpCMD());
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new BlockTimeEvent(), 40L, 40L);

		reloadPrefixes();
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
	}

	public void testpackets(Player p) {
		PacketPlayOutNamedEntitySpawn packet = new PacketPlayOutNamedEntitySpawn(((CraftPlayer) p).getHandle());
		try {
			Field f = packet.getClass().getDeclaredField("h");
			f.setAccessible(true);

			f.setInt(packet, 0);

			f.setAccessible(!f.isAccessible());
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDisable() {
		skywars.onDisable();
		PlayerManager.shutdown();
	}

	public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
		return new WorldGenerator();
	}

	public static Main get() {
		return main;
	}

	public static EcoMain getEconomy() {
		return eco;
	}

	public void reloadPrefixes() {
		for (Player pp : Bukkit.getOnlinePlayers()) {
			reloadPrefix(pp);
		}
	}

	public void reloadPrefix(Player p) {
		User user = BungeePerms.getInstance().getPermissionsManager().getUser(p.getUniqueId().toString());
		String prefix = user.buildPrefix();
		if (prefix == null)
			prefix = "";
		String suffix = user.buildSuffix();
		if (suffix == null)
			suffix = "";
		p.setDisplayName(ChatColor.translateAlternateColorCodes('&', prefix + p.getName() + suffix) + ChatColor.RESET);
	}

}
