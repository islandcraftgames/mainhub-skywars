package me.islandcraft.mainhub.packetManagers;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Title {

	public Title(Player p, String title, String subtitle, int fadeInSec,
			int staySec, int fadeOutSec) {
		PacketPlayOutTitle titleP = new PacketPlayOutTitle(
				EnumTitleAction.TITLE, toJSON(title));
		PacketPlayOutTitle subtitleP = new PacketPlayOutTitle(
				EnumTitleAction.SUBTITLE, toJSON(subtitle));
		PacketPlayOutTitle timesP = new PacketPlayOutTitle(fadeInSec*20, staySec*20,
				fadeOutSec*20);
		for (PacketPlayOutTitle packet : new PacketPlayOutTitle[] { titleP,
				subtitleP, timesP }) {
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}
	}

	private IChatBaseComponent toJSON(String text) {
		return ChatSerializer.a("{\"text\": \"" + text + "\"}");
	}

}
