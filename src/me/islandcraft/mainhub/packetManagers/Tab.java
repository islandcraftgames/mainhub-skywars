package me.islandcraft.mainhub.packetManagers;

import java.lang.reflect.Field;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Tab {
	
	public Tab(Player p, String header, String footer){
		PacketPlayOutPlayerListHeaderFooter headerfooter = new PacketPlayOutPlayerListHeaderFooter();
		try {
		    Field headerF = headerfooter.getClass().getDeclaredField("a");
		    Field footerF = headerfooter.getClass().getDeclaredField("b");
		    headerF.setAccessible(true);
		    footerF.setAccessible(true);
		    headerF.set(headerfooter, toJSON(header));
		    footerF.set(headerfooter, toJSON(footer));
		} catch (Exception ex) {
		    ex.printStackTrace();
		}
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(headerfooter);
	}
	
	private IChatBaseComponent toJSON(String text) {
		return ChatSerializer.a("{\"text\": \"" + text + "\"}");
	}

}
