package me.islandcraft.mainhub.economy;

import me.islandcraft.mainhub.managers.SbManager;
import me.islandcraft.mainhub.player.IslandPlayer;
import me.islandcraft.mainhub.player.PlayerManager;

import org.bukkit.Bukkit;

public class EcoMain {

	public double getBalance(String p) {
		return PlayerManager.get(p).getMoney();
	}

	public boolean withdrawPlayer(String p, double money) {
		IslandPlayer ip = PlayerManager.get(p);
		if (ip.getMoney() - money >= 0) {
			ip.setMoney(ip.getMoney() - money);
			if(Bukkit.getPlayer(p) != null){
				SbManager.updateScoreboard(Bukkit.getPlayer(p));
			}
			return true;
		}
		return false;
	}

	public void depositPlayer(String p, double money) {
		IslandPlayer ip = PlayerManager.get(p);
		ip.setMoney(ip.getMoney() + money);
		if(Bukkit.getPlayer(p) != null){
			SbManager.updateScoreboard(Bukkit.getPlayer(p));
		}
	}
	
	public boolean setMoney(String p, double money){
		if(money >= 0){
			IslandPlayer ip = PlayerManager.get(p);
			ip.setMoney(money);
			if(Bukkit.getPlayer(p) != null){
				SbManager.updateScoreboard(Bukkit.getPlayer(p));
			}
			return true;
		}else{
			return false;
		}
	}

}
