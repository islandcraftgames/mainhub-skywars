package me.islandcraft.mainhub.player;

import java.io.IOException;
import java.util.UUID;

import me.islandcraft.mainhub.bans.BanType;
import me.islandcraft.mainhub.bans.Reasons;
import me.islandcraft.mainhub.bans.Time;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.managers.FileManager;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class IslandPlayer {
	private final String name;
	private final UUID uuid;
	private boolean state;
	private double money;
	private int xp;
	private BanType type;
	private Time expires;
	private String reason;
	private IslandPlayer banner;
	private int priority;
	private String lang;

	public IslandPlayer(String name) {
		YamlConfiguration config = YamlConfiguration
				.loadConfiguration(FileManager.getPlayerFile(name));
		if (!config.contains("name")) {
			config.set("name", name);
			config.set("online", PlayerManager.isOnline(name));
			config.set("last_ip", Bukkit.getPlayer(name).getAddress()
					.getAddress().getHostAddress());
			config.set("language",
					LanguageMain.getCountry(Bukkit.getPlayer(name)));
			config.set("xp", 0);
			config.set("money", 0);
			config.set("ban.type", BanType.NULL.toString());
			config.set("ban.expires", "null");
			config.set("ban.reason", "null");
			config.set("ban.banner", "null");
			try {
				config.save(FileManager.getPlayerFile(name));
			} catch (IOException e) {
			}
		}
		this.name = name;
		this.uuid = UUID.fromString(FileManager.getPlayerFile(name).getName()
				.replaceAll(".yml", ""));
		this.state = PlayerManager.isOnline(name);
		this.lang = config.getString("language", "United Kingdom");
		this.money = config.getDouble("money");
		this.xp = config.getInt("xp");
		this.type = BanType.getType(config.getString("ban.type"));
		this.expires = getTimeFromString(config.getString("ban.expires"));
		if (!config.getString("ban.reason", "null").equalsIgnoreCase("null")) {
			this.reason = config.getString("ban.reason");
		} else {
			this.reason = null;
		}
		if (!config.getString("ban.banner", "null").equalsIgnoreCase("null")) {
			this.banner = PlayerManager.get(Bukkit.getOfflinePlayer(UUID
					.fromString(config.getString("ban.banner"))));
		} else {
			this.banner = null;
		}
	}

	public IslandPlayer(Player p, boolean login) {
		if (!login) {
			YamlConfiguration config = YamlConfiguration
					.loadConfiguration(FileManager.getPlayerFile(p
							.getUniqueId()));
			if (!config.contains("name")) {
				config.set("name", p.getName());
				config.set("online", PlayerManager.isOnline(p));
				config.set("last_ip", p.getAddress().getAddress()
						.getHostAddress());
				config.set("language", LanguageMain.getCountry(p));
				config.set("xp", 0);
				config.set("money", 0);
				config.set("ban.type", BanType.NULL.toString());
				config.set("ban.expires", "null");
				config.set("ban.reason", "null");
				try {
					config.save(FileManager.getPlayerFile(p.getUniqueId()));
				} catch (IOException e) {
				}
			}
			this.name = p.getName();
			this.uuid = UUID.fromString(FileManager
					.getPlayerFile(p.getUniqueId()).getName()
					.replaceAll(".yml", ""));
			this.state = PlayerManager.isOnline(p);
			this.lang = config
					.getString("language", LanguageMain.getCountry(p));
			this.money = config.getDouble("money");
			this.xp = config.getInt("xp");
			this.type = BanType.getType(config.getString("ban.type"));
			this.expires = getTimeFromString(config.getString("ban.expires"));
			if (!config.getString("ban.reason", "null")
					.equalsIgnoreCase("null")) {
				this.reason = config.getString("ban.reason");
			} else {
				this.reason = null;
			}
			if (!config.getString("ban.banner", "null")
					.equalsIgnoreCase("null")) {
				this.banner = PlayerManager.get(Bukkit.getOfflinePlayer(UUID
						.fromString(config.getString("ban.banner"))));
			} else {
				this.banner = null;
			}
		} else {
			YamlConfiguration config = YamlConfiguration
					.loadConfiguration(FileManager.getPlayerFile(p
							.getUniqueId()));
			if (!config.contains("name")) {
				this.name = p.getName();
				this.uuid = UUID.fromString(FileManager
						.getPlayerFile(p.getUniqueId()).getName()
						.replaceAll(".yml", ""));
				this.state = PlayerManager.isOnline(p);
				this.lang = "en_US";
				this.money = 0;
				this.xp = 0;
				this.type = BanType.NULL;
				this.expires = new Time(0, 0, 0, 0, 0, 0);
				this.reason = null;
				return;
			}
			this.name = p.getName();
			this.uuid = UUID.fromString(FileManager
					.getPlayerFile(p.getUniqueId()).getName()
					.replaceAll(".yml", ""));
			this.state = PlayerManager.isOnline(p);
			this.lang = config.getString("language");
			this.money = config.getDouble("money");
			this.xp = config.getInt("xp");
			this.type = BanType.getType(config.getString("ban.type"));
			this.expires = getTimeFromString(config.getString("ban.expires"));
			if (!config.getString("ban.reason", "null")
					.equalsIgnoreCase("null")) {
				this.reason = config.getString("ban.reason");
			} else {
				this.reason = null;
			}
			if (!config.getString("ban.banner", "null")
					.equalsIgnoreCase("null")) {
				if (this.uuid == UUID
						.fromString(config.getString("ban.banner"))) {
					this.banner = null;
				} else {
					this.banner = PlayerManager.get(Bukkit
							.getOfflinePlayer(UUID.fromString(config
									.getString("ban.banner"))));
				}
			} else {
				this.banner = null;
			}
		}
	}

	public String getName() {
		return this.name;
	}

	private Time getTimeFromString(String s) {
		String[] a = s.split(",");
		if (!(s.equalsIgnoreCase("null")) && s != null) {
			int[] b = new int[6];
			for (int i = 0; i < 6; i++) {
				b[i] = Integer.parseInt(a[i]);
			}
			return new Time(b[0], b[1], b[2], b[3], b[4], b[5]);
		}
		return null;
	}

	public UUID getUUID() {
		return this.uuid;
	}

	public boolean getState() {
		return this.state;
	}

	public double getMoney() {
		return this.money;
	}

	public BanType getBanType() {
		return this.type;
	}

	public Time getExpires() {
		return this.expires;
	}

	public String getReason() {
		return this.reason;
	}

	public void save() {
		YamlConfiguration config = YamlConfiguration
				.loadConfiguration(FileManager.getPlayerFile(this.uuid));
		config.set("name", this.name);
		config.set("online",
				PlayerManager.isOnline(Bukkit.getPlayer(this.uuid)));
		if (Bukkit.getOfflinePlayer(this.uuid).isOnline())
			config.set("last_ip", Bukkit.getPlayer(this.uuid).getAddress()
					.getAddress().getHostAddress());
		config.set("language", this.lang);
		config.set("xp", this.xp);
		config.set("money", this.money);
		config.set("ban.type", this.type.toString());
		if (this.expires != null) {
			config.set("ban.expires", this.expires.toString());
		} else {
			config.set("ban.expires", "null");
		}
		if (this.reason != null) {
			config.set("ban.reason", this.reason);
		} else {
			config.set("ban.reason", "null");
		}
		if (this.banner != null) {
			config.set("ban.banner", this.banner.getUUID().toString());
		} else {
			config.set("ban.banner", "null");
		}

		try {
			config.save(FileManager.getPlayerFile(this.uuid));
		} catch (IOException e) {
		}
	}

	public int getXP() {
		return this.xp;
	}

	public int getPriority() {
		return this.priority;
	}

	public String getLang() {
		return this.lang;
	}

	public void setBanType(BanType bt) {
		this.type = bt;
	}

	public void setExpires(Time time) {
		this.expires = time;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setReason(Reasons reason) {
		this.reason = reason.toString();
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public void setXP(int xp) {
		this.xp = xp;
	}

	public void setState(boolean online) {
		this.state = online;
	}

	public IslandPlayer getBanner() {
		return this.banner;
	}

	public void setBanner(IslandPlayer ip) {
		this.banner = ip;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
}
