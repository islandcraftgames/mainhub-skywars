package me.islandcraft.mainhub.player;

import java.util.Collection;
import java.util.HashMap;

import javax.annotation.Nonnull;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

@SuppressWarnings("deprecation")
public class PlayerManager {

	private static HashMap<Player, IslandPlayer> playerRegistry = new HashMap<Player, IslandPlayer>();
	private static boolean started = false;

	public static IslandPlayer register(@Nonnull Player bukkitPlayer) {
		IslandPlayer ip = null;

		if (!playerRegistry.containsKey(bukkitPlayer)) {
			ip = new IslandPlayer(bukkitPlayer, false);
			playerRegistry.put(bukkitPlayer, ip);
		} else {
			ip = get(bukkitPlayer);
		}

		return ip;
	}

	public static IslandPlayer registerLogin(@Nonnull Player bukkitPlayer) {
		IslandPlayer ip = null;

		if (!playerRegistry.containsKey(bukkitPlayer)) {
			ip = new IslandPlayer(bukkitPlayer, true);
		} else {
			ip = get(bukkitPlayer);
		}

		return ip;
	}

	public static IslandPlayer unregister(@Nonnull Player bukkitPlayer) {
		return (IslandPlayer) playerRegistry.remove(bukkitPlayer);
	}

	public static IslandPlayer get(@Nonnull OfflinePlayer offlinePlayer) {
		IslandPlayer ip = (IslandPlayer) playerRegistry.get(offlinePlayer);
		if (ip != null)
			return ip;
		if(Bukkit.getPlayer(offlinePlayer.getUniqueId()) != null){
			return register(Bukkit.getPlayer(offlinePlayer.getUniqueId()));
		}else{
			return new IslandPlayer(offlinePlayer.getName());
		}
	}

	public static IslandPlayer unregister(@Nonnull String name) {
		for (IslandPlayer ip : playerRegistry.values()) {
			if (ip.getName().equalsIgnoreCase(name)) {
				playerRegistry.remove(Bukkit.getOfflinePlayer(name));
				return ip;

			}
		}
		return null;
	}

	public static IslandPlayer get(@Nonnull String name) {
		for (IslandPlayer ip : playerRegistry.values()) {
			if (ip.getName().equalsIgnoreCase(name))
				return ip;
		}
		return new IslandPlayer(name);
	}

	public static IslandPlayer getNull(@Nonnull String name) {
		for (IslandPlayer ip : playerRegistry.values()) {
			if (ip.getName().equalsIgnoreCase(name))
				return ip;
		}
		return null;
	}

	public static Collection<IslandPlayer> getAll() {
		return playerRegistry.values();
	}

	public static void shutdown() {
		for (IslandPlayer ip : playerRegistry.values()) {
			ip.save();
		}

		playerRegistry.clear();
	}

	public static boolean isOnline(Player p) {
		if (playerRegistry.containsKey(p))
			return true;
		return false;
	}

	public static boolean isOnline(String p) {
		if (Bukkit.getPlayer(p) != null)
			return true;
		return false;
	}

	public static void start() {
		if (started)
			return;
		playerRegistry.clear();
		for (Player p : Bukkit.getOnlinePlayers()) {
			register(p);
		}
		started = true;
	}

}
