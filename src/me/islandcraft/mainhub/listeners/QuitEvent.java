package me.islandcraft.mainhub.listeners;

import me.islandcraft.mainhub.player.IslandPlayer;
import me.islandcraft.mainhub.player.PlayerManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitEvent implements Listener {

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onQuit(PlayerQuitEvent e) {
		IslandPlayer ip = PlayerManager.get(e.getPlayer());
		ip.setState(false);
		ip.save();
		PlayerManager.unregister(e.getPlayer());
		e.setQuitMessage("");
	}

}
