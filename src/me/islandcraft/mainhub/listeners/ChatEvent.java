package me.islandcraft.mainhub.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import me.islandcraft.mainhub.events.PlayerSpeakEvent;
import me.islandcraft.mainhub.utils.ListUtils;
import net.md_5.bungee.api.ChatColor;

public class ChatEvent implements Listener {

	@SuppressWarnings("unchecked")
	@EventHandler
	public void changeChat(AsyncPlayerChatEvent e) {
		if(e.isCancelled())return;
		e.setCancelled(true);
		Player p = e.getPlayer();
		String message = e.getMessage();
		if (p.hasPermission("vip")) {
			message = ChatColor.translateAlternateColorCodes('&', message);
		}
		List<Player> listeners = ListUtils.setToList(e.getRecipients());
		List<Player> secundaryListeners = new ArrayList<Player>();
		String prefix = "�8[�6" + p.getLevel() + "�8]�r" + p.getDisplayName()
				+ "�8:�r ";
		String secundaryPrefix = "�8[Lobby][�6" + p.getLevel() + "�8]�r"
				+ p.getDisplayName() + "�8:�r ";

		PlayerSpeakEvent pse = new PlayerSpeakEvent(p, message, listeners,
				secundaryListeners, prefix, secundaryPrefix);
		
		Bukkit.getPluginManager().callEvent(pse);
		
		if(!pse.isCancelled()){
			for(Player p2 : pse.getListeners()){
				p2.sendMessage(pse.getPrefix() + pse.getMessage());
			}
			for(Player p2 : pse.getSecundaryListeners()){
				p2.sendMessage(pse.getSecundaryPrefix() + pse.getMessage());
			}
			System.out.println("<" + pse.getSecundaryPrefix() + "> " + pse.getMessage());
		}
	}
}
