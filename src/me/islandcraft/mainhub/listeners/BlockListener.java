package me.islandcraft.mainhub.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;

public class BlockListener implements Listener {

	@EventHandler
	public void onSignChange(SignChangeEvent e) {
		for (int i = 0; i < 4; i++) {
			String line = e.getLine(i);
			line = ChatColor.translateAlternateColorCodes('&', line);
			e.setLine(i, line);
		}
	}

	@EventHandler
	public void onRegeneration(EntityRegainHealthEvent e) {
		if (e.getRegainReason() == RegainReason.SATIATED
				|| e.getRegainReason() == RegainReason.REGEN
				|| e.getRegainReason() == RegainReason.CUSTOM) {
			e.setAmount(1);
		}
	}

}
