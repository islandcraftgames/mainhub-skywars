package me.islandcraft.mainhub.listeners;

import me.islandcraft.mainhub.Main;
import me.islandcraft.mainhub.bans.BanType;
import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.managers.BanManager;
import me.islandcraft.mainhub.managers.SbManager;
import me.islandcraft.mainhub.managers.TimeManager;
import me.islandcraft.mainhub.packetManagers.Tab;
import me.islandcraft.mainhub.packetManagers.Title;
import me.islandcraft.mainhub.player.IslandPlayer;
import me.islandcraft.mainhub.player.PlayerManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

public class JoinEvent implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		IslandPlayer ip = PlayerManager.register(e.getPlayer());
		ip.setState(true);
		ItemsMain.setStartItems(e.getPlayer(), true);
		e.setJoinMessage("");
		
		Main.get().reloadPrefix(e.getPlayer());

		new Title(e.getPlayer(), ChatColor.GREEN
				+ LanguageMain.get(e.getPlayer(), "main.title.welcome"),
				ChatColor.GOLD + "play.islandcraftgames.net", 1, 5, 1);
		new Tab(e.getPlayer(), ChatColor.GREEN + "IslandCraft", ChatColor.GOLD
				+ "play.islandcraftgames.net");
		SbManager.updateScoreboard(e.getPlayer());
		FileConfiguration storage = Main.get().getConfig();

		World world = Bukkit.getWorld(storage.getString("hub").split(", ")[3]);
		int x = Integer.parseInt(storage.getString("hub").split(", ")[0]);
		int y = Integer.parseInt(storage.getString("hub").split(", ")[1]);
		int z = Integer.parseInt(storage.getString("hub").split(", ")[2]);

		e.getPlayer().teleport(new Location(world, x + 0.5, y + 0.5, z + 0.5));
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onLogin(PlayerLoginEvent e) {
		IslandPlayer ip = PlayerManager.registerLogin(e.getPlayer());
		if (ip.getBanType() == null)
			return;
		if (ip.getBanType() == BanType.BAN) {
			if (ip.getExpires() == null) {
				e.disallow(Result.KICK_BANNED, LanguageMain.get(ip.getLang(),
						"main.ban.permanent", new String[] { ip.getReason() }));
			} else {
				if (ip.getExpires().isBefore(
						TimeManager.fromGregorianToTime(TimeManager
								.getCurrentTime()))) {
					ip.setBanType(BanType.NULL);
					ip.setExpires(null);
					ip.setReason("");
					return;
				}
				e.disallow(Result.KICK_BANNED, LanguageMain.get(
						ip.getLang(),
						"main.ban.temp",
						new String[] { BanManager.getTime(ip.getExpires()),
								ip.getReason() }));
			}
		}
	}
}
