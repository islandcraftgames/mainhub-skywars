package me.islandcraft.mainhub.listeners;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class BlockTimeEvent implements Runnable, Listener {

	@EventHandler(priority = EventPriority.NORMAL)
	public void onRainStart(WeatherChangeEvent event) {
		if (!event.isCancelled()) {
			if (event.toWeatherState()) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onStrike(LightningStrikeEvent e) {
		e.setCancelled(true);
	}

	@Override
	public void run() {
		for (World w : Bukkit.getWorlds()) {
			w.setTime(6000L);
		}
	}
}
