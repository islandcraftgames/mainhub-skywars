package me.islandcraft.mainhub.listeners;

import me.islandcraft.mainhub.Main;
import me.islandcraft.mainhub.player.IslandPlayer;
import me.islandcraft.mainhub.player.PlayerManager;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;

public class VotifierListener implements Listener {

	@EventHandler(priority = EventPriority.NORMAL)
	public void onVotifierEvent(VotifierEvent event) {
		Vote vote = event.getVote();
		Main.getEconomy().depositPlayer(vote.getUsername(), 400);
		IslandPlayer ip = PlayerManager.get(vote.getUsername());
		ip.setXP(ip.getXP() + 10);
		Bukkit.broadcastMessage("�6[Vote]�f Thanks �a" + vote.getUsername()
				+ "�f for voting for 400$ and 10XP!");
		if (!PlayerManager.isOnline(vote.getUsername()))
			ip.save();
	}

}
