package me.islandcraft.mainhub.listeners;

import me.islandcraft.mainhub.Main;
import net.alpenblock.bungeeperms.platform.bukkit.event.BungeePermsGroupChangedEvent;
import net.alpenblock.bungeeperms.platform.bukkit.event.BungeePermsReloadedEvent;
import net.alpenblock.bungeeperms.platform.bukkit.event.BungeePermsUserChangedEvent;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PrefixesListener implements Listener{
	
	@EventHandler
	public void onGroupUpdate(BungeePermsGroupChangedEvent e){
		Main.get().reloadPrefixes();
	}
	
	@EventHandler
	public void onUserUpdate(BungeePermsUserChangedEvent e){
		Main.get().reloadPrefixes();
	}
	
	@EventHandler
	public void onBPReload(BungeePermsReloadedEvent e){
		Main.get().reloadPrefixes();
	}

}
