package me.islandcraft.mainhub.listeners;

import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.Language;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.managers.BungeeManager;
import me.islandcraft.mainhub.player.PlayerManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.BannerMeta;

public class InvListener implements Listener {

	@EventHandler
	public void onItemClick(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR
				|| e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Player p = e.getPlayer();
			if (e.getItem() == null)
				return;
			if (!e.getItem().hasItemMeta())
				return;
			if (LanguageMain.isLangMessage(
					ChatColor.stripColor(e.getItem().getItemMeta()
							.getDisplayName()), "main.item.compass")) {
				p.openInventory(ItemsMain.getTravelInv(p));
				e.setCancelled(true);
				return;
			}
			if (LanguageMain.isLangMessage(
					ChatColor.stripColor(e.getItem().getItemMeta()
							.getDisplayName()), "main.item.changelang")) {
				p.openInventory(ItemsMain.getLangInv(p));
				e.setCancelled(true);
				return;
			}
			if (LanguageMain.isLangMessage(
					ChatColor.stripColor(e.getItem().getItemMeta()
							.getDisplayName()), "main.item.vote")) {
				p.performCommand("vote");
				e.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler
	public void onCompassMenuClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (LanguageMain.isLangMessage(
				ChatColor.stripColor(e.getInventory().getName()),
				"main.inv.travelto")) {
			e.setCancelled(true);
			switch (e.getCurrentItem().getType()) {
			case BEACON:
				p.performCommand("arcade");
				break;
			case LEAVES:
				p.closeInventory();
				Inventory inv = Bukkit.createInventory(null, 5 * 9,
						ChatColor.YELLOW + "Adventure Lobby");
				inv.setItem(22, ItemsMain.createItem(Material.LEAVES, 1,
						"žaAdventure Lobby"));
				inv.setItem(2, ItemsMain.createItem(Material.IRON_FENCE, 1,
						ChatColor.GREEN + "OP Prison"));
				inv.setItem(38, ItemsMain.createItem(
						Material.DIAMOND_SWORD,
						1,
						ChatColor.RED + "Full PvP",
						new String[] { ChatColor.GOLD
								+ LanguageMain.get(p, "main.commingsoon") }));
				inv.setItem(
						42,
						ItemsMain.createItem(Material.BARRIER, 1, ChatColor.RED
								+ "?????", new String[] { ChatColor.GOLD
								+ LanguageMain.get(p, "main.commingsoon") }));
				inv.setItem(
						6,
						ItemsMain.createItem(Material.TNT, 1, ChatColor.GREEN
								+ "Factions"));
				p.openInventory(inv);

				break;
			case SNOW_BLOCK:
				p.performCommand("sky");
				break;
			case DIAMOND_SWORD:
				p.performCommand("pvp");
				break;
			case COMPASS:
				p.performCommand("hub");
				break;
			default:
				break;
			}
		} else if (ChatColor.stripColor(e.getInventory().getName())
				.equalsIgnoreCase("Adventure Lobby")) {
			e.setCancelled(true);
			switch (e.getCurrentItem().getType()) {
			case LEAVES:
				p.performCommand("adventure");
				break;
			case IRON_FENCE:
				BungeeManager.sendToServer(p, "prison");
				break;
			case DIAMOND_SWORD:
				break;
			case BARRIER:
				break;
			case TNT:
				BungeeManager.sendToServer(p, "factions");
				break;
			default:
				break;
			}
		}
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		if (!p.hasPermission("admin")) {
			if (p.getWorld().getName().equalsIgnoreCase("world")) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onLangMenuClick(InventoryClickEvent e) {
		if (LanguageMain.isLangMessage(
				ChatColor.stripColor(e.getInventory().getName()),
				"main.inv.selectlang")) {
			Player p = (Player) e.getWhoClicked();
			e.setCancelled(true);
			BannerMeta flag = (BannerMeta) e.getCurrentItem().getItemMeta();
			if (flag == null)
				return;
			for (Language l : LanguageMain.getAllLangs()) {
				String s = l.getMeta().getDisplayName();
				if (ChatColor.stripColor(flag.getDisplayName())
						.equalsIgnoreCase(ChatColor.stripColor(s))) {
					PlayerManager.get(p).setLang(l.getName());
					p.sendMessage(ChatColor.GREEN + "Language changed to: "
							+ ChatColor.stripColor(flag.getDisplayName()));
					p.closeInventory();
					ItemsMain.setStartItems(p, false);
					break;
				}
			}
		}
	}
}
