package me.islandcraft.mainhub.items;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.islandcraft.mainhub.language.Language;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.player.PlayerManager;
import me.islandcraft.skywars.utilities.PlayerUtil;

public class ItemsMain {

	public static void setStartItems(Player p, boolean join) {
		if (join)
			if (p.hasPermission("staff"))
				return;
		Inventory inv = p.getInventory();

		PlayerUtil.clearInventory(p);

		inv.setItem(0, createItem(Material.COMPASS, 1, ChatColor.GREEN + LanguageMain.get(p, "main.item.compass")));
		inv.setItem(4, createItem(Material.DIAMOND, 1, ChatColor.AQUA + LanguageMain.get(p, "main.item.vote")));
		inv.setItem(8, createItem(Material.BANNER, 1, ChatColor.GREEN + LanguageMain.get(p, "main.item.changelang")));

		PlayerUtil.setTotalExperience(p, PlayerManager.get(p).getXP());
	}

	public static Inventory getTravelInv(Player p) {
		Inventory inv = Bukkit.createInventory(null, 45, ChatColor.GOLD + LanguageMain.get(p, "main.inv.travelto"));

		inv.setItem(2,
				ItemsMain.createItem(Material.BEACON, 1, ChatColor.GREEN + "Arcade Lobby", new String[] {
						ChatColor.AQUA + "CaptureTheFlag", ChatColor.AQUA + "CrazyRace " + ChatColor.GOLD + "[AlPHA]",
						ChatColor.AQUA + "PaintWars " + ChatColor.GOLD + "[BETA]",
						ChatColor.AQUA + "EggWars " + ChatColor.GOLD + LanguageMain.get(p, "main.commingsoon") }));
		inv.setItem(6,
				ItemsMain.createItem(Material.LEAVES, 1, ChatColor.GREEN + "Adventure Lobby",
						new String[] { ChatColor.AQUA + "Factions " + ChatColor.GOLD + "[BETA]",
								ChatColor.AQUA + "OP Prison " + ChatColor.GOLD + "[ALPHA]" }));
		inv.setItem(22, ItemsMain.createItem(Material.COMPASS, 1, ChatColor.GREEN + "Hub"));
		inv.setItem(38, ItemsMain.createItem(Material.SNOW_BLOCK, 1, ChatColor.GREEN + "Sky Lobby",
				new String[] { ChatColor.AQUA + "SkyWars" }));
		inv.setItem(42, ItemsMain.createItem(Material.DIAMOND_SWORD, 1, ChatColor.GREEN + "PvP Lobby",
				new String[] { ChatColor.AQUA + "1v1" }));
		return inv;
	}

	public static Inventory getLangInv(Player p) {
		Inventory inv = Bukkit.createInventory(null, getSlots(LanguageMain.getAllLangs().size()),
				LanguageMain.get(p, "main.inv.selectlang"));
		for (Language bm : LanguageMain.getAllLangs()) {
			inv.addItem(bm.getStack());
		}
		return inv;
	}

	private static int getSlots(int i) {
		int actual = 9;
		while (i > 9) {
			i = i - 9;
			actual = actual + 9;
		}
		return actual;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull String displayName) {
		ItemStack a = new ItemStack(material, amount);
		if (displayName.length() > 0) {
			ItemMeta b = a.getItemMeta();
			b.setDisplayName(displayName);
			a.setItemMeta(b);
		}
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull String displayName,
			@Nonnull String[] lore) {
		ItemStack a = new ItemStack(material, amount);
		ItemMeta b = a.getItemMeta();
		if (displayName.length() > 0)
			b.setDisplayName(displayName);
		List<String> c = new ArrayList<String>();
		for (String d : lore)
			c.add(d);
		b.setLore(c);
		a.setItemMeta(b);
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull int durability,
			@Nonnull String displayName) {
		ItemStack a = createItem(material, amount, displayName);
		a.setDurability((short) durability);
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull int durability,
			@Nonnull String displayName, @Nonnull String[] lore) {
		ItemStack a = createItem(material, amount, displayName, lore);
		a.setDurability((short) durability);
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull String displayName,
			@Nonnull Object[][] enchantments) {
		ItemStack a = createItem(material, amount, displayName);
		for (Object[] enc : enchantments) {
			a.addEnchantment((Enchantment) enc[0], (int) enc[1]);
		}
		return a;
	}

	public static ItemStack changeAmount(@Nonnull ItemStack itemStack, @Nonnull int amount) {
		ItemStack a = itemStack.clone();
		a.setAmount(amount);
		return a;
	}

}
