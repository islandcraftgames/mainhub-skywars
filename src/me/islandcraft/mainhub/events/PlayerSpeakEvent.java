package me.islandcraft.mainhub.events;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerSpeakEvent extends Event implements Cancellable {

	private static final HandlerList handlers = new HandlerList();
	private Player player;
	private String message;
	private List<Player> listeners;
	private List<Player> secundaryListeners;
	private String prefix;
	private String secundaryPrefix;
	private boolean cancelled;

	public PlayerSpeakEvent(Player player, String message, List<Player> listeners,
			List<Player> secundaryListeners, String prefix,
			String secundaryPrefix) {
		this.player = player;
		this.message = message;
		this.listeners = listeners;
		this.secundaryListeners = secundaryListeners;
		this.prefix = prefix;
		this.secundaryPrefix = secundaryPrefix;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean arg0) {
		this.cancelled = arg0;
	}
	
	public Player getPlayer(){
		return this.player;
	}

	public String getMessage() {
		return this.message;
	}

	public List<Player> getListeners() {
		return this.listeners;
	}

	public List<Player> getSecundaryListeners() {
		return this.secundaryListeners;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public String getSecundaryPrefix() {
		return this.secundaryPrefix;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public void setListeners(List<Player> listeners) {
		this.listeners = listeners;
	}

	public void setSecundaryListeners(List<Player> listeners) {
		this.secundaryListeners = listeners;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setSecundaryPrefix(String prefix) {
		this.secundaryPrefix = prefix;
	}

}
