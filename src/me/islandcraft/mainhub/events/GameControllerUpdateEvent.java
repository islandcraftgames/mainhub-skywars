package me.islandcraft.mainhub.events;

import me.islandcraft.gameapi.IGameController;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameControllerUpdateEvent extends Event {

	private static final HandlerList handlers = new HandlerList();
	private final IGameController gc;

	public GameControllerUpdateEvent(IGameController gc) {
		this.gc = gc;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public IGameController getGameController(){
		return this.gc;
	}

}
