package me.islandcraft.skywars.storage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.islandcraft.mainhub.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

public class SignStorage {

	public static void addSign(Location l, String mapName) {
		File directory = Main.get().getDataFolder();
		File signsFile = new File(directory, "signs.yml");
		if (!signsFile.exists()) {
			return;
		}
		YamlConfiguration signs = new YamlConfiguration();
		try {
			signs.load(signsFile);
			if (signs.getStringList("signs") != null) {
				List<String> a = signs.getStringList("signs");
				a.add(code(l, mapName));
				signs.set("signs", a);
			} else {
				List<String> a = new ArrayList<String>();
				a.add(code(l, mapName));
				signs.set("signs", a);
			}
			signs.save(signsFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	public static String getSignMap(Location l) {
		File directory = Main.get().getDataFolder();
		File signsFile = new File(directory, "signs.yml");
		if (!signsFile.exists()) {
			return "";
		}
		YamlConfiguration signs = new YamlConfiguration();
		try {
			signs.load(signsFile);
			if (signs.getStringList("signs") != null) {
				List<String> a = signs.getStringList("signs");
				for (String b : a) {
					if (decodeLocation(b) == l) {
						return decodeMapName(b);
					}
				}
			} else {
				return "";
			}
			signs.save(signsFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static boolean removeSign(Location l) {
		File directory = Main.get().getDataFolder();
		File signsFile = new File(directory, "signs.yml");
		if (!signsFile.exists()) {
			return false;
		}
		YamlConfiguration signs = new YamlConfiguration();
		try {
			signs.load(signsFile);
			if (signs.getStringList("signs") != null) {
				List<String> a = signs.getStringList("signs");
				for (String b : a) {
					if (decodeLocation(b).getWorld() == l.getWorld()
							&& decodeLocation(b).getBlockX() == l.getBlockX()
							&& decodeLocation(b).getBlockY() == l.getBlockY()
							&& decodeLocation(b).getBlockZ() == l.getBlockZ()) {
						a.remove(b);
						signs.set("signs", a);
						signs.save(signsFile);
						return true;
					}
				}
			} else {
				return false;
			}
			signs.save(signsFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isSign(Location l) {
		File directory = Main.get().getDataFolder();
		File signsFile = new File(directory, "signs.yml");
		if (!signsFile.exists()) {
			return false;
		}
		YamlConfiguration signs = new YamlConfiguration();
		try {
			signs.load(signsFile);
			if (signs.getStringList("signs") != null) {
				List<String> a = signs.getStringList("signs");
				for (String b : a) {
					if (decodeLocation(b).getWorld() == l.getWorld()
							&& decodeLocation(b).getBlockX() == l.getBlockX()
							&& decodeLocation(b).getBlockY() == l.getBlockY()
							&& decodeLocation(b).getBlockZ() == l.getBlockZ()) {
						return true;
					}
				}
			} else {
				return false;
			}
			signs.save(signsFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static String code(Location l, String mapName) {
		return l.getWorld().getName() + ", " + l.getBlockX() + ", "
				+ l.getBlockY() + ", " + (l.getBlockZ()) + ", " + mapName;
	}

	private static String decodeMapName(String a) {
		String[] b = a.split(", ");
		return b[4];
	}

	private static Location decodeLocation(String path) {
		String[] a = path.split(", ");
		return new Location(Bukkit.getWorld(a[0]), Integer.parseInt(a[1]),
				Integer.parseInt(a[2]), Integer.parseInt(a[3]));
	}

	public static Sign[] getAllSigns() {
		File directory = Main.get().getDataFolder();
		File signsFile = new File(directory, "signs.yml");
		if (!signsFile.exists()) {
			return null;
		}
		YamlConfiguration signs = new YamlConfiguration();
		try {
			signs.load(signsFile);
			if (signs.getStringList("signs") != null) {
				List<String> a = signs.getStringList("signs");
				Sign[] signList = new Sign[a.size()];
				int i = 0;
				for (String list : a) {
					Location l = decodeLocation(list);
					Sign sign = null;
					if (l.getBlock().getType() == Material.WALL_SIGN
							|| l.getBlock().getType() == Material.SIGN_POST) {
						sign = (Sign) l.getBlock().getState();
						signList[i] = sign;
					} else {
						a.remove(sign);
					}
					i++;
				}
				signs.save(signsFile);
				return signList;
			} else {
				return null;
			}
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
			return null;
		}
	}

}
