package me.islandcraft.skywars.storage;

import java.io.File;
import java.io.IOException;

import javax.annotation.Nonnull;

import me.islandcraft.mainhub.Main;
import me.islandcraft.skywars.kits.KitsMain;
import me.islandcraft.skywars.player.GamePlayer;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FlatFileStorage extends DataStorage {
	public void loadPlayer(@Nonnull GamePlayer player) {
		try {
			File playerDataDirectory = new File("./plugins/MainHub/players");

			if ((!playerDataDirectory.exists())
					&& (!playerDataDirectory.mkdirs())) {
				System.out.println("Failed to load player " + player
						+ ": Could not create player_data directory.");
				return;
			}

			File playerFile = new File(playerDataDirectory, player.getUUID()
					.toString() + ".yml");
			if ((!playerFile.exists()) && (!playerFile.createNewFile())) {
				System.out.println("Failed to load player " + player
						+ ": Could not create player file.");
				return;
			}

			FileConfiguration fileConfiguration = YamlConfiguration
					.loadConfiguration(playerFile);

			player.setGamesWon(fileConfiguration.getInt("skywars.wins", 0));
			player.setGamesPlayed(fileConfiguration.getInt("skywars.played", 0));
			player.setKills(fileConfiguration.getInt("skywars.kills", 0));
			player.setDeaths(fileConfiguration.getInt("skywars.deaths", 0));
			boolean[] kits = new boolean[KitsMain.getAllKits().size()];
			for (int i = 0; i < KitsMain.getAllKits().size(); i++) {
				kits[i] = fileConfiguration.getBoolean("skywars.kits."
						+ (i + 1));
			}
			player.setKits(kits);
		} catch (IOException ioException) {
			System.out.println("Failed to load player " + player.getName()
					+ ": " + ioException.getMessage());
		}
	}

	public void savePlayer(@Nonnull GamePlayer player) {
		try {
			File dataDirectory = Main.get().getDataFolder();
			File playerDataDirectory = new File(dataDirectory, "players");

			if ((!playerDataDirectory.exists())
					&& (!playerDataDirectory.mkdirs())) {
				System.out.println("Failed to save player " + player
						+ ": Could not create player_data directory.");
				return;
			}

			File playerFile = new File(playerDataDirectory, player.getUUID()
					.toString() + ".yml");
			if ((!playerFile.exists()) && (!playerFile.createNewFile())) {
				System.out.println("Failed to save player " + player
						+ ": Could not create player file.");
				return;
			}

			YamlConfiguration fileConfiguration = YamlConfiguration
					.loadConfiguration(playerFile);
			fileConfiguration.set("skywars.name", player.getName());
			fileConfiguration.set("skywars.wins",
					Integer.valueOf(player.getGamesWon()));
			fileConfiguration.set("skywars.played",
					Integer.valueOf(player.getGamesPlayed()));
			fileConfiguration.set("skywars.deaths",
					Integer.valueOf(player.getDeaths()));
			fileConfiguration.set("skywars.kills",
					Integer.valueOf(player.getKills()));
			boolean[] kits = player.getKits();
			for (int i = 0; i < kits.length; i++) {
				fileConfiguration.set("skywars.kits." + (i + 1), kits[i]);
			}
			fileConfiguration.save(playerFile);
		} catch (IOException ioException) {
			System.out.println("Failed to save player " + player.getName()
					+ ": " + ioException.getMessage());
		}
	}
}