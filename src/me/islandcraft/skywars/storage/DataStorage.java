package me.islandcraft.skywars.storage;

import me.islandcraft.skywars.player.GamePlayer;

public abstract class DataStorage {
	private static DataStorage instance;

	public abstract void loadPlayer(
			@javax.annotation.Nonnull GamePlayer paramGamePlayer);

	public abstract void savePlayer(
			@javax.annotation.Nonnull GamePlayer paramGamePlayer);
	
	public static DataStorage get() {
		if(instance == null){
			instance = new FlatFileStorage();
		}
		return instance;
	}
}