package me.islandcraft.skywars.commands;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import me.islandcraft.mainhub.Main;
import me.islandcraft.skywars.SkyWars;

public class VillagerCommand implements CommandExecutor {

	@CommandDescription("Spawns the shop")
	@CommandPermissions({ "skywars.command.villager" })
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		if (!(s instanceof Player)) {
			return false;
		}

		Player p = (Player) s;

		if (!p.isOp()) {
			return false;
		}

		World w = p.getWorld();

		UUID uuid = null;

		Entity e = w.spawnEntity(p.getLocation(), EntityType.VILLAGER);
		Villager v = (Villager) e;
		uuid = v.getUniqueId();
		v.setProfession(Profession.BLACKSMITH);
		v.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE,
				137), true);
		v.setCustomNameVisible(true);
		v.setCanPickupItems(false);
		v.setRemoveWhenFarAway(false);
		v.setVelocity(new Vector());
		v.setCustomName(ChatColor.YELLOW + SkyWars.getGameName());

		File folder = Main.get().getDataFolder();
		File villagersFile = new File(folder, "villagers.yml");

		if (!villagersFile.exists()) {
			try {
				villagersFile.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		YamlConfiguration villagers = new YamlConfiguration();
		try {
			villagers.load(villagersFile);
			villagers.set("UUID", uuid.toString());
			villagers.set("location", codeLoc(p.getLocation()));
			villagers.save(villagersFile);
		} catch (IOException | InvalidConfigurationException e1) {
			e1.printStackTrace();
		}

		return false;
	}

	public String codeLoc(Location l) {
		return l.getWorld().getName() + ", " + l.getX() + ", " + l.getY() + ", " + l.getZ() + ", " + l.getYaw() + ", "
				+ l.getPitch();
	}

}
