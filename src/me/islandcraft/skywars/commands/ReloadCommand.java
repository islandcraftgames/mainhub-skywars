package me.islandcraft.skywars.commands;

import me.islandcraft.mainhub.Main;
import me.islandcraft.mainhub.language.LanguageMain;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDescription("Reloads the chests, kits and the plugin.yml")
@CommandPermissions({ "skywars.command.reload" })
public class ReloadCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		Main.get().reloadConfig();

		sender.sendMessage(ChatColor.GREEN
				+ LanguageMain.get((Player) sender, "sw.reloadsuccess"));
		return true;
	}
}