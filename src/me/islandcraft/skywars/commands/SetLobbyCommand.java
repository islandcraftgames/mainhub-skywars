package me.islandcraft.skywars.commands;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.skywars.config.PluginConfig;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDescription("Set the lobby")
@CommandPermissions({ "skywars.command.setlobby" })
public class SetLobbyCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		PluginConfig.setLobbySpawn(((Player) sender).getLocation());
		sender.sendMessage(ChatColor.RED + LanguageMain.get((Player)sender,"sw.lobbyset"));
		return true;
	}
}