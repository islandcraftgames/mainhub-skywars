package me.islandcraft.skywars.commands;

import me.islandcraft.skywars.controllers.PlayerController;
import me.islandcraft.skywars.player.GamePlayer;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDescription("Leaves a SkyWars game")
public class LeaveCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		GamePlayer gamePlayer = PlayerController.get().get((Player) sender);

		if (!gamePlayer.isPlaying()) {
			sender.sendMessage(ChatColor.RED + "Voc� n�o est� a jogar!");
		} else {
			gamePlayer.getGame().onPlayerLeave(gamePlayer);
		}

		return true;
	}
}