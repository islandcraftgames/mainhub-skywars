package me.islandcraft.skywars.commands;


import me.islandcraft.gameapi.IGameState;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.skywars.config.PluginConfig;
import me.islandcraft.skywars.controllers.PlayerController;
import me.islandcraft.skywars.player.GamePlayer;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;


@CommandDescription("Starts a SkyWars game")
@CommandPermissions({ "skywars.command.start" })
public class StartCommand implements CommandExecutor
{
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args)
	{
		GamePlayer gamePlayer = PlayerController.get().get(
				(org.bukkit.entity.Player) sender);
		
		if (!gamePlayer.isPlaying()) {
			sender
					.sendMessage(ChatColor.RED + LanguageMain.get(gamePlayer, "sw.notplaying"));
			} else if (gamePlayer.getGame().getState() != IGameState.WAITING) {
			sender.sendMessage(ChatColor.RED + LanguageMain.get(gamePlayer, "sw.alreadystarted"));
			} else if (gamePlayer.getGame().getPlayerCount() < 2) {
			sender.sendMessage(ChatColor.RED
					+ LanguageMain.get(gamePlayer, "sw.insuficientplayers"));
			} else if ((PluginConfig.buildSchematic())
				&& (!gamePlayer.getGame().isBuilt())) {
			sender.sendMessage(ChatColor.RED
					+ LanguageMain.get(gamePlayer, "sw.creatingarena"));
			} else {
			gamePlayer.getGame().onGameStart();
			}
		
		return true;
		}
	
}