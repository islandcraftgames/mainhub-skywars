package me.islandcraft.skywars.controllers;

import java.util.Collection;
import java.util.Map;

import javax.annotation.Nonnull;

import me.islandcraft.gameapi.IGamePlayer;
import me.islandcraft.gameapi.IPlayerController;
import me.islandcraft.skywars.player.GamePlayer;
import me.islandcraft.skywars.storage.DataStorage;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

public class PlayerController extends IPlayerController {
	private Map<Player, GamePlayer> playerRegistry = Maps.newHashMap();
	private static PlayerController instance;

	private PlayerController() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			register(player);
		}
	}

	public GamePlayer register(@Nonnull Player bukkitPlayer) {
		GamePlayer gamePlayer = null;

		if (!this.playerRegistry.containsKey(bukkitPlayer)) {
			gamePlayer = new GamePlayer(bukkitPlayer.getUniqueId());
			this.playerRegistry.put(bukkitPlayer, gamePlayer);
		}

		return gamePlayer;
	}

	public GamePlayer unregister(@Nonnull Player bukkitPlayer) {
		return (GamePlayer) this.playerRegistry.remove(bukkitPlayer);
	}

	public GamePlayer get(@Nonnull Player bukkitPlayer) {
		return (GamePlayer) this.playerRegistry.get(bukkitPlayer);
	}

	public Collection<GamePlayer> getAll() {
		return this.playerRegistry.values();
	}

	public void shutdown() {
		for (GamePlayer gamePlayer : this.playerRegistry.values()) {
			DataStorage.get().savePlayer(gamePlayer);
		}

		this.playerRegistry.clear();
	}

	public static PlayerController get() {
		if (instance == null) {
			instance = new PlayerController();
		}

		return instance;
	}

	@Override
	public IGamePlayer getI(Player bukkitPlayer) {
		return (IGamePlayer) this.playerRegistry.get(bukkitPlayer);
	}

}