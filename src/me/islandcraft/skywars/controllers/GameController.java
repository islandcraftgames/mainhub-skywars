package me.islandcraft.skywars.controllers;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.Nonnull;

import me.islandcraft.gameapi.IGame;
import me.islandcraft.gameapi.IGameController;
import me.islandcraft.gameapi.IGameState;
import me.islandcraft.mainhub.Main;
import me.islandcraft.skywars.game.Game;

import com.google.common.collect.Lists;
import com.sk89q.worldedit.CuboidClipboard;

@SuppressWarnings("deprecation")
public class GameController extends IGameController {
	private static GameController instance;
	private List<IGame> gameList = Lists.newArrayList();
	private static boolean disabled = false;

	public Game findEmpty(String mapName, Integer partySize) {
		for (IGame game : this.gameList) {
			if ((game.getState() != IGameState.PLAYING) && (!game.isFull())
					&& (game.getSlots() - game.getPlayerCount()) >= partySize) {
				if (SchematicController.get().getName(game.getSchematic())
						.equalsIgnoreCase(mapName))
					return (Game) game;
			}
		}

		if (SchematicController.get().getMapNull(mapName) != null) {
			return create(mapName);
		}

		for (IGame game : this.gameList) {
			if ((game.getState() != IGameState.PLAYING) && (!game.isFull())
					&& (game.getSlots() - game.getPlayerCount()) >= partySize) {
				return (Game) game;
			}
		}

		return create(mapName);
	}

	public int getPlayersWaiting(String name) {
		int pWaiting = 0;

		if (SchematicController.get().getMapNull(name) != null) {
			if (this.gameList == null || this.gameList.size() == 0)
				return 0;
			for (IGame game : this.gameList) {
				if (SchematicController.get().getName(game.getSchematic())
						.equalsIgnoreCase(name))
					if (game.getState() == IGameState.WAITING) {
						pWaiting = pWaiting + game.getPlayerCount();
					}
			}
		} else {
			if (this.gameList == null || this.gameList.size() == 0)
				return 0;
			for (IGame game : this.gameList) {
				if (game.getState() == IGameState.WAITING) {
					pWaiting = pWaiting + game.getPlayerCount();
				}
			}
		}

		return pWaiting;
	}

	public int getPlayersPlaying(String name) {
		int pWaiting = 0;

		if (SchematicController.get().getMapNull(name) != null) {
			if (this.gameList == null || this.gameList.size() == 0)
				return 0;
			for (IGame game : this.gameList) {
				if (SchematicController.get().getName(game.getSchematic())
						.equalsIgnoreCase(name))
					if (game.getState() == IGameState.PLAYING) {
						pWaiting = pWaiting + game.getPlayerCount();
					}
			}
		} else {
			if (this.gameList == null || this.gameList.size() == 0)
				return 0;
			for (IGame game : this.gameList) {
				if (game.getState() == IGameState.PLAYING) {
					pWaiting = pWaiting + game.getPlayerCount();
				}
			}
		}

		return pWaiting;
	}

	public Game create(String mapName) {
		CuboidClipboard schematic = SchematicController.get().getMap(mapName);
		Game game = new Game(schematic);

		while (!game.isReady()) {
			String schematicName = SchematicController.get().getName(schematic);
			Main.get()
					.getLogger()
					.log(Level.SEVERE,
							String.format(
									"Schematic '%s' does not have any spawns set!",
									new Object[] { schematicName }));
			schematic = SchematicController.get().getMap(mapName);
			game = new Game(schematic);
		}

		this.gameList.add(game);
		return game;
	}

	public void remove(@Nonnull Game game) {
		this.gameList.remove(game);
	}

	public void shutdown() {
		if (gameList.size() == 0)
			return;
		for (IGame game : this.gameList) {
			game.onGameEnd();
		}
	}

	public Collection<IGame> getAll() {
		return this.gameList;
	}

	public static GameController get() {
		if (instance == null) {
			return instance = new GameController();
		}

		return instance;
	}

	public boolean canRestart() {
		for (IGame game : getAll()) {
			if (game.getPlayerCount() > 0)
				return false;
		}
		return true;
	}

	public void disableGames() {
		disabled = true;
	}

	public boolean isDisabled() {
		return disabled;
	}

}