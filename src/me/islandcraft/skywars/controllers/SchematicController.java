package me.islandcraft.skywars.controllers;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;

import me.islandcraft.mainhub.Main;
import me.islandcraft.skywars.utilities.LogUtils;

import org.bukkit.Material;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;

@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
public class SchematicController {
	private static SchematicController instance;
	private final Random random = new Random();

	private final Map<String, CuboidClipboard> schematicMap = Maps.newHashMap();
	private final Map<CuboidClipboard, Map<Integer, Vector>> spawnCache = Maps
			.newHashMap();
	private final Map<CuboidClipboard, List<Vector>> chestCache = Maps
			.newHashMap();
	private int schematicSize = 0;

	public SchematicController() {
		File dataDirectory = Main.get().getDataFolder();
		File schematicsDirectory = new File(dataDirectory, "schematics");

		if ((!schematicsDirectory.exists()) && (!schematicsDirectory.mkdirs())) {
			return;
		}

		File[] schematics = schematicsDirectory.listFiles();
		if (schematics == null) {
			return;
		}

		for (File schematic : schematics) {
			if (schematic.getName().endsWith(".schematic")) {

				if (!schematic.isFile()) {
					LogUtils.log(Level.INFO, getClass(),
							"Could not load schematic %s: Not a file",
							new Object[] { schematic.getName() });
				} else {
					SchematicFormat schematicFormat = SchematicFormat
							.getFormat(schematic);
					if (schematicFormat == null) {
						LogUtils.log(
								Level.INFO,
								getClass(),
								"Could not load schematic %s: Unable to determine schematic format",
								new Object[] { schematic.getName() });
					} else
						try {
							registerSchematic(
									schematic.getName().replace(".schematic",
											""),
									schematicFormat.load(schematic));
						} catch (DataException e) {
							LogUtils.log(
									Level.INFO,
									getClass(),
									"Could not load schematic %s: %s",
									new Object[] { schematic.getName(),
											e.getMessage() });
						} catch (IOException e) {
							LogUtils.log(
									Level.INFO,
									getClass(),
									"Could not load schematic %s: %s",
									new Object[] { schematic.getName(),
											e.getMessage() });
						}
				}
			}
		}
		LogUtils.log(Level.INFO, getClass(), "Registered %d schematics ...",
				new Object[] { Integer.valueOf(this.schematicSize) });
	}

	private void registerSchematic(final String name,
			final CuboidClipboard schematic) {
		org.bukkit.Bukkit.getScheduler().runTaskAsynchronously(Main.get(),
				new Runnable() {

					public void run() {
						int spawnId = 0;

						for (int y = 0; y < schematic.getSize().getBlockY(); y++) {
							for (int x = 0; x < schematic.getSize().getBlockX(); x++) {
								for (int z = 0; z < schematic.getSize()
										.getBlockZ(); z++) {
									Vector currentPoint = new Vector(x, y, z);
									int currentBlock = schematic.getPoint(
											currentPoint).getType();

									if (currentBlock != 0) {

										if ((currentBlock == Material.SIGN_POST
												.getId())
												|| (currentBlock == Material.BEACON
														.getId())) {
											SchematicController.this
													.cacheSpawn(schematic,
															spawnId++,
															currentPoint);
											schematic.setBlock(currentPoint,
													new BaseBlock(0));
										} else if (currentBlock == Material.CHEST
												.getId()) {
											SchematicController.this
													.cacheChest(schematic,
															currentPoint);
										}
									}
								}
							}
						}
						if (spawnId <= 1) {
							SchematicController.this.noSpawnsNotifier(name);
							return;
						}
						SchematicController.this.schematicMap.put(name,
								schematic);
					}

				});
		this.schematicSize += 1;
	}

	public CuboidClipboard getMap(String mapName) {
		List<CuboidClipboard> schematics = Lists.newArrayList(this.schematicMap
				.values());
		for (String a : this.schematicMap.keySet()) {
			if (a.equalsIgnoreCase(mapName)) {
				return this.schematicMap.get(a);
			}
		}
		return (CuboidClipboard) schematics.get(this.random.nextInt(schematics
				.size()));
	}

	public CuboidClipboard getMapNull(String mapName) {
		if (this.schematicMap.containsKey(mapName)) {
			return this.schematicMap.get(mapName);
		}
		return null;
	}

	public String getName(CuboidClipboard cuboidClipboard) {
		for (Map.Entry<String, CuboidClipboard> entry : this.schematicMap
				.entrySet()) {
			if (((CuboidClipboard) entry.getValue()).equals(cuboidClipboard)) {
				return (String) entry.getKey();
			}
		}

		return null;
	}

	public void cacheSpawn(CuboidClipboard schematic, int position,
			Vector location) {
		Map<Integer, Vector> spawnPlaces;
		if (this.spawnCache.containsKey(schematic)) {
			spawnPlaces = (Map) this.spawnCache.get(schematic);
		} else {
			spawnPlaces = Maps.newHashMap();
		}

		spawnPlaces.put(Integer.valueOf(position), location);
		this.spawnCache.put(schematic, spawnPlaces);
	}

	public void noSpawnsNotifier(String name) {
		LogUtils.log(
				Level.SEVERE,
				getClass(),
				String.format("Schematic '" + name
						+ "' does not have any spawns set!", new Object[0]),
				new Object[0]);
	}

	public Map<Integer, Vector> getCachedSpawns(CuboidClipboard schematic) {
		return (Map) this.spawnCache.get(schematic);
	}

	private void cacheChest(CuboidClipboard schematic, Vector location) {
		List<Vector> chestList;
		if (this.chestCache.containsKey(schematic)) {
			chestList = (List) this.chestCache.get(schematic);
		} else {
			chestList = Lists.newArrayList();
		}

		chestList.add(location);
		this.chestCache.put(schematic, chestList);
	}

	public Collection<Vector> getCachedChests(CuboidClipboard schematic) {
		return (Collection) this.chestCache.get(schematic);
	}

	public int size() {
		return this.schematicMap.size();
	}

	public void remove(String schematic) {
		this.schematicMap.remove(schematic);
	}

	public static SchematicController get() {
		if (instance == null) {
			instance = new SchematicController();
		}

		return instance;
	}

}