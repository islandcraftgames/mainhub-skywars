package me.islandcraft.skywars.controllers;

import java.util.List;

import me.islandcraft.gameapi.IKit;
import me.islandcraft.skywars.kits.KitsMain;
import me.islandcraft.skywars.player.GamePlayer;

public class KitManager {

	public static void getKits(GamePlayer gp) {
		List<IKit> a = KitsMain.getAllKits();
		boolean[] kits = gp.getKits();
		for (IKit kit : a) {
			if (kits[kit.getID()]) {
				gp.getBukkitPlayer().getInventory().addItem(kit.getItems());
			}
		}
	}

}
