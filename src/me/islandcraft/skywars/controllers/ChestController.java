package me.islandcraft.skywars.controllers;

import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;

public class ChestController{
	private static ChestController chestController;
	
public void populateChest(Chest chest) {
		Inventory inventory = chest.getBlockInventory();
		inventory.addItem(RandomController.randomSword());
		inventory.addItem(RandomController.randomHelmet());
		inventory.addItem(RandomController.randomChestplate());
		inventory.addItem(RandomController.randomLeggings());
		inventory.addItem(RandomController.randomBoots());
		inventory.addItem(RandomController.randomFood());
		inventory.addItem(RandomController.randomBow());
		inventory.addItem(RandomController.randomArrow());
		inventory.addItem(RandomController.randomCobblestone());
		inventory.addItem(RandomController.randomDirt());
		inventory.addItem(RandomController.randomTNT());
		inventory.addItem(RandomController.randomFlintAndSteel());
		inventory.addItem(RandomController.randomBucket());
		}

public static ChestController get() {
	if (chestController == null) {
			chestController = new ChestController();
		}
		return chestController;
	}
}
