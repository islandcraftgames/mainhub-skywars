package me.islandcraft.skywars.controllers;

import java.util.Map;
import java.util.Queue;

import me.islandcraft.gameapi.IWEUtils;
import me.islandcraft.mainhub.Main;
import me.islandcraft.skywars.SkyWars;
import me.islandcraft.skywars.config.PluginConfig;
import me.islandcraft.skywars.game.Game;

import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;

@SuppressWarnings({ "deprecation", "unused" })
public class WorldController {
	private static final int PASTE_HEIGHT = 75;
	private static WorldController worldController;
	private World islandWorld;
	private final Queue<int[]> islandReferences = com.google.common.collect.Lists
			.newLinkedList();
	private int nextId;
	private int prevOffset = 0;

	public WorldController() {
		generateGridReferences();
		this.islandWorld = createWorld();
	}

	private void generateGridReferences() {
		for (int xxx = 0; xxx < PluginConfig.getIslandsPerWorld(); xxx += 100) {
			for (int zzz = 0; zzz < PluginConfig.getIslandsPerWorld(); zzz += 100) {
				int[] coordinates = { xxx, zzz };

				if (!this.islandReferences.contains(coordinates)) {
					this.islandReferences.add(coordinates);
				}
			}
		}
	}

	public World create(Game game, CuboidClipboard schematic) {
		if (this.islandReferences.size() == 0) {
			me.islandcraft.skywars.utilities.LogUtils.log(
					java.util.logging.Level.INFO, getClass(),
					"No more free games left. Generating new world.",
					new Object[0]);

			generateGridReferences();
			this.islandWorld = createWorld();
		}

		int[] gridReference = (int[]) this.islandReferences.poll();
		game.setGridReference(gridReference);

		int gridX = gridReference[0];
		int gridZ = gridReference[1];
		int length = schematic.getLength();
		int width = schematic.getWidth();
		int islandSize = length > width ? length : width;
		int offsetX = schematic.getOffset().getBlockX();
		int offsetZ = schematic.getOffset().getBlockZ();
		int offset = offsetX < offsetZ ? offsetX : offsetZ;
		int buffer = PluginConfig.getIslandBuffer();

		int midX = gridX
				* (org.bukkit.Bukkit.getViewDistance() * 16 + 15 - offset
						+ this.prevOffset + buffer * 2);
		int midZ = gridZ
				* (org.bukkit.Bukkit.getViewDistance() * 16 + 15 - offset
						+ this.prevOffset + buffer * 2);

		game.setLocation(midX, midZ);

		this.prevOffset = (islandSize / 2);

		new IWEUtils(game, new Location(this.islandWorld, midX, 75.0D, midZ),
				schematic);

		java.util.Map<Integer, Vector> spawns = SchematicController.get()
				.getCachedSpawns(schematic);
		Vector isleLocation = new Vector(midX, 75, midZ);

		for (Map.Entry<Integer, Vector> entry : spawns.entrySet()) {
			Vector spawn = ((Vector) entry.getValue()).add(isleLocation).add(
					schematic.getOffset());
			Location location = new Location(this.islandWorld,
					spawn.getBlockX(), spawn.getBlockY(), spawn.getBlockZ());

			game.addSpawn(((Integer) entry.getKey()).intValue(), location);

			if ((PluginConfig.buildSchematic()) || (PluginConfig.buildCages())) {
				createSpawnHousing(location);
			}
		}

		java.util.Collection<Vector> chests = SchematicController.get()
				.getCachedChests(schematic);

		if (chests != null) {
			for (Vector location : chests) {
				Vector spawn = location.add(isleLocation).add(
						schematic.getOffset());
				Location chest = new Location(this.islandWorld,
						spawn.getBlockX(), spawn.getBlockY(), spawn.getBlockZ());

				game.addChest(chest);
			}
		}

		return this.islandWorld;
	}

	private void createSpawnHousing(Location location) {
		World world = location.getWorld();

		int x = location.getBlockX();
		int y = location.getBlockY();
		int z = location.getBlockZ();

		world.getBlockAt(x, y - 1, z).setType(Material.GLASS);
		world.getBlockAt(x, y + 3, z).setType(Material.GLASS);

		world.getBlockAt(x + 1, y, z).setType(Material.GLASS);
		world.getBlockAt(x + 1, y + 1, z).setType(Material.GLASS);
		world.getBlockAt(x + 1, y + 2, z).setType(Material.GLASS);

		world.getBlockAt(x - 1, y, z).setType(Material.GLASS);
		world.getBlockAt(x - 1, y + 1, z).setType(Material.GLASS);
		world.getBlockAt(x - 1, y + 2, z).setType(Material.GLASS);

		world.getBlockAt(x, y, z + 1).setType(Material.GLASS);
		world.getBlockAt(x, y + 1, z + 1).setType(Material.GLASS);
		world.getBlockAt(x, y + 2, z + 1).setType(Material.GLASS);

		world.getBlockAt(x, y, z - 1).setType(Material.GLASS);
		world.getBlockAt(x, y + 1, z - 1).setType(Material.GLASS);
		world.getBlockAt(x, y + 2, z - 1).setType(Material.GLASS);
	}

	private World createWorld() {
		String worldName = SkyWars.getGameAlias() + "-" + getNextId();
		World world = null;
		MultiverseCore mV = (MultiverseCore) Main.get().getServer()
				.getPluginManager().getPlugin("Multiverse-Core");
		if (/* mV != null */false) {
			if (mV.getMVWorldManager().loadWorld(worldName)) {
				return mV.getMVWorldManager().getMVWorld(worldName)
						.getCBWorld();
			}
			Boolean ret = Boolean.valueOf(mV.getMVWorldManager().addWorld(
					worldName, World.Environment.NORMAL, null,
					org.bukkit.WorldType.NORMAL, Boolean.valueOf(false),
					"MainHub", false));

			if (ret.booleanValue()) {
				MultiverseWorld mvWorld = mV.getMVWorldManager().getMVWorld(
						worldName);
				world = mvWorld.getCBWorld();
				mvWorld.setDifficulty(Difficulty.NORMAL);
				mvWorld.setPVPMode(true);
				mvWorld.setEnableWeather(false);
				mvWorld.setKeepSpawnInMemory(false);
				mvWorld.setAllowAnimalSpawn(false);
				mvWorld.setAllowMonsterSpawn(false);
			}
		}
		if (world == null) {
			WorldCreator worldCreator = new WorldCreator(worldName);
			worldCreator.environment(World.Environment.NORMAL);
			worldCreator.generateStructures(false);
			worldCreator.generator("MainHub");
			world = worldCreator.createWorld();
			world.setDifficulty(Difficulty.NORMAL);
			world.setSpawnFlags(false, false);
			world.setPVP(true);
			world.setStorm(false);
			world.setThundering(false);
			world.setWeatherDuration(Integer.MAX_VALUE);
			world.setKeepSpawnInMemory(false);
			world.setTicksPerAnimalSpawns(0);
			world.setTicksPerMonsterSpawns(0);
		}
		world.setAutoSave(false);
		//world.setGameRuleValue("doFireTick", "false");

		return world;
	}

	private int getNextId() {
		int id = this.nextId++;

		if (this.nextId == Integer.MAX_VALUE) {
			this.nextId = 0;
		}

		return id;
	}

	public static WorldController get() {
		if (worldController == null) {
			worldController = new WorldController();
		}

		return worldController;
	}

}