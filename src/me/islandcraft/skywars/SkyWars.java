package me.islandcraft.skywars;

import java.io.File;
import java.util.logging.Level;

import me.islandcraft.gameapi.IGameType;
import me.islandcraft.gameapi.controllers.ClassController;
import me.islandcraft.mainhub.Main;
import me.islandcraft.mainhub.economy.EcoMain;
import me.islandcraft.skywars.controllers.GameController;
import me.islandcraft.skywars.controllers.PlayerController;
import me.islandcraft.skywars.kits.KitsMain;
import me.islandcraft.skywars.listeners.EnchantListener;
import me.islandcraft.skywars.utilities.FileUtils;

import org.bukkit.Bukkit;
import org.bukkit.World;

import com.onarandombox.MultiverseCore.MultiverseCore;

public class SkyWars {
	private static SkyWars instance;
	private static EcoMain ecomain;
	private static final IGameType gameType = IGameType.SKYWARS;
	private static final String gameName = "SkyWars";
	private static final String gameAlias = "sw";
	private static final int shopRows = 5;

	public SkyWars() {
		onEnable();
	}

	public void onEnable() {
		instance = this;

		deleteIslandWorlds();
		ecomain = Main.getEconomy();
		KitsMain.setup();

		me.islandcraft.skywars.controllers.SchematicController.get();
		me.islandcraft.skywars.controllers.WorldController.get();
		ClassController.setGameController(gameType, GameController.get());
		ClassController.setPlayerController(gameType, PlayerController.get());
		me.islandcraft.skywars.controllers.ChestController.get();

		Bukkit.getPluginManager().registerEvents(
				new me.islandcraft.skywars.listeners.PlayerListener(),
				Main.get());
		Bukkit.getPluginManager().registerEvents(
				new me.islandcraft.skywars.listeners.EntityListener(),
				Main.get());
		Bukkit.getPluginManager().registerEvents(
				new me.islandcraft.skywars.listeners.BlockListener(),
				Main.get());
		Bukkit.getPluginManager().registerEvents(
				new me.islandcraft.skywars.listeners.VillagerListener(),
				Main.get());
		Bukkit.getPluginManager().registerEvents(new EnchantListener(),
				Main.get());

		Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.get(),
				new me.islandcraft.skywars.tasks.SyncTask(), 20L, 20L);
	}

	public void onDisable() {
		GameController.get().shutdown();
		PlayerController.get().shutdown();
		deleteIslandWorlds();
	}

	private void deleteIslandWorlds() {
		File workingDirectory = new File(".");
		File[] contents = workingDirectory.listFiles();

		if (contents != null) {
			for (File file : contents) {
				if ((file.isDirectory())
						&& (file.getName().matches(gameAlias + "-\\d+"))) {

					World world = Main.get().getServer()
							.getWorld(file.getName());
					Boolean result = Boolean.valueOf(false);
					if (Bukkit.getPluginManager().getPlugin("Multiverse-Core") != null) {
						MultiverseCore multiVerse = (MultiverseCore) Bukkit
								.getPluginManager()
								.getPlugin("Multiverse-Core");
						if (world != null) {
							try {
								result = Boolean.valueOf(multiVerse
										.getMVWorldManager().deleteWorld(
												world.getName()));
							} catch (IllegalArgumentException ignored) {
								result = Boolean.valueOf(false);
							}
						} else {
							result = Boolean.valueOf(multiVerse
									.getMVWorldManager().removeWorldFromConfig(
											file.getName()));
						}
					}
					if (!result.booleanValue()) {
						if (world != null) {
							result = Boolean.valueOf(Main.get().getServer()
									.unloadWorld(world, true));
							if (result.booleanValue() == true) {
								Main.get()
										.getLogger()
										.log(Level.INFO,
												"World ''{0}'' was unloaded from memory.",
												file.getName());
							} else {
								Main.get()
										.getLogger()
										.log(Level.SEVERE,
												"World ''{0}'' could not be unloaded.",
												file.getName());
							}
						}
						result = Boolean.valueOf(FileUtils.deleteFolder(file));
						if (result.booleanValue() == true) {
							Main.get()
									.getLogger()
									.log(Level.INFO,
											"World ''{0}'' was deleted.",
											file.getName());
						} else {
							Main.get()
									.getLogger()
									.log(Level.SEVERE,
											"World ''{0}'' was NOT deleted.",
											file.getName());
							Main.get()
									.getLogger()
									.log(Level.SEVERE,
											"Are you sure the folder {0} exists?",
											file.getName());
							Main.get()
									.getLogger()
									.log(Level.SEVERE,
											"Please check your file permissions on ''{0}''",
											file.getName());
						}
					}
				}
			}
		}

		workingDirectory = new File("./plugins/WorldGuard/worlds/");
		contents = workingDirectory.listFiles();

		if (contents != null) {
			for (File file : contents) {
				if ((file.isDirectory())
						&& (file.getName().matches(gameAlias + "-\\d+"))) {
					FileUtils.deleteFolder(file);
				}
			}
		}
	}

	public static SkyWars get() {
		return instance;
	}

	public static EcoMain getEconomy() {
		return ecomain;
	}

	public static IGameType getGameType() {
		return gameType;
	}

	public static String getGameName() {
		return gameName;
	}

	public static String getGameAlias() {
		return gameAlias;
	}

	public static int getShopSize() {
		return shopRows * 9;
	}

}