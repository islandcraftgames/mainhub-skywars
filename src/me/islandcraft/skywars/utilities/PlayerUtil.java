package me.islandcraft.skywars.utilities;

import javax.annotation.Nonnull;

import me.islandcraft.mainhub.player.PlayerManager;

import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;

public class PlayerUtil {
	public static void refreshPlayer(@Nonnull Player player) {
		if (!player.isDead()) {
			player.setHealth(20.0D);
			player.setFoodLevel(20);
		}
		
		player.setFlySpeed(getRealMoveSpeed(getMoveSpeed("1"), true));
		player.setWalkSpeed(getRealMoveSpeed(getMoveSpeed("1"), false));

		player.setFireTicks(0);
		setTotalExperience(player, PlayerManager.get(player).getXP());

		removePotionEffects(player);

		player.updateInventory();
		player.setFlying(false);
		player.setAllowFlight(false);
	}

	public static void clearInventory(@Nonnull Player pPlayer) {
		pPlayer.closeInventory();

		PlayerInventory inventory = pPlayer.getInventory();
		inventory.setArmorContents(null);
		inventory.clear();

		for (int iii = 0; iii < inventory.getSize(); iii++) {
			inventory.clear(iii);
		}

		pPlayer.updateInventory();
	}

	public static void removePotionEffects(@Nonnull Player pPlayer) {
		for (PotionEffect potionEffect : pPlayer.getActivePotionEffects()) {
			pPlayer.removePotionEffect(potionEffect.getType());
		}
	}

	public static void setTotalExperience(Player player, int exp) {
		if (exp < 0) {
			throw new IllegalArgumentException("Experience is negative!");
		}
		player.setExp(0.0F);
		player.setLevel(0);
		player.setTotalExperience(0);

		int amount = exp;
		while (amount > 0) {
			int expToLevel = getExpAtLevel(player);
			amount -= expToLevel;
			if (amount >= 0) {
				player.giveExp(expToLevel);
			} else {
				amount += expToLevel;
				player.giveExp(amount);
				amount = 0;
			}
		}
	}

	private static int getExpAtLevel(Player player) {
		return getExpAtLevel(player.getLevel());
	}

	public static int getExpAtLevel(int level) {
		if (level > 29) {
			return 62 + (level - 30) * 7;
		}
		if (level > 15) {
			return 17 + (level - 15) * 3;
		}
		return 17;
	}

	public static int getExpToLevel(int level) {
		int currentLevel = 0;
		int exp = 0;
		while (currentLevel < level) {
			exp += getExpAtLevel(currentLevel);
			currentLevel++;
		}
		if (exp < 0) {
			exp = 2147483647;
		}
		return exp;
	}

	public static int getTotalExperience(Player player) {
		int exp = Math.round(getExpAtLevel(player) * player.getExp());
		int currentLevel = player.getLevel();
		while (currentLevel > 0) {
			currentLevel--;
			exp += getExpAtLevel(currentLevel);
		}
		if (exp < 0) {
			exp = 2147483647;
		}
		return exp;
	}

	public static int getExpUntilNextLevel(Player player) {
		int exp = Math.round(getExpAtLevel(player) * player.getExp());
		int nextLevel = player.getLevel();
		return getExpAtLevel(nextLevel) - exp;
	}
	
	private static float getMoveSpeed(String moveSpeed){
		float userSpeed;
		try {
			userSpeed = Float.parseFloat(moveSpeed);
			if (userSpeed > 10.0F) {
				userSpeed = 10.0F;
			} else if (userSpeed < 1.0E-004F) {
				userSpeed = 1.0E-004F;
			}
		} catch (NumberFormatException e) {
			return 0;
		}
		return userSpeed;
	}

	private static float getRealMoveSpeed(float userSpeed, boolean isFly) {
		float defaultSpeed = isFly ? 0.1F : 0.2F;
		float maxSpeed = 1.0F;
		if (userSpeed < 1.0F) {
			return defaultSpeed * userSpeed;
		}
		float ratio = (userSpeed - 1.0F) / 9.0F * (maxSpeed - defaultSpeed);
		return ratio + defaultSpeed;
	}
}
