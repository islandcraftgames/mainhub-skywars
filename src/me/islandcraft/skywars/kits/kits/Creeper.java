package me.islandcraft.skywars.kits.kits;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Creeper extends IKit {

	public Creeper() {
		setID(3);
		setLangCode("sw.kit3");
		setItems(new ItemStack[] { new ItemStack(Material.TNT, 10), new ItemStack(Material.FLINT_AND_STEEL, 1) });
		setShowCaseItem(new ItemStack(Material.TNT, 1));
		setPrice(750);
		setBeforeKit(null);
		setSlot(13);
		setPermission("");
	}

}
