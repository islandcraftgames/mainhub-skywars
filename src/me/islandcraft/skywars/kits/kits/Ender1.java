package me.islandcraft.skywars.kits.kits;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Ender1 extends IKit {

	public Ender1() {
		setID(8);
		setLangCode("sw.kit8");
		setItems(new ItemStack[] { new ItemStack(Material.ENDER_PEARL, 1) });
		setShowCaseItem(new ItemStack(Material.ENDER_PEARL, 1));
		setPrice(2000);
		setBeforeKit(null);
		setSlot(16);
		setPermission("");
	}

}
