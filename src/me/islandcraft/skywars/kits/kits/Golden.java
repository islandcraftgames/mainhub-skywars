package me.islandcraft.skywars.kits.kits;

import java.util.Random;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Golden extends IKit {

	public Golden() {
		setID(6);
		setLangCode("sw.kit6");
		setItems(new ItemStack[] { new ItemStack(Material.GOLDEN_APPLE, 3) });
		setShowCaseItem(new ItemStack(Material.GOLDEN_APPLE, 3));
		setPrice(3000);
		setBeforeKit(null);
		setSlot(31);
		setPermission("");
	}

	public ItemStack[] getItems() {
		Random r = new Random();
		if (r.nextInt(10) == 5) {
			return this.items;
		}
		return new ItemStack[] { new ItemStack(Material.AIR) };
	}

}
