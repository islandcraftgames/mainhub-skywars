package me.islandcraft.skywars.kits.kits;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Snow2 extends IKit {

	public Snow2() {
		setID(1);
		setLangCode("sw.kit1");
		setItems(new ItemStack[] { new ItemStack(Material.SNOW_BALL, 5) });
		setShowCaseItem(new ItemStack(Material.SNOW_BALL, 10));
		setPrice(2000);
		setBeforeKit(new Snow1());
		setSlot(19);
		setPermission("");
	}

}
