package me.islandcraft.skywars.kits.kits;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Ender2 extends IKit {

	public Ender2() {
		setID(9);
		setLangCode("sw.kit9");
		setItems(new ItemStack[] { new ItemStack(Material.ENDER_PEARL, 1) });
		setShowCaseItem(new ItemStack(Material.ENDER_PEARL, 2));
		setPrice(4000);
		setBeforeKit(new Ender1());
		setSlot(25);
		setPermission("");
	}

}
