package me.islandcraft.skywars.kits.kits;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Ender3 extends IKit {

	public Ender3() {
		setID(10);
		setLangCode("sw.kit10");
		setItems(new ItemStack[] { new ItemStack(Material.ENDER_PEARL, 1) });
		setShowCaseItem(new ItemStack(Material.ENDER_PEARL, 3));
		setPrice(6000);
		setBeforeKit(new Ender2());
		setSlot(34);
		setPermission("");
	}

}
