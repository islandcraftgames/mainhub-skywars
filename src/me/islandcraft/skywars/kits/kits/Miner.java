package me.islandcraft.skywars.kits.kits;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Miner extends IKit {

	public Miner() {
		setID(7);
		setLangCode("sw.kit7");
		setItems(new ItemStack[] { new ItemStack(Material.IRON_PICKAXE, 1) });
		setShowCaseItem(new ItemStack(Material.IRON_PICKAXE, 1));
		setPrice(1250);
		setBeforeKit(null);
		setSlot(23);
		setPermission("");
	}

}
