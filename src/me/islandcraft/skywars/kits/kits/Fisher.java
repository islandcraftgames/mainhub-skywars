package me.islandcraft.skywars.kits.kits;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Fisher extends IKit {

	public Fisher() {
		setID(4);
		setLangCode("sw.kit4");
		setItems(new ItemStack[] { new ItemStack(Material.FISHING_ROD, 1) });
		setShowCaseItem(new ItemStack(Material.FISHING_ROD, 1));
		setPrice(500);
		setBeforeKit(null);
		setSlot(21);
		setPermission("");
	}

}
