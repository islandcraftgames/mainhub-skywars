package me.islandcraft.skywars.kits.kits;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Snow1 extends IKit {

	public Snow1() {
		setID(0);
		setLangCode("sw.kit0");
		setItems(new ItemStack[] { new ItemStack(Material.SNOW_BALL, 5) });
		setShowCaseItem(new ItemStack(Material.SNOW_BALL, 5));
		setPrice(1000);
		setBeforeKit(null);
		setSlot(10);
		setPermission("");
	}

}
