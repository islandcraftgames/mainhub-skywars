package me.islandcraft.skywars.kits.kits;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Archer extends IKit {

	public Archer() {
		setID(5);
		setLangCode("sw.kit5");
		setItems(new ItemStack[] { new ItemStack(Material.BOW, 1),
				new ItemStack(Material.ARROW, 7) });
		setShowCaseItem(new ItemStack(Material.BOW, 1));
		setPrice(1500);
		setBeforeKit(null);
		setSlot(22);
		setPermission("");
	}

}
