package me.islandcraft.skywars.kits.kits;

import me.islandcraft.gameapi.IKit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Snow3 extends IKit {

	public Snow3() {
		setID(2);
		setLangCode("sw.kit2");
		setItems(new ItemStack[] { new ItemStack(Material.SNOW_BALL, 6) });
		setShowCaseItem(new ItemStack(Material.SNOW_BALL, 16));
		setPrice(3000);
		setBeforeKit(new Snow2());
		setSlot(28);
		setPermission("");
	}

}
