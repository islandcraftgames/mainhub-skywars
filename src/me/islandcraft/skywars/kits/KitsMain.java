package me.islandcraft.skywars.kits;

import java.util.List;

import me.islandcraft.gameapi.IKit;
import me.islandcraft.skywars.kits.kits.Archer;
import me.islandcraft.skywars.kits.kits.Creeper;
import me.islandcraft.skywars.kits.kits.Ender1;
import me.islandcraft.skywars.kits.kits.Ender2;
import me.islandcraft.skywars.kits.kits.Ender3;
import me.islandcraft.skywars.kits.kits.Fisher;
import me.islandcraft.skywars.kits.kits.Golden;
import me.islandcraft.skywars.kits.kits.Miner;
import me.islandcraft.skywars.kits.kits.Snow1;
import me.islandcraft.skywars.kits.kits.Snow2;
import me.islandcraft.skywars.kits.kits.Snow3;

import com.google.common.collect.Lists;

public class KitsMain {

	private static List<IKit> kitList = Lists.newArrayList();
	private static boolean settedup = false;

	public static List<IKit> getAllKits() {
		return kitList;
	}
	
	public static void setup(){
		if(settedup) return;
		
		kitList.add(new Snow1());
		kitList.add(new Snow2());
		kitList.add(new Snow3());
		kitList.add(new Creeper());
		kitList.add(new Fisher());
		kitList.add(new Archer());
		kitList.add(new Golden());
		kitList.add(new Miner());
		kitList.add(new Ender1());
		kitList.add(new Ender2());
		kitList.add(new Ender3());
	}
}
