package me.islandcraft.skywars.game;

import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.inventivetalent.bossbar.BossBarAPI;
import org.primesoft.asyncworldedit.blockPlacer.BlockPlacerPlayer;
import org.primesoft.asyncworldedit.playerManager.PlayerEntry;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sk89q.worldedit.CuboidClipboard;

import me.islandcraft.gameapi.IGame;
import me.islandcraft.gameapi.IGamePlayer;
import me.islandcraft.gameapi.IGameState;
import me.islandcraft.gameapi.IWEUtils;
import me.islandcraft.gameapi.Spectable;
import me.islandcraft.mainhub.Main;
import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.managers.SbManager;
import me.islandcraft.mainhub.player.IslandPlayer;
import me.islandcraft.mainhub.player.PlayerManager;
import me.islandcraft.skywars.SkyWars;
import me.islandcraft.skywars.config.PluginConfig;
import me.islandcraft.skywars.controllers.GameController;
import me.islandcraft.skywars.controllers.KitManager;
import me.islandcraft.skywars.controllers.PlayerController;
import me.islandcraft.skywars.controllers.WorldController;
import me.islandcraft.skywars.player.GamePlayer;
import me.islandcraft.skywars.utilities.CraftBukkitUtil;
import me.islandcraft.skywars.utilities.PlayerUtil;

@SuppressWarnings({ "deprecation", "unused" })
public class Game extends IGame implements Spectable {

	private IGameState gameState;
	private Map<Integer, GamePlayer> idPlayerMap = Maps.newLinkedHashMap();
	private Map<GamePlayer, Integer> playerIdMap = Maps.newHashMap();
	private Map<GamePlayer, Integer> fixplayerIdMap = Maps.newHashMap();
	private int playerCount = 0;
	private int slots;
	private Map<Integer, Location> spawnPlaces = Maps.newHashMap();
	private List<GamePlayer> spectators = Lists.newArrayList();
	private Map<GamePlayer, Integer> moneyToGet = Maps.newHashMap();
	private Map<GamePlayer, Integer> xpToGet = Maps.newHashMap();
	private List<GamePlayer> toJoin = Lists.newArrayList();

	private int timer;
	private boolean built;
	private CuboidClipboard schematic;
	private World world;
	private int[] islandReference;
	private org.bukkit.util.Vector minLoc;
	private org.bukkit.util.Vector maxLoc;
	private List<Location> chestList = com.google.common.collect.Lists.newArrayList();
	private IWEUtils iweUtils;
	private BlockPlacerPlayer bpp;
	private PlayerEntry pe;

	public Game(CuboidClipboard schematic) {
		this.schematic = schematic;
		this.world = WorldController.get().create(this, schematic);
		this.slots = this.spawnPlaces.size();
		this.gameState = IGameState.WAITING;
		this.timer = 60;

		for (int iii = 0; iii < this.slots; iii++) {
			this.idPlayerMap.put(Integer.valueOf(iii), null);
		}
	}

	public boolean isBuilt() {
		return this.built;
	}

	public CuboidClipboard getSchematic() {
		return this.schematic;
	}

	public void setBuilt(boolean built) {
		if (built == true) {
			setTimer(61);
			if (toJoin.size() != 0)
				for (GamePlayer gp : toJoin) {
					if (BossBarAPI.hasBar(gp.getBukkitPlayer()))
						BossBarAPI.removeBar(gp.getBukkitPlayer());
					onPlayerJoin(gp);
				}
			toJoin.clear();
			final IWEUtils a = this.iweUtils;
			Bukkit.getScheduler().runTaskLater(Main.get(), new Runnable() {
				@Override
				public void run() {
					a.shutdown();
				}
			}, 1L);
		}
		this.built = built;
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}

	public void addChest(Location location) {
		this.chestList.add(location);
	}

	public void removeChest(Location location) {
		this.chestList.remove(location);
	}

	public boolean isChest(Location location) {
		return this.chestList.contains(location);
	}

	public boolean isReady() {
		return this.slots >= 2;
	}

	public World getWorld() {
		return this.world;
	}

	public void setGridReference(int[] gridReference) {
		this.islandReference = gridReference;
	}

	public int[] getGridReference() {
		return this.islandReference;
	}

	public int getSlots() {
		return this.slots;
	}

	public void setLocation(int midX, int midZ) {
		int minX = midX + this.schematic.getOffset().getBlockX() + 1;
		int minZ = midZ + this.schematic.getOffset().getBlockZ() + 1;
		int maxX = minX + this.schematic.getWidth() - 2;
		int maxZ = minZ + this.schematic.getLength() - 2;
		int buffer = PluginConfig.getIslandBuffer();
		this.minLoc = new org.bukkit.util.Vector(minX - buffer, Integer.MIN_VALUE, minZ - buffer);
		this.maxLoc = new org.bukkit.util.Vector(maxX + buffer, Integer.MAX_VALUE, maxZ + buffer);
	}

	public org.bukkit.util.Vector getMinLoc() {
		return this.minLoc;
	}

	public org.bukkit.util.Vector getMaxLoc() {
		return this.maxLoc;
	}

	public void onPlayerLogin(GamePlayer gp) {
		Player player = gp.getBukkitPlayer();
		gp.setGame(this);
		if (!isBuilt()) {
			player.sendMessage(LanguageMain.get(player, "game.building"));
			toJoin.add(gp);
			return;
		} else {
			onPlayerJoin(gp);
		}

	}

	public void onPlayerJoin(GamePlayer gamePlayer) {
		Player player = gamePlayer.getBukkitPlayer();

		int id = getFistEmpty();
		this.playerCount++;
		this.idPlayerMap.put(Integer.valueOf(getFistEmpty()), gamePlayer);
		this.playerIdMap.put(gamePlayer, Integer.valueOf(id));
		sendMessage("sw.joinarena", new String[] { player.getDisplayName(), Integer.toString(getPlayerCount()),
				Integer.toString(this.slots) });

		PlayerUtil.refreshPlayer(player);
		PlayerUtil.clearInventory(player);

		if (player.getGameMode() != org.bukkit.GameMode.SURVIVAL) {
			player.setGameMode(org.bukkit.GameMode.SURVIVAL);
		}

		gamePlayer.setSkipFallDamage(true);
		player.teleport(getSpawn(id).clone().add(0.5D, 0.5D, 0.5D));
		gamePlayer.setGame(this);

		Plugin commandBook = Main.get().getServer().getPluginManager().getPlugin("CommandBook");
		Plugin worldGuard = Main.get().getServer().getPluginManager().getPlugin("WorldGuard");
		if (player.hasMetadata("god")) {
			if ((commandBook != null) && ((commandBook instanceof com.sk89q.commandbook.CommandBook))) {
				player.removeMetadata("god", commandBook);
			}
			if ((worldGuard != null) && ((worldGuard instanceof com.sk89q.worldguard.bukkit.WorldGuardPlugin))) {
				player.removeMetadata("god", worldGuard);
			}
		}
		getCompass(player);
		KitManager.getKits(gamePlayer);
		gamePlayer.setEnderPearldDelay(60);
		player.updateInventory();
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (!this.playerIdMap.containsKey(PlayerController.get().get(p)))
				player.hidePlayer(p);
		}
		for (IGamePlayer p : getPlayers()) {
			p.getBukkitPlayer().showPlayer(player);
		}
	}

	public void onPlayerLeave(IGamePlayer gamePlayer) {
		onPlayerLeave((GamePlayer) gamePlayer, true, true, false);
	}

	public void onPlayerLeave(GamePlayer gamePlayer, boolean message, boolean process, boolean spectate) {
		Player player = gamePlayer.getBukkitPlayer();

		if (player.isDead())
			CraftBukkitUtil.forceRespawn(player);
		PlayerUtil.refreshPlayer(player);
		PlayerUtil.clearInventory(player);

		if (spectators.contains(gamePlayer)) {
			gamePlayer.setGame(null);

			player.setGameMode(GameMode.SURVIVAL);
			player.setAllowFlight(false);
			player.setFlying(false);
			player.teleport(PluginConfig.getLobbySpawn());
			SbManager.updateScoreboard(player);
			for (Player p : Bukkit.getOnlinePlayers()) {
				if (p != player) {
					player.showPlayer(p);
				}
			}
			for (IGamePlayer p : getPlayers()) {
				p.getBukkitPlayer().hidePlayer(player);
			}
			ItemsMain.setStartItems(player, false);
			spectators.remove(gamePlayer);
		} else {
			if (isBuilt())
				this.playerCount--;
			if (message) {
				if ((this.gameState == IGameState.PLAYING)) {
					sendMessage("sw.leftingame",
							new String[] { player.getDisplayName(), Integer.toString(getPlayerCount()) });
				} else {
					sendMessage("sw.left", new String[] { player.getDisplayName(), Integer.toString(getPlayerCount()),
							Integer.toString(this.slots) });
				}
			}
			this.idPlayerMap.put(this.playerIdMap.remove(gamePlayer), null);
			if (spectate) {
				spectators.add(gamePlayer);
				setSpectator(gamePlayer);
				for (GamePlayer p : spectators) {
					p.getBukkitPlayer().hidePlayer(player);
				}
			} else {
				gamePlayer.setGame(null);
				player.setGameMode(GameMode.SURVIVAL);
				player.setAllowFlight(false);
				player.setFlying(false);
				player.teleport(PluginConfig.getLobbySpawn());
				SbManager.updateScoreboard(player);
				for (Player p : Bukkit.getOnlinePlayers()) {
					if (p != player) {
						player.showPlayer(p);
					}
				}
				ItemsMain.setStartItems(player, false);
				for (IGamePlayer p : getPlayers()) {
					p.getBukkitPlayer().hidePlayer(player);
				}
				for (GamePlayer p : spectators) {
					p.getBukkitPlayer().hidePlayer(player);
				}
			}
		}

		for (IGamePlayer igp : getPlayers()) {
			GamePlayer gp = (GamePlayer) igp;
			registerScoreboard(gp);
		}

		for (GamePlayer gp : spectators) {
			registerScoreboard(gp);
		}

		if ((process) && (this.gameState == IGameState.PLAYING) && (this.playerCount == 1))
			onGameEnd(getWinner());
	}

	public void onPlayerDeath(GamePlayer gamePlayer, PlayerDeathEvent event) {
		Player player = gamePlayer.getBukkitPlayer();
		Player killer = player.getKiller();

		gamePlayer.setDeaths(gamePlayer.getDeaths() + 1);
		if (moneyToGet.get(gamePlayer) != null) {
			SkyWars.getEconomy().depositPlayer(gamePlayer.getBukkitPlayer().getName(), moneyToGet.get(gamePlayer));
			IslandPlayer ip = PlayerManager.get(player);
			ip.setXP(ip.getXP() + xpToGet.get(gamePlayer));
			player.sendMessage(ChatColor.GOLD + "------------------------");
			player.sendMessage(ChatColor.GOLD + LanguageMain.get(player, "sw.getcredits",
					new String[] { Integer.toString(moneyToGet.get(gamePlayer)) }));
			player.sendMessage(ChatColor.GOLD + "------------------------");
		} else {
			player.sendMessage(ChatColor.GOLD + "------------------------");
			player.sendMessage(ChatColor.GOLD + LanguageMain.get(player, "sw.getcredits", new String[] { "0" }));
			player.sendMessage(ChatColor.GOLD + "------------------------");
		}

		if (killer != null) {
			GamePlayer gameKiller = me.islandcraft.skywars.controllers.PlayerController.get().get(killer);

			int scorePerKill = PluginConfig.getScorePerKill(killer);
			int xpPerKill = PluginConfig.getXpPerKill(killer);
			if (moneyToGet.get(gameKiller) != null) {
				moneyToGet.put(gameKiller, moneyToGet.get(gameKiller) + scorePerKill);
				xpToGet.put(gameKiller, xpToGet.get(gameKiller) + xpPerKill);
			} else {
				moneyToGet.put(gameKiller, scorePerKill);
				xpToGet.put(gameKiller, xpPerKill);
			}
			gameKiller.setKills(gameKiller.getKills() + 1);

			if ((this.playerCount - 1) != 1) {
				sendMessage("sw.die", new String[] { player.getDisplayName(), killer.getDisplayName(),
						Integer.toString(this.playerCount - 1) });
			} else {
				for (GamePlayer gp : this.playerIdMap.keySet()) {
					if (gp != gamePlayer) {
						sendMessage("sw.lastdie", new String[] { player.getDisplayName(), killer.getDisplayName(),
								gp.getBukkitPlayer().getDisplayName() });
					}
				}
				onPlayerLeave(gamePlayer, false, true, true);
			}
		} else {
			if ((this.playerCount - 1) != 1) {
				sendMessage("sw.suicide",
						new String[] { player.getDisplayName(), Integer.toString(this.playerCount - 1) });
			} else {
				for (GamePlayer gp : this.playerIdMap.keySet()) {
					if (gp != gamePlayer) {
						sendMessage("sw.lastsuicide",
								new String[] { player.getDisplayName(), gp.getBukkitPlayer().getDisplayName() });
					}
				}
				onPlayerLeave(gamePlayer, false, true, true);
			}
		}

		if (event != null) {
			Location location = player.getLocation().clone();
			World world = location.getWorld();

			for (org.bukkit.inventory.ItemStack itemStack : event.getDrops()) {
				world.dropItemNaturally(location, itemStack);
			}

			((ExperienceOrb) world.spawn(location, ExperienceOrb.class)).setExperience(event.getDroppedExp());

			event.setDeathMessage(null);
			event.getDrops().clear();
			event.setDroppedExp(0);

		}

		onPlayerLeave(gamePlayer, false, true, true);
	}

	public void onGameStart() {
		this.gameState = IGameState.PLAYING;
		for (Map.Entry<Integer, GamePlayer> playerEntry : this.idPlayerMap.entrySet()) {
			GamePlayer gamePlayer = (GamePlayer) playerEntry.getValue();

			if (gamePlayer != null) {
				getSpawn(((Integer) playerEntry.getKey()).intValue()).clone().add(0.0D, -1.0D, 0.0D).getBlock()
						.setTypeId(0);
				gamePlayer.setGamesPlayed(gamePlayer.getGamesPlayed() + 1);
			}
		}

		for (IGamePlayer igamePlayer : getPlayers()) {
			GamePlayer gamePlayer = (GamePlayer) igamePlayer;
			gamePlayer.getBukkitPlayer().setHealth(20.0D);
			gamePlayer.getBukkitPlayer().setFoodLevel(20);
			if (gamePlayer.getBukkitPlayer().getGameMode() != org.bukkit.GameMode.SURVIVAL) {
				gamePlayer.getBukkitPlayer().setGameMode(org.bukkit.GameMode.SURVIVAL);
			}

			registerScoreboard(gamePlayer);
			gamePlayer.getBukkitPlayer().sendMessage(ChatColor.GOLD + LanguageMain.get(gamePlayer, "sw.start"));
		}
		this.fixplayerIdMap = this.playerIdMap;
	}

	public void onGameEnd() {
		onGameEnd(null);
	}

	public void onGameEnd(GamePlayer gamePlayer) {
		if (gamePlayer != null) {
			Player player = gamePlayer.getBukkitPlayer();
			gamePlayer.setGamesWon(gamePlayer.getGamesWon() + 1);
			player.sendMessage(ChatColor.GREEN + LanguageMain.get(player, "sw.win"));

			if (moneyToGet.get(gamePlayer) != null) {
				moneyToGet.put(gamePlayer, moneyToGet.get(gamePlayer) + PluginConfig.getScorePerWin(player));
				xpToGet.put(gamePlayer, xpToGet.get(gamePlayer) + PluginConfig.getXpPerWin(player));
			} else {
				moneyToGet.put(gamePlayer, PluginConfig.getScorePerWin(player));
				xpToGet.put(gamePlayer, PluginConfig.getXpPerWin(player));
			}

			SkyWars.getEconomy().depositPlayer(gamePlayer.getBukkitPlayer().getName(), moneyToGet.get(gamePlayer));
			IslandPlayer ip = PlayerManager.get(player);
			ip.setXP(ip.getXP() + xpToGet.get(gamePlayer));
			player.sendMessage(ChatColor.GOLD + "------------------------");
			player.sendMessage(ChatColor.GOLD + LanguageMain.get(player, "sw.getcredits",
					new String[] { Integer.toString(moneyToGet.get(gamePlayer)) }));
			player.sendMessage(ChatColor.GOLD + "------------------------");

			for (Player p : Bukkit.getOnlinePlayers())
				player.showPlayer(p);
		}
		for (IGamePlayer igp : getPlayers()) {
			GamePlayer player = (GamePlayer) igp;
			onPlayerLeave(player, false, false, false);
		}
		for (GamePlayer player : spectators) {
			onPlayerLeave(player, false, false, false);
		}

		this.gameState = IGameState.ENDING;
		GameController.get().remove(this);
	}

	@SuppressWarnings("incomplete-switch")
	public void onTick() {

		if (!isBuilt()) {
			BlockPlacerPlayer entry = this.bpp;
			int blocks = 0;
			int maxBlocks = 0;
			double speed = 0.0D;
			double time = 0.0D;
			double percentage = 100.0D;
			if (entry != null) {
				blocks = entry.getQueue().size();
				maxBlocks = entry.getMaxQueueBlocks();
				speed = entry.getSpeed();
			}
			if (speed > 0.0D) {
				time = blocks / speed;
			}
			int newMax = Math.max(blocks, maxBlocks);
			if (newMax > 0) {
				percentage = 100 - 100 * blocks / newMax;
			}

			for (GamePlayer gp : toJoin) {
				BossBarAPI.setMessage(gp.getBukkitPlayer(),
						LanguageMain.get(gp, "game.building.bar", new String[] { Math.round(percentage) + "%" }));
				BossBarAPI.setHealth(gp.getBukkitPlayer(), (float) percentage);
			}
		}

		for (IGamePlayer gp : getPlayers()) {
			pointCompass(gp.getBukkitPlayer());
		}

		if (this.timer <= 0 && this.gameState == IGameState.WAITING) {
			return;
		}

		this.timer -= 1;

		switch (this.gameState) {
		case WAITING:
			if (this.timer == 0) {
				if (hasReachedMinimumPlayers()) {
					onGameStart();
				} else {
					sendMessage("sw.nominplayers", new String[] { Integer.toString(getMinimumPlayers()) });
					this.timer = 60;
				}
			} else if ((this.timer % 10 == 0 || this.timer <= 5) && this.timer != 1) {
				for (IGamePlayer gp : getPlayers()) {
					gp.getBukkitPlayer().sendMessage(LanguageMain.get(gp, "sw.starting",
							new String[] { Integer.toString(this.timer), LanguageMain.get(gp, "sw.seconds") }));
				}
			} else if (this.timer == 1) {
				for (IGamePlayer gp : getPlayers()) {
					gp.getBukkitPlayer().sendMessage(LanguageMain.get(gp, "sw.starting",
							new String[] { Integer.toString(this.timer), LanguageMain.get(gp, "sw.second") }));
				}
			}
			break;
		case PLAYING:
			for (IGamePlayer igp : this.getPlayers()) {
				GamePlayer p = (GamePlayer) igp;
				if (p.getEnderPearlDelay() > 0) {
					if (p.getEnderPearlDelay() != 0) {
						p.setEnderPearldDelay(p.getEnderPearlDelay() - 1);
					}
				}
			}
			break;
		}
	}

	public IGameState getState() {
		return this.gameState;
	}

	public boolean isFull() {
		return getPlayerCount() == this.slots;
	}

	public int getMinimumPlayers() {
		return 2;
	}

	public boolean hasReachedMinimumPlayers() {
		return getPlayerCount() >= getMinimumPlayers();
	}

	public void sendMessage(String message) {
		for (IGamePlayer gamePlayer : getPlayers()) {
			gamePlayer.getBukkitPlayer().sendMessage(message);
		}
		for (GamePlayer gp : spectators) {
			gp.getBukkitPlayer().sendMessage(message);
		}
	}

	public void sendMessage(String msgCode, String[] components) {
		for (IGamePlayer gamePlayer : getPlayers()) {
			gamePlayer.getBukkitPlayer().sendMessage(LanguageMain.get(gamePlayer, msgCode, components));
		}
		for (GamePlayer gp : spectators) {
			gp.getBukkitPlayer().sendMessage(LanguageMain.get(gp, msgCode, components));
		}
	}

	private GamePlayer getWinner() {
		for (GamePlayer gamePlayer : this.idPlayerMap.values()) {
			if (gamePlayer != null) {
				return gamePlayer;
			}
		}
		return null;
	}

	private int getFistEmpty() {
		for (Map.Entry<Integer, GamePlayer> playerEntry : this.idPlayerMap.entrySet()) {
			if (playerEntry.getValue() == null) {
				return ((Integer) playerEntry.getKey()).intValue();
			}
		}

		return -1;
	}

	public int getPlayerCount() {
		return this.playerCount;
	}

	public java.util.Collection<IGamePlayer> getPlayers() {
		List<IGamePlayer> playerList = com.google.common.collect.Lists.newArrayList();

		for (GamePlayer gamePlayer : this.idPlayerMap.values()) {
			if (gamePlayer != null) {
				playerList.add(gamePlayer);
			}
		}

		return playerList;
	}

	public List<IGamePlayer> getSpectators() {
		List<IGamePlayer> a = Lists.newArrayList();
		for (GamePlayer gp : spectators)
			a.add(gp);
		return a;
	}

	public Location getSpawn(int id) {
		return (Location) this.spawnPlaces.get(Integer.valueOf(id));
	}

	public void addSpawn(int id, Location location) {
		this.spawnPlaces.put(Integer.valueOf(id), location);
	}

	private void registerScoreboard(GamePlayer gp) {
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective objective = scoreboard.registerNewObjective("info", "dummy");
		objective.setDisplaySlot(org.bukkit.scoreboard.DisplaySlot.SIDEBAR);
		objective.setDisplayName("�c�lSkyWars");
		objective.getScore(LanguageMain.get(gp, "sw.sbplayer")).setScore(getPlayerCount());
		try {
			gp.getBukkitPlayer().setScoreboard(scoreboard);
		} catch (Exception e) {

		}
	}

	public boolean isSpectator(GamePlayer gp) {
		return spectators.contains(gp);
	}

	public int getID(GamePlayer gp) {
		return this.fixplayerIdMap.get(gp);
	}

	public void setSpectator(GamePlayer gp) {
		Player p = gp.getBukkitPlayer();

		boolean result = false;
		int x = 0;
		int z = 0;

		for (int i = 0; i < p.getNearbyEntities(500, 200, 500).size(); i++) {
			for (Entity et : p.getNearbyEntities(x, 200, z)) {
				if (et instanceof Player) {
					p.teleport(et);
					result = true;
					break;
				}
			}
			x++;
			z++;
			if (result) {
				break;
			}
		}

		Player a = null;
		double b = Double.MAX_VALUE;
		for (GamePlayer tgp : this.playerIdMap.keySet()) {
			Player tp = tgp.getBukkitPlayer();
			if (tp != p) {
				if (tp.getWorld() == p.getWorld()) {
					double c = p.getLocation().distanceSquared(tp.getLocation());
					if (c < b) {
						a = tp;
						b = c;
					}
				}
			}
		}

		if (a != null) {
			p.teleport(a);
		} else {
			p.teleport(getSpawn(1));
		}

		p.setGameMode(GameMode.SPECTATOR);
	}

	public void getCompass(Player p) {
		ItemStack compass = new ItemStack(Material.COMPASS);
		ItemMeta compassMeta = compass.getItemMeta();
		compassMeta.setDisplayName(ChatColor.GREEN + LanguageMain.get(p, "sw.compassitemname"));
		compass.setItemMeta(compassMeta);
		p.getInventory().setItem(0, compass);
	}

	public void pointCompass(Player p) {
		Player tp = p;
		double i = Double.MAX_VALUE;

		for (IGamePlayer gp : getPlayers()) {
			double distance = gp.getBukkitPlayer().getLocation().distanceSquared(p.getLocation());
			if (distance < i && distance > 5) {
				i = distance;
				tp = gp.getBukkitPlayer();
			}
		}

		try {
			p.setCompassTarget(tp.getLocation());
		} catch (Exception e) {
			pointCompass(p);
		}
	}

	public void setWEUtil(IWEUtils iweUtils) {
		this.iweUtils = iweUtils;
	}

	public void setBlockPlacerPlayer(BlockPlacerPlayer bpp) {
		this.bpp = bpp;
	}

	public void setPlayerEntry(PlayerEntry pe) {
		this.pe = pe;
	}
}