package me.islandcraft.skywars.listeners;

import java.util.List;

import org.bukkit.DyeColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.EnchantingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dye;

import com.google.common.collect.Lists;

public class EnchantListener implements Listener {

	private static List<Inventory> invList = Lists.newArrayList();

	private ItemStack lapis;

	public EnchantListener() {
		Dye d = new Dye();
		d.setColor(DyeColor.BLUE);
		this.lapis = d.toItemStack();
		this.lapis.setAmount(64);
	}

	@EventHandler
	public void openInventoryEvent(InventoryOpenEvent e) {
		if (e.getInventory() instanceof EnchantingInventory) {
			e.getInventory().setItem(1, this.lapis);
			invList.add(e.getInventory());
		}
	}

	@EventHandler
	public void closeInventoryEvent(InventoryCloseEvent e) {
		if (e.getInventory() instanceof EnchantingInventory) {
			e.getInventory().setItem(1, null);
			invList.remove(e.getInventory());
		}
	}

	@EventHandler
	public void inventoryClickEvent(InventoryClickEvent e) {
		if (e.getInventory() instanceof EnchantingInventory) {
			if (e.getSlot() == 1 && invList.contains(e.getInventory())
					&& e.getCurrentItem().getType() == this.lapis.getType()) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void enchantItemEvent(EnchantItemEvent e) {
		e.getInventory().setItem(1, this.lapis);
	}

}
