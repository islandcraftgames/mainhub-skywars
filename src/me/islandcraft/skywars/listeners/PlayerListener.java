package me.islandcraft.skywars.listeners;

import me.islandcraft.gameapi.IGame;
import me.islandcraft.gameapi.IGamePlayer;
import me.islandcraft.gameapi.IGameState;
import me.islandcraft.gameapi.controllers.ChatController;
import me.islandcraft.mainhub.events.PlayerSpeakEvent;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.skywars.SkyWars;
import me.islandcraft.skywars.controllers.ChestController;
import me.islandcraft.skywars.controllers.GameController;
import me.islandcraft.skywars.controllers.PlayerController;
import me.islandcraft.skywars.game.Game;
import me.islandcraft.skywars.player.GamePlayer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Chest;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.util.Vector;

public class PlayerListener implements org.bukkit.event.Listener {
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		GamePlayer gamePlayer = PlayerController.get().get(player);

		if (gamePlayer.isPlaying()) {
			gamePlayer.getGame().onPlayerLeave(gamePlayer);
		}

		gamePlayer.save();
		PlayerController.get().unregister(player);
	}

	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Player player = event.getPlayer();
		final GamePlayer gamePlayer = PlayerController.get().get(player);

		if (gamePlayer.isPlaying())
			event.setRespawnLocation(gamePlayer.getGame().getSpawn(1).clone()
					.add(0, -2, 0));
	}

	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		GamePlayer gamePlayer = PlayerController.get().get(player);

		if (gamePlayer.isPlaying()) {
			String command = event.getMessage().split(" ")[0].toLowerCase();
			if (command.equals("/tell") || command.equals("/msg")) {
				String to = event.getMessage().split(" ")[1].toLowerCase();
				if (Bukkit.getPlayer(to) != null) {
					if (PlayerController.get().get(Bukkit.getPlayer(to))
							.getGame() == gamePlayer.getGame()) {
						return;
					}
					player.sendMessage(ChatColor.RED
							+ LanguageMain.get(player, "sw.tellonlygame"));
					event.setCancelled(true);
					return;
				}
				return;
			}

			if ((!command.equals("/sw")) && (!command.equals("/leave"))) {
				event.setCancelled(true);
				player.sendMessage(ChatColor.RED
						+ LanguageMain.get(player, "sw.nocmdarena"));
			}
		}
	}

	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent e) {
		Player p = e.getPlayer();
		GamePlayer gP = PlayerController.get().get(p);
		if (gP == null) {
			return;
		}
		if (!gP.isPlaying()) {
			return;
		}
		Vector minVec = gP.getGame().getMinLoc();
		Vector maxVec = gP.getGame().getMaxLoc();
		if (e.getTo().getWorld() != gP.getGame().getWorld()) {
			p.sendMessage(ChatColor.RED + LanguageMain.get(p, "sw.leftarena"));
			gP.getGame().onPlayerLeave(gP);
		} else if (!e.getTo().toVector().isInAABB(minVec, maxVec)) {
			p.sendMessage(ChatColor.RED + LanguageMain.get(p, "sw.leftarena"));
			gP.getGame().onPlayerLeave(gP);
		}
	}

	@EventHandler
	public void onPlayerFlight(PlayerToggleFlightEvent e) {
		GamePlayer gP = PlayerController.get().get(e.getPlayer());
		if (gP == null) {
			return;
		}
		if (!gP.isPlaying())
			return;
		if (gP.getGame().isSpectator(gP))
			return;
		if (e.isFlying()) {
			e.setCancelled(true);
			e.getPlayer().setAllowFlight(false);
			e.getPlayer().setFlying(false);
			e.getPlayer()
					.sendMessage(
							ChatColor.RED
									+ LanguageMain.get(e.getPlayer(),
											"sw.noflyingame"));
		}
	}

	@EventHandler
	public void onGameModeChange(PlayerGameModeChangeEvent e) {
		GamePlayer gP = PlayerController.get().get(e.getPlayer());
		if (gP == null) {
			return;
		}
		if (!gP.isPlaying()) {
			return;
		}
		if (!(e.getNewGameMode().equals(GameMode.SURVIVAL) || e
				.getNewGameMode().equals(GameMode.SPECTATOR))) {
			e.setCancelled(true);
			e.getPlayer().setGameMode(GameMode.SURVIVAL);
			e.getPlayer().sendMessage(
					ChatColor.RED
							+ LanguageMain.get(e.getPlayer(),
									"sw.nogamemodeingame"));
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		PlayerController.get().register(e.getPlayer());
		if (GameController.get().getAll().size() != 0
				&& GameController.get().getAll() != null)
			for (IGame igame : GameController.get().getAll()) {
				Game game = (Game) igame;
				for (IGamePlayer gp : game.getPlayers()) {
					gp.getBukkitPlayer().hidePlayer(e.getPlayer());
				}
				for (IGamePlayer gp : game.getSpectators()) {
					gp.getBukkitPlayer().hidePlayer(e.getPlayer());
				}
			}
	}

	@EventHandler
	public void onSpectatorHitedByProjectile(EntityDamageByEntityEvent e) {
		Entity projectil = e.getDamager();
		Entity player1 = e.getEntity();

		if (projectil instanceof Arrow) {
			if (player1 instanceof Player) {
				Arrow arrow = (Arrow) projectil;
				Player player = (Player) player1;
				Vector velocity = arrow.getVelocity();

				GamePlayer gp = PlayerController.get().get(player);

				if (gp.isPlaying() && gp.getGame().isSpectator(gp)) {
					Location newSpawnLoc = player.getLocation();
					player.teleport(player.getLocation().add(0, 5, 0));
					Arrow newArrow = newSpawnLoc.getWorld().spawn(newSpawnLoc,
							Arrow.class);
					newArrow.setVelocity(velocity);
					newArrow.setShooter(((Arrow) projectil).getShooter());
				}
			}
		}

		if (projectil instanceof EnderPearl) {
			if (player1 instanceof Player) {
				EnderPearl enderpearl = (EnderPearl) projectil;
				Player player = (Player) player1;
				Player shooter = (Player) enderpearl.getShooter();
				if (player == shooter)
					return;
				Vector velocity = enderpearl.getVelocity();

				GamePlayer gp = PlayerController.get().get(player);

				if (gp.isPlaying() && gp.getGame().isSpectator(gp)) {
					Location newSpawnLoc = player.getLocation();
					player.teleport(player.getLocation().add(0, 5, 0));
					EnderPearl newEnderPearl = newSpawnLoc.getWorld().spawn(
							newSpawnLoc, EnderPearl.class);
					newEnderPearl.setVelocity(velocity);
					newEnderPearl.setShooter(((EnderPearl) projectil)
							.getShooter());
				}
			}
		}

		if (projectil instanceof Snowball) {
			if (player1 instanceof Player) {
				Snowball snowball = (Snowball) projectil;
				Player player = (Player) player1;
				Vector velocity = snowball.getVelocity();

				GamePlayer gp = PlayerController.get().get(player);

				if (gp.isPlaying() && gp.getGame().isSpectator(gp)) {
					Location newSpawnLoc = player.getLocation();
					player.teleport(player.getLocation().add(0, 5, 0));
					Snowball newSnowball = newSpawnLoc.getWorld().spawn(
							newSpawnLoc, Snowball.class);
					newSnowball.setVelocity(velocity);
					newSnowball.setShooter(((Snowball) projectil).getShooter());
				}
			}
		}
	}

	@EventHandler
	public void onSpectatorDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			GamePlayer gp = PlayerController.get().get((Player) e.getEntity());
			if (gp.isPlaying()) {
				if (gp.getGame().isSpectator(gp)) {
					e.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onShot(EntityShootBowEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			GamePlayer gp = PlayerController.get().get(p);
			if (gp.isPlaying()) {
				if (gp.getGame().getState() == IGameState.WAITING) {
					e.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onExplosiveArrow(ProjectileHitEvent e) {
		if (e.getEntity() instanceof Arrow
				&& e.getEntity().getShooter() instanceof Player) {
			if (PlayerController.get().get((Player) e.getEntity().getShooter())
					.isPlaying()) {
				Player p = (Player) e.getEntity().getShooter();
				if (PlayerController.get().get(p).getGame().getState() == IGameState.PLAYING)
					if (p.hasPermission("vip.vip+"))
						e.getEntity()
								.getWorld()
								.createExplosion(
										e.getEntity().getLocation().getBlockX(),
										e.getEntity().getLocation().getBlockY(),
										e.getEntity().getLocation().getBlockZ(),
										1.2F, false, true);
			}
		}
	}

	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent event) {
		if (!(event.getInventory().getHolder() instanceof Chest)) {
			return;
		}

		Player player = (Player) event.getPlayer();
		GamePlayer gamePlayer = PlayerController.get().get(player);

		if (!gamePlayer.isPlaying()) {
			return;
		}

		Chest chest = (Chest) event.getInventory().getHolder();
		org.bukkit.Location location = chest.getLocation();

		if (!gamePlayer.getGame().isChest(location)) {
			return;
		}

		Inventory inv = chest.getInventory();

		inv.clear();

		gamePlayer.getGame().removeChest(location);
		ChestController.get().populateChest(chest);
	}

	@EventHandler
	public void onChat(PlayerSpeakEvent e) {
		ChatController.sendChatGame(e, SkyWars.getGameType(),
				SkyWars.getGameName());
	}

}