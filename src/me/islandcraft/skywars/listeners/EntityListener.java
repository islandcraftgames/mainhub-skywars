package me.islandcraft.skywars.listeners;


import me.islandcraft.gameapi.IGameState;
import me.islandcraft.mainhub.Main;
import me.islandcraft.skywars.controllers.PlayerController;
import me.islandcraft.skywars.game.Game;
import me.islandcraft.skywars.player.GamePlayer;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;


public class EntityListener implements org.bukkit.event.Listener
{
	@EventHandler(ignoreCancelled = true)
	public void onEntityDamage(EntityDamageEvent event)
	{
		if (event.getEntityType() != EntityType.PLAYER) {
			return;
			}
		
		Player player = (Player) event.getEntity();
		GamePlayer gamePlayer = PlayerController.get().get(player);
		
		if ((event.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK)
				&& (gamePlayer.shouldSkipFireTicks())) {
			player.setFireTicks(0);
			event.setCancelled(true);
			gamePlayer.setSkipFireTicks(false);
			}
		
		if (!gamePlayer.isPlaying()) {
			return;
			}
		
		Game game = gamePlayer.getGame();
		
		if (game.getState() == IGameState.WAITING) {
			event.setCancelled(true);
			} else if ((event.getCause() == EntityDamageEvent.DamageCause.FALL)
				&& (gamePlayer.shouldSkipFallDamage())) {
			gamePlayer.setSkipFallDamage(false);
			event.setCancelled(true);
			} else if (event.getCause() == EntityDamageEvent.DamageCause.VOID) {
			player.setFallDistance(0.0F);
			event.setCancelled(true);
			gamePlayer.getGame().onPlayerDeath(gamePlayer, null);
			}
		}

	
	@EventHandler(ignoreCancelled = true)
	public void onPlayerDeath(final PlayerDeathEvent event) {
		Player player = event.getEntity();
		final GamePlayer gamePlayer = PlayerController.get()
				.get(player);
		
		if (!gamePlayer.isPlaying()) {
			return;
			}
		
		EntityDamageEvent.DamageCause damageCause = player
				.getLastDamageCause().getCause();
		if ((player.getLastDamageCause() instanceof EntityDamageByEntityEvent)) {
			org.bukkit.Bukkit.getScheduler().runTaskLater(Main.get(),
					new Runnable()
					{
						
						public void run() {
							gamePlayer.getGame().onPlayerDeath(gamePlayer,
									event);
						}
					}, 1L);
			
			}
		else if ((damageCause == EntityDamageEvent.DamageCause.LAVA)
				|| (damageCause == EntityDamageEvent.DamageCause.FIRE)
				|| (damageCause == EntityDamageEvent.DamageCause.FIRE_TICK)) {
			gamePlayer.setSkipFireTicks(true);
			gamePlayer.getGame().onPlayerDeath(gamePlayer, event);
			} else {
			gamePlayer.getGame().onPlayerDeath(gamePlayer, event);
			}
		}
	
}