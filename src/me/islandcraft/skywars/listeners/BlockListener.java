package me.islandcraft.skywars.listeners;

import me.islandcraft.gameapi.IGameState;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.party.Party;
import me.islandcraft.party.PartyManager;
import me.islandcraft.skywars.controllers.GameController;
import me.islandcraft.skywars.controllers.PlayerController;
import me.islandcraft.skywars.game.Game;
import me.islandcraft.skywars.player.GamePlayer;
import me.islandcraft.skywars.storage.SignStorage;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class BlockListener implements org.bukkit.event.Listener {
	@EventHandler(ignoreCancelled = true)
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		GamePlayer gamePlayer = PlayerController.get().get(player);

		if ((gamePlayer.isPlaying())
				&& (gamePlayer.getGame().getState() == IGameState.WAITING)) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = false)
	public void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		GamePlayer gamePlayer = PlayerController.get().get(player);

		if ((gamePlayer.isPlaying())
				&& (gamePlayer.getGame().getState() == IGameState.WAITING)) {
			event.setCancelled(true);
		}

		if (SignStorage.isSign(event.getBlock().getLocation()) && event.getPlayer().hasPermission("admin")) {
			SignStorage.removeSign(event.getBlock().getLocation());
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onSignCreate(SignChangeEvent e) {
		if (e.getLine(0).equalsIgnoreCase("[sw]") && e.getLine(1) != "") {
			e.setLine(0, ChatColor.BLUE + "[SkyWars]");
			SignStorage.addSign(e.getBlock().getLocation(), e.getLine(1));
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onSignInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (SignStorage.isSign(e.getClickedBlock().getLocation())) {
				if (e.getClickedBlock().getType() == Material.SIGN
						|| e.getClickedBlock().getType() == Material.SIGN_POST
						|| e.getClickedBlock().getType() == Material.WALL_SIGN) {
					Sign sign = (Sign) e.getClickedBlock().getState();
					e.setCancelled(true);
					GamePlayer gp = PlayerController.get().get(e.getPlayer());
					if (gp.isPlaying())
						return;
					Party party = PartyManager.getPlayerParty(e.getPlayer());
					if (party != null) {
						if (party.isOwner(e.getPlayer())) {
							int size = party.getSize();
							Game game = GameController.get().findEmpty(
									sign.getLine(1), size);
							for (Player p : party.getPlayers()) {
								game.onPlayerLogin(PlayerController.get().get(p));
							}
						} else {
							e.getPlayer()
									.sendMessage(
											ChatColor.RED
													+ LanguageMain.get(e.getPlayer(), "party.onlyownercanjoin"));
						}
					} else {
						Game game = GameController.get().findEmpty(
								sign.getLine(1), 1);
						game.onPlayerLogin(gp);
					}
				}
			}
		}
	}
}