package me.islandcraft.skywars.listeners;

import java.io.File;
import java.io.IOException;
import java.util.List;

import me.islandcraft.gameapi.IGameState;
import me.islandcraft.gameapi.IKit;
import me.islandcraft.mainhub.Main;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.skywars.SkyWars;
import me.islandcraft.skywars.build.VillagerKits;
import me.islandcraft.skywars.controllers.PlayerController;
import me.islandcraft.skywars.kits.KitsMain;
import me.islandcraft.skywars.player.GamePlayer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;

public class VillagerListener implements Listener {

	int timer;

	@EventHandler
	public void onVillagerInteract(EntityInteractEvent e) {
		if (e.getEntity() instanceof Villager) {
			File directory = Main.get().getDataFolder();
			File villagersFile = new File(directory, "villagers.yml");

			if (!villagersFile.exists()) {
				return;
			}

			YamlConfiguration villagers = new YamlConfiguration();

			try {
				villagers.load(villagersFile);

				if (villagers.getString("UUID").equalsIgnoreCase(
						e.getEntity().getUniqueId().toString())) {
					e.setCancelled(true);
				}

			} catch (IOException | InvalidConfigurationException e1) {
				e1.printStackTrace();
			}
		}
	}

	@EventHandler
	public void onVillagerDamage(EntityDamageEvent e) {
		if (e.isCancelled())
			return;
		if (e.getEntity() instanceof Villager) {
			File directory = Main.get().getDataFolder();
			File villagersFile = new File(directory, "villagers.yml");

			if (!villagersFile.exists()) {
				return;
			}

			YamlConfiguration villagers = new YamlConfiguration();

			try {
				villagers.load(villagersFile);

				if (villagers.getString("UUID").equalsIgnoreCase(
						e.getEntity().getUniqueId().toString())) {
					e.setCancelled(true);
				}

			} catch (IOException | InvalidConfigurationException e1) {
				e1.printStackTrace();
			}
		}
	}

	@EventHandler
	public void onPlayerClick(PlayerInteractEntityEvent e) {
		if (e.getRightClicked() instanceof Villager) {
			File directory = Main.get().getDataFolder();
			File villagersFile = new File(directory, "villagers.yml");

			if (!villagersFile.exists()) {
				return;
			}

			YamlConfiguration villagers = new YamlConfiguration();

			try {
				villagers.load(villagersFile);

				if (villagers.getString("UUID").equalsIgnoreCase(
						e.getRightClicked().getUniqueId().toString())) {
					VillagerKits.openGUI(e.getPlayer());
					e.setCancelled(true);
				}

			} catch (IOException | InvalidConfigurationException e1) {
				e1.printStackTrace();
			}
		}
	}

	@EventHandler
	public void onEnderPearl(ProjectileLaunchEvent e) {
		if (e.getEntity() instanceof EnderPearl) {
			if (e.getEntity().getShooter() instanceof Player) {
				GamePlayer gp = PlayerController.get().get(
						(Player) e.getEntity().getShooter());
				if (gp.isPlaying()) {
					if (gp.getEnderPearlDelay() == 0) {
						gp.setEnderPearldDelay(60);
					} else {
						if (gp.getGame().getState() == IGameState.PLAYING) {
							gp.getBukkitPlayer()
									.sendMessage(
											ChatColor.RED
													+ LanguageMain
															.get(gp,
																	"sw.waitenderpearl",
																	new String[] {
																			Integer.toString(gp
																					.getEnderPearlDelay()),
																			(gp.getEnderPearlDelay() == 1 ?

																			LanguageMain
																					.get(gp,
																							"sw.second")

																					: LanguageMain
																							.get(gp,
																									"sw.seconds")) }));
						} else {
							gp.getBukkitPlayer().sendMessage(
									ChatColor.RED
											+ LanguageMain.get(gp,
													"sw.enderpearlwaiting"));
						}
						gp.getBukkitPlayer().getInventory()
								.addItem(new ItemStack(Material.ENDER_PEARL));
						gp.getBukkitPlayer().updateInventory();
						e.setCancelled(true);
					}
				}
			}
		} else if (e.getEntity() instanceof Arrow) {
			if (e.getEntity().getShooter() instanceof Player) {
				GamePlayer gp = PlayerController.get().get(
						(Player) e.getEntity().getShooter());
				if (gp.isPlaying()) {
					if (gp.getGame().getState() == IGameState.WAITING) {
						gp.getBukkitPlayer().getInventory()
								.addItem(new ItemStack(Material.ARROW));
						gp.getBukkitPlayer().getInventory()
								.remove(Material.BOW);
						gp.getBukkitPlayer().getInventory()
								.addItem(new ItemStack(Material.BOW));
						e.setCancelled(true);
					}
				}
			}
		} else if (e.getEntity() instanceof Snowball) {
			if (e.getEntity().getShooter() instanceof Player) {
				GamePlayer gp = PlayerController.get().get(
						(Player) e.getEntity().getShooter());
				if (gp.isPlaying()) {
					if (gp.getGame().getState() == IGameState.WAITING) {
						gp.getBukkitPlayer().getInventory()
								.addItem(new ItemStack(Material.SNOW_BALL));
						e.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler
	public void onEat(PlayerItemConsumeEvent e) {
		GamePlayer gp = PlayerController.get().get(e.getPlayer());
		if (gp.isPlaying()) {
			if (gp.getGame().getState() == IGameState.WAITING) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		GamePlayer gp = PlayerController.get().get(e.getPlayer());
		if (gp.isPlaying()) {
			if (gp.getGame().getState() == IGameState.WAITING) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		if (ChatColor.stripColor(e.getInventory().getName()).equalsIgnoreCase(
				SkyWars.getGameName())) {
			e.setCancelled(true);
			GamePlayer gp = PlayerController.get().get(
					(Player) e.getWhoClicked());
			boolean[] kits = gp.getKits();
			List<IKit> a = KitsMain.getAllKits();
			for (IKit kit : a) {
				if (gp.getBukkitPlayer().hasPermission(kit.getPerm()))
					if (e.getSlot() == kit.getSlot()) {
						if (kits[kit.getID()]) {
							gp.getBukkitPlayer()
									.sendMessage(
											ChatColor.RED
													+ LanguageMain
															.get(gp,
																	"sw.alreadykit",
																	new String[] { LanguageMain
																			.get(gp,
																					kit.getLangCode()) }));
							gp.getBukkitPlayer().closeInventory();
							return;
						}
						if (kit.getBeforeKit() != null) {
							if (!kits[kit.getBeforeKit().getID()]) {
								gp.getBukkitPlayer()
										.sendMessage(
												ChatColor.RED
														+ LanguageMain
																.get(gp,
																		"sw.needkit",
																		new String[] { LanguageMain
																				.get(gp,
																						kit.getBeforeKit()
																								.getLangCode()) }));
								gp.getBukkitPlayer().closeInventory();
								return;
							}
						}
						if (SkyWars.getEconomy().withdrawPlayer(gp.getName(),
								kit.getPrice())) {
							kits[kit.getID()] = true;
							gp.setKits(kits);
							gp.getBukkitPlayer()
									.sendMessage(
											ChatColor.GREEN
													+ LanguageMain
															.get(gp,
																	"sw.buykit",
																	new String[] { LanguageMain
																			.get(gp,
																					kit.getLangCode()) })
													+ ChatColor.GOLD
													+ SkyWars
															.getEconomy()
															.getBalance(
																	gp.getName()));
							gp.getBukkitPlayer().closeInventory();
						} else {
							gp.getBukkitPlayer()
									.sendMessage(
											ChatColor.RED
													+ LanguageMain
															.get(gp,
																	"sw.nomoneykit",
																	new String[] {
																			Double.toString(kit
																					.getPrice()
																					- SkyWars
																							.getEconomy()
																							.getBalance(
																									gp.getName())),
																			LanguageMain
																					.get(gp,
																							kit.getLangCode()) }));
							gp.getBukkitPlayer().closeInventory();
						}
						gp.getBukkitPlayer().closeInventory();
					}
			}
		}
	}

	public void stopTimer() {
		Bukkit.getScheduler().cancelTask(timer);
	}
}
