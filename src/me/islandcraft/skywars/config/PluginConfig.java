package me.islandcraft.skywars.config;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import me.islandcraft.mainhub.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class PluginConfig {
	private static FileConfiguration storage = null;
	private static Location lobbySpawn = null;

	private static void initialize() {
		storage = Main.get().getConfig();

		String world = storage.getString("skylobby").split(", ")[3];
		int x = Integer.parseInt(storage.getString("skylobby").split(", ")[0]);
		int y = Integer.parseInt(storage.getString("skylobby").split(", ")[1]);
		int z = Integer.parseInt(storage.getString("skylobby").split(", ")[2]);

		lobbySpawn = new Location(Bukkit.getWorld(world), x, y, z);
	}

	public static Location getLobbySpawn() {
		if (lobbySpawn == null || storage == null)
			initialize();
		return lobbySpawn;
	}

	public static void setLobbySpawn(Location location) {
		if (lobbySpawn == null || storage == null)
			initialize();
		lobbySpawn = location.clone();
		storage.set("lobby.world", lobbySpawn.getWorld().getName());
		storage.set("lobby.spawn", String.format(
				Locale.US,
				"%.2f %.2f %.2f %.2f %.2f",
				new Object[] { Double.valueOf(location.getX()),
						Double.valueOf(location.getY()),
						Double.valueOf(location.getZ()),
						Float.valueOf(location.getYaw()),
						Float.valueOf(location.getPitch()) }));
		saveConfig();
	}

	public static int getIslandsPerWorld() {
		return 1000;
	}

	public static int getIslandBuffer() {
		return 30;
	}

	public static int getScorePerKill(Player p) {
		double a = 50;
		if (p.hasPermission("vip.vip++")) {
			a = a * 3;
		} else if (p.hasPermission("vip.vip+")) {
			a = a * 2;
		} else if (p.hasPermission("vip.vip")) {
			a = a * 1.5;
		}

		return (int) Math.floor(a);
	}

	public static int getXpPerKill(Player p) {
		double a = 1;
		if (p.hasPermission("vip.vip++")) {
			a = a * 3;
		} else if (p.hasPermission("vip.vip+")) {
			a = a * 2;
		} else if (p.hasPermission("vip.vip")) {
			a = a * 1.5;
		}

		return (int) Math.floor(a);
	}

	public static int getScorePerWin(Player p) {
		double a = 200;
		if (p.hasPermission("vip.vip++")) {
			a = a * 3;
		} else if (p.hasPermission("vip.vip+")) {
			a = a * 2;
		} else if (p.hasPermission("vip.vip")) {
			a = a * 1.5;
		}

		return (int) Math.floor(a);
	}

	public static int getXpPerWin(Player p) {
		double a = 4;
		if (p.hasPermission("vip.vip++")) {
			a = a * 3;
		} else if (p.hasPermission("vip.vip+")) {
			a = a * 2;
		} else if (p.hasPermission("vip.vip")) {
			a = a * 1.5;
		}

		return (int) Math.floor(a);
	}

	public static int getScorePerDeath(Player p) {
		return 0;
	}

	public static int getScorePerLeave(Player p) {
		return 0;
	}

	public static boolean buildSchematic() {
		return false;
	}

	public static int blocksPerTick() {
		return 20;
	}

	public static long buildInterval() {
		return 1L;
	}

	public static boolean buildCages() {
		return true;
	}

	public static boolean ignoreAir() {
		return false;
	}

	public static boolean useEconomy() {
		return true;
	}

	private static boolean saveConfig() {
		if (lobbySpawn == null || storage == null)
			initialize();
		File file = new File("./plugins/MainHub/config.yml");
		try {
			storage.save(file);
			return true;
		} catch (IOException ignored) {
		}
		return false;
	}

	public static boolean getSpectator() {
		return false;
	}
}