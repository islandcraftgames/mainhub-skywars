package me.islandcraft.party;

import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class PartyManager {

	private static List<Party> parties = Lists.newArrayList();
	private static HashMap<Player, List<Player>> invites = Maps.newHashMap();

	public static void register(Party party) {
		parties.add(party);
	}

	public static List<Party> getAll() {
		return parties;
	}

	public static Party getPlayerParty(Player p) {
		for (Party party : parties) {
			if (party.getPlayers().contains(p)) {
				return party;
			}
		}
		return null;
	}

	public static void invite(Player inviter, Player invited) {
		List<Player> inviters = invites.get(invited);
		if (inviters == null)
			inviters = Lists.newArrayList();
		inviters.add(inviter);
		invites.put(invited, inviters);
	}

	public static boolean hasInvitedBy(Player inviter, Player invited) {
		List<Player> inviters = invites.get(invited);
		if (inviters == null)
			return false;
		if (inviters.contains(inviter))
			return true;
		return false;
	}

	public static void clearInvites(Player invited) {
		if (invites.containsKey(invited))
			invites.remove(invited);
	}

	public static void unregister(Party p) {
		if (parties.contains(p))
			parties.remove(p);
	}
}
