package me.islandcraft.party.commands;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.party.Party;
import me.islandcraft.party.PartyManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KickCMD implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (!(s instanceof Player)) {
			s.sendMessage("Only Players");
			return false;
		}

		Player p = (Player) s;

		if (args.length < 2) {
			p.sendMessage(ChatColor.GREEN + "/party kick <player>");
			return false;
		}

		Player tp = Bukkit.getPlayer(args[1]);

		if (tp == null) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.notonline",
							new String[] { args[1] }));
			return false;
		}

		Party party = PartyManager.getPlayerParty(p);

		if (party == null) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.notinaparty"));
			return false;
		}

		if (party.getOwner() != p) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.onlyowner"));
			return false;
		}

		if (PartyManager.getPlayerParty(tp) != party) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.notthesame",
							new String[] { tp.getDisplayName() }));
			return false;
		}

		for (Player p2 : party.getPlayers()) {
			p2.sendMessage(ChatColor.GOLD
					+ LanguageMain.get(
							p2,
							"party.kick",
							new String[] { tp.getDisplayName(),
									p.getDisplayName() }));
		}

		party.onLeave(tp, false);
		return true;
	}

}
