package me.islandcraft.party.commands;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.party.Party;
import me.islandcraft.party.PartyManager;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PvpCMD implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (!(s instanceof Player)) {
			s.sendMessage("Only Players");
			return false;
		}

		Player p = (Player) s;
		
		if(!p.hasPermission("vip")){
			p.sendMessage(ChatColor.RED + LanguageMain.get(p, "party.onlyvip"));
		}

		if (args.length < 2) {
			p.sendMessage(ChatColor.GREEN + "/party pvp <Y/N>");
			return false;
		}

		Party party = PartyManager.getPlayerParty(p);

		if (party == null) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.notinaparty"));
			return false;
		}

		if (party.getOwner() != p) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.onlyowner"));
			return false;
		}

		if (args[1].equalsIgnoreCase("Y")) {
			party.setPVP(true);
			return true;
		} else if (args[1].equalsIgnoreCase("N")) {
			party.setPVP(false);
			return true;
		} else {
			p.sendMessage(ChatColor.GREEN + "/party pvp <Y/N>");
		}

		return false;
	}

}
