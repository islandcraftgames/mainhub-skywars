package me.islandcraft.party.commands;

import java.util.List;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.party.Party;
import me.islandcraft.party.PartyManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InfoCMD implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (!(s instanceof Player)) {
			s.sendMessage("Only Players");
			return false;
		}

		Player p = (Player) s;

		if (args.length == 1) {
			Party party = PartyManager.getPlayerParty(p);

			if (party == null) {
				p.sendMessage(ChatColor.RED
						+ LanguageMain.get(p, "party.notinaparty"));
				return false;
			}

			p.sendMessage(getInfo(p, party.getName(), party.getPlayers(),
					party.getPVP()));
			return true;
		}

		Player tp = Bukkit.getPlayer(args[1]);

		if (tp == null) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.notonline",
							new String[] { args[1] }));
			return false;
		}

		Party party = PartyManager.getPlayerParty(tp);

		if (party == null) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.notinaparty.others",
							new String[] { tp.getDisplayName() }));
			return false;
		}

		p.sendMessage(getInfo(p, party.getName(), party.getPlayers(),
				party.getPVP()));

		return false;
	}

	public String[] getInfo(Player p, String name, List<Player> players,
			boolean pvp) {
		String[] a = new String[3];
		a[0] = ChatColor.GOLD + "----------[" + name + "]----------";
		a[1] = ChatColor.GOLD + LanguageMain.get(p, "party.info.players");
		for (Player p2 : players) {
			a[1] += p2.getName() + " ";
		}
		a[2] = ChatColor.GOLD + "PVP: ";
		if (pvp) {
			a[2] += ChatColor.RED + LanguageMain.get(p, "party.info.yes");
		} else {
			a[2] += ChatColor.GREEN + LanguageMain.get(p, "party.info.no");
		}
		return a;
	}

}
