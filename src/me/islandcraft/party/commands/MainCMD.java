package me.islandcraft.party.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MainCMD implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (args.length == 0) {
			s.sendMessage(getHelpMenu());
			return false;
		}

		if (args[0].equalsIgnoreCase("create"))
			return new CreateCMD().onCommand(s, cmd, label, args);
		else if (args[0].equalsIgnoreCase("invite"))
			return new InviteCMD().onCommand(s, cmd, label, args);
		else if (args[0].equalsIgnoreCase("kick"))
			return new KickCMD().onCommand(s, cmd, label, args);
		else if (args[0].equalsIgnoreCase("delete"))
			return new DeleteCMD().onCommand(s, cmd, label, args);
		else if (args[0].equalsIgnoreCase("info"))
			return new InfoCMD().onCommand(s, cmd, label, args);
		else if (args[0].equalsIgnoreCase("accept"))
			return new AcceptCMD().onCommand(s, cmd, label, args);
		else if (args[0].equalsIgnoreCase("leave"))
			return new LeaveCMD().onCommand(s, cmd, label, args);
		else if (args[0].equalsIgnoreCase("name"))
			new NameCMD().onCommand(s, cmd, label, args);
		else if (args[0].equalsIgnoreCase("pvp"))
			new PvpCMD().onCommand(s, cmd, label, args);

		s.sendMessage(getHelpMenu());
		return false;
	}

	private String[] getHelpMenu() {
		String[] a = new String[10];
		a[0] = ChatColor.GREEN + "Use: ";
		a[1] = ChatColor.GREEN + "/party create";
		a[2] = ChatColor.GREEN + "/party invite <player>";
		a[3] = ChatColor.GREEN + "/party kick <player>";
		a[4] = ChatColor.GREEN + "/party delete";
		a[5] = ChatColor.GREEN + "/party info [player]";
		a[6] = ChatColor.GREEN + "/party accept <plater>";
		a[7] = ChatColor.GREEN + "/party leave";
		a[8] = ChatColor.GREEN + "/party name <new name>    " + ChatColor.GOLD
				+ "-----> VIP";
		a[9] = ChatColor.GREEN + "/party pvp <Y/N>       " + ChatColor.GOLD
				+ "-----> VIP";
		return a;
	}

}
