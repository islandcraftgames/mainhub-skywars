package me.islandcraft.party.commands;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.party.Party;
import me.islandcraft.party.PartyManager;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreateCMD implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (!(s instanceof Player)) {
			s.sendMessage("Only Players");
			return false;
		}

		Player p = (Player) s;

		if (PartyManager.getPlayerParty(p) == null) {
			new Party(p);
			p.sendMessage(ChatColor.GREEN + LanguageMain.get(p, "party.createsuccess"));
			return true;
		} else {
			p.sendMessage(ChatColor.RED + LanguageMain.get(p, "party.alreadyinaparty"));
		}

		return false;
	}
	
}
