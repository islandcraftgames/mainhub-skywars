package me.islandcraft.party.commands;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.party.Party;
import me.islandcraft.party.PartyManager;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class InviteCMD implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (!(s instanceof Player)) {
			s.sendMessage("Only Players");
			return false;
		}

		Player p = (Player) s;

		if (args.length < 2) {
			p.sendMessage(ChatColor.GREEN + "/party invite <player>");
			return false;
		}

		Player tp = Bukkit.getPlayer(args[1]);

		if (tp == null) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.notonline",
							new String[] { args[1] }));
			return false;
		}

		if (tp == p) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.invitemeerr"));
			return false;
		}

		Party party = PartyManager.getPlayerParty(p);

		if (party == null) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.notinaparty"));
			return false;
		}

		if (party.getOwner() != p) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.onlyowner"));
			return false;
		}

		if (PartyManager.getPlayerParty(tp) != null) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.alreadyinaparty.others",
							new String[] { tp.getDisplayName() }));
			return false;
		}

		if (PartyManager.hasInvitedBy(p, tp)) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.alreadyinvited.others",
							new String[] { tp.getDisplayName() }));
		}

		PartyManager.invite(p, tp);
		p.sendMessage(ChatColor.GREEN
				+ LanguageMain.get(p, "party.invitesuccess",
						new String[] { tp.getDisplayName() }));
		String rawJSON = LanguageMain.get(tp, "party.invited",
				new String[] { p.getDisplayName(), p.getName() });
		PacketPlayOutChat packet = new PacketPlayOutChat(
				ChatSerializer.a(rawJSON));
		((CraftPlayer) tp).getHandle().playerConnection.sendPacket(packet);
		return true;
	}
}
