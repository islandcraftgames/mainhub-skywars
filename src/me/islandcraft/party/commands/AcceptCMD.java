package me.islandcraft.party.commands;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.party.Party;
import me.islandcraft.party.PartyManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AcceptCMD implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {

		if (!(s instanceof Player)) {
			s.sendMessage("Only Players");
			return false;
		}

		if (args.length < 2) {
			s.sendMessage(ChatColor.GREEN + "/party accept <player>");
		}

		Player p = (Player) s;

		Party party = PartyManager.getPlayerParty(p);

		if (party != null) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.alreadyinaparty"));
			return false;
		}

		Player tp = Bukkit.getPlayer(args[1]);

		if (tp == null) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.notonline",
							new String[] { args[1] }));
			return false;
		}

		if (!PartyManager.hasInvitedBy(tp, p)) {
			p.sendMessage(ChatColor.RED
					+ LanguageMain.get(p, "party.notinvited",
							new String[] { tp.getDisplayName() }));
			return false;
		}

		PartyManager.clearInvites(p);
		PartyManager.getPlayerParty(tp).onJoin(p, true);

		return false;
	}
}
