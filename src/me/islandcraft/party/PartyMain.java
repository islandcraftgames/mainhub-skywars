package me.islandcraft.party;

import me.islandcraft.mainhub.Main;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PartyMain implements Listener {

	public PartyMain(){
		onEnable();
	}
	
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(this, Main.get());
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Party party = PartyManager.getPlayerParty(e.getPlayer());
		if (party != null) {
			party.onLeave(e.getPlayer(), true);
		}
	}

}
