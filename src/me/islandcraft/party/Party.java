package me.islandcraft.party;

import java.util.List;

import me.islandcraft.mainhub.language.LanguageMain;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

public class Party {

	private final Player owner;
	private List<Player> players = Lists.newArrayList();
	private String name;
	private boolean pvp = true;

	public Party(Player owner) {
		this.owner = owner;
		players.add(owner);

		name = owner.getName();

		PartyManager.register(this);
	}

	public List<Player> getPlayers() {
		return players;
	}

	public Player getOwner() {
		return owner;
	}

	public boolean isOwner(Player p) {
		return owner == p;
	}

	public void onJoin(Player p, boolean message) {
		players.add(p);

		if (message) {
			for (Player p2 : players) {
				p2.sendMessage(
						ChatColor.GREEN + LanguageMain.get(p2, "party.join", new String[] { p.getDisplayName() }));
			}
		}
	}

	public void onLeave(Player p, boolean message) {
		if (p == owner)
			onDelete();
		else {

			if (message) {
				for (Player p2 : players) {
					p2.sendMessage(LanguageMain.get(p2, "party.leave", new String[] { p.getDisplayName() }));
				}
			}

			players.remove(p);
		}

	}

	public void onDelete() {
		PartyManager.unregister(this);
		for (Player p : players) {
			p.sendMessage(LanguageMain.get(p, "party.delete"));
		}
	}

	public boolean contains(Player p) {
		return players.contains(p);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPVP(boolean pvp) {
		this.pvp = pvp;
	}

	public String getName() {
		return this.name;
	}

	public boolean getPVP() {
		return this.pvp;
	}

	public int getSize() {
		return players.size();
	}

}
