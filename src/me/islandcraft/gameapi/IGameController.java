package me.islandcraft.gameapi;

import java.util.Collection;

import me.islandcraft.mainhub.events.GameControllerUpdateEvent;

import org.bukkit.Bukkit;

public abstract class IGameController {

	public abstract Collection<IGame> getAll();

	public abstract boolean canRestart();

	public abstract void disableGames();

	public abstract boolean isDisabled();

	public void sendUpdate() {
		Bukkit.getPluginManager()
				.callEvent(new GameControllerUpdateEvent(this));
	}

}
