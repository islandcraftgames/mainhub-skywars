package me.islandcraft.gameapi.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.islandcraft.gameapi.IGameController;
import me.islandcraft.gameapi.IGameType;
import me.islandcraft.gameapi.IPlayerController;

import com.google.common.collect.Maps;

public class ClassController {

	private static HashMap<IGameType, IGameController> gameControllerMap = Maps
			.newHashMap();
	private static HashMap<IGameType, IPlayerController> playerControllerMap = Maps
			.newHashMap();

	public static void setGameController(IGameType gt, IGameController gc) {
		gameControllerMap.put(gt, gc);
	}

	public static IGameController getGameController(IGameType gt) {
		return gameControllerMap.get(gt);
	}
	
	public static List<IGameController> getAllGameControllers() {
		return new ArrayList<IGameController>(gameControllerMap.values());
	}
	
	public static void setPlayerController(IGameType gt, IPlayerController pc) {
		playerControllerMap.put(gt, pc);
	}

	public static IPlayerController getPlayerController(IGameType gt) {
		return playerControllerMap.get(gt);
	}
	
	public static List<IPlayerController> getAllPlayerControllers() {
		return new ArrayList<IPlayerController>(playerControllerMap.values());
	}

}
