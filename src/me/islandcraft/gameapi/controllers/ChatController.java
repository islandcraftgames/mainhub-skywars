package me.islandcraft.gameapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.islandcraft.gameapi.IGame;
import me.islandcraft.gameapi.IGamePlayer;
import me.islandcraft.gameapi.IGameState;
import me.islandcraft.gameapi.IGameType;
import me.islandcraft.gameapi.IPlayerController;
import me.islandcraft.gameapi.ITeam;
import me.islandcraft.gameapi.ITeamGame;
import me.islandcraft.gameapi.Spectable;
import me.islandcraft.mainhub.events.PlayerSpeakEvent;
import me.islandcraft.mainhub.utils.ListUtils;

public class ChatController {

	@SuppressWarnings("unchecked")
	public static void sendChatGame(PlayerSpeakEvent e, IGameType igt, String gameName) {
		Player p = e.getPlayer();
		IPlayerController ipc = ClassController.getPlayerController(igt);
		IGamePlayer gp = ipc.getI(p);
		if (!gp.isPlaying())
			return;
		String message = e.getMessage();
		try {
			switch (igt.getIGamePlayType()) {
			case TEAM:
				ITeamGame game = (ITeamGame) gp.getGame();
				IGameState gs = game.getState();
				ITeam team = game.getPlayerTeam(gp);

				List<Player> a = e.getListeners();
				for (Player p2 : a) {
					IGamePlayer gp2 = ipc.getI(p);
					if (gp2.isGlobalPlaying()) {
						if (gp2.getGlobalGame() != game) {
							a.remove(p2);
						}
					}
				}
				e.setListeners(a);

				if (message.startsWith("?") && message.length() > 1 && p.hasPermission("main.game.speak.global")) {
					e.setMessage(message.substring(1));
					return;
				} else if (game instanceof Spectable) {
					Spectable specgame = (Spectable) game;
					if (specgame.getSpectators().contains(gp)) {
						e.setPrefix("�8[Spectator]�r" + p.getName() + "�8:�r ");
						e.setListeners(ListUtils.gpListToPlayerList(specgame.getSpectators()));
						e.setSecundaryPrefix("�8[" + gameName + "]�8[Spectator]�r" + p.getName() + "�8:�r ");
						List<Player> l = new ArrayList<Player>();
						for (Player p2 : Bukkit.getOnlinePlayers()) {
							if (p2.hasPermission("main.game.listen.global"))
								if (ipc.getI(p2).getGame() != game)
									l.add(p2);
						}
						e.setSecundaryListeners(l);
						break;
					}else if ((message.startsWith("!") && message.length() > 1) || gs != IGameState.PLAYING) {
						e.setMessage(gs != IGameState.PLAYING ? message : message.substring(1));
						e.setPrefix("�4[Global]�r" + p.getName() + "�8:�r ");
						e.setListeners(ListUtils
								.gpListToPlayerList((List<IGamePlayer>) ListUtils.collectionToList(game.getPlayers())));
						e.setSecundaryPrefix("�8[" + gameName + "]�4[Global]�r" + p.getName() + "�8:�r ");
						List<Player> l = new ArrayList<Player>();
						for (Player p2 : Bukkit.getOnlinePlayers()) {
							if (p2.hasPermission("main.game.listen.global"))
								if (ipc.getI(p2).getGame() != game)
									l.add(p2);
						}
						e.setSecundaryListeners(l);
						break;
					}
				} else if ((message.startsWith("!") && message.length() > 1) || gs != IGameState.PLAYING) {
					e.setMessage(gs != IGameState.PLAYING ? message : message.substring(1));
					e.setPrefix("�4[Global]�r" + p.getName() + "�8:�r ");
					e.setListeners(ListUtils
							.gpListToPlayerList((List<IGamePlayer>) ListUtils.collectionToList(game.getPlayers())));
					e.setSecundaryPrefix("�8[" + gameName + "]�4[Global]�r" + p.getName() + "�8:�r ");
					List<Player> l = new ArrayList<Player>();
					for (Player p2 : Bukkit.getOnlinePlayers()) {
						if (p2.hasPermission("main.game.listen.global"))
							if (ipc.getI(p2).getGame() != game)
								l.add(p2);
					}
					e.setSecundaryListeners(l);
					break;
				}
				e.setPrefix("�b[Team]�r" + p.getName() + "�8:�r ");
				e.setListeners(ListUtils.gpListToPlayerList(game.getTeamIPlayers(team)));
				e.setSecundaryPrefix("�8[" + gameName + "]�b[Team]�r" + p.getName() + "�8:�r ");
				List<Player> l = new ArrayList<Player>();
				for (Player p2 : Bukkit.getOnlinePlayers()) {
					if (p2.hasPermission("main.game.listen.global"))
						if (ipc.getI(p2).getGame() != game)
							l.add(p2);
				}
				e.setSecundaryListeners(l);
				break;
			default:
				IGame game2 = gp.getGlobalGame();

				List<Player> a2 = e.getListeners();
				for (Player p2 : a2) {
					IGamePlayer gp2 = ipc.getI(p);
					if (gp2.isGlobalPlaying()) {
						if (gp2.getGlobalGame() != game2) {
							a2.remove(p2);
						}
					}
				}
				e.setListeners(a2);

				if (message.startsWith("?") && message.length() > 1 && p.hasPermission("main.game.speak.global")) {
					e.setMessage(message.substring(1));
					return;
				} else if (game2 instanceof Spectable) {
					Spectable specgame = (Spectable) game2;
					if (specgame.getSpectators().contains(gp)) {
						e.setPrefix("�8[Spectator]�r" + p.getName() + "�8:�r ");
						e.setListeners(ListUtils.gpListToPlayerList(specgame.getSpectators()));
						e.setSecundaryPrefix("�8[" + gameName + "]�8[Spectator]�r" + p.getName() + "�8:�r ");
						List<Player> l1 = new ArrayList<Player>();
						for (Player p2 : Bukkit.getOnlinePlayers()) {
							if (p2.hasPermission("main.game.listen.global"))
								if (ipc.getI(p2).getGame() != game2)
									l1.add(p2);
						}
						e.setSecundaryListeners(l1);
						break;
					}
				}
				e.setPrefix(p.getName() + "�8:�r ");
				e.setListeners(ListUtils
						.gpListToPlayerList((List<IGamePlayer>) ListUtils.collectionToList(game2.getPlayers())));
				e.setSecundaryPrefix("�8[" + gameName + "]�r" + p.getName() + "�8:�r ");
				List<Player> l1 = new ArrayList<Player>();
				for (Player p2 : Bukkit.getOnlinePlayers()) {
					if (p2.hasPermission("main.game.listen.global"))
						if (ipc.getI(p2).getGlobalGame() != game2)
							l1.add(p2);
				}
				e.setSecundaryListeners(l1);

				break;
			}
		} catch (Exception e1) {
		}
	}

}
