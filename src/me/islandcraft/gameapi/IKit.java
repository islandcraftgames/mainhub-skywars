package me.islandcraft.gameapi;

import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permission;

public abstract class IKit {
	
	protected int id;
	protected String langCode;
	protected ItemStack[] items;
	protected ItemStack showcaseItem;
	protected double price;
	protected IKit beforeKit;
	protected int slot;
	protected Permission perm;

	public int getID() {
		return id;
	}

	public String getLangCode() {
		return langCode;
	}

	public ItemStack[] getItems() {
		return items;
	}
	
	public ItemStack getShowCaseItem(){
		return showcaseItem;
	}

	public double getPrice() {
		return price;
	}

	public IKit getBeforeKit() {
		return beforeKit;
	}

	public int getSlot() {
		return slot;
	}
	
	public Permission getPerm(){
		return perm;
	}

	protected void setID(int id) {
		this.id = id;
	}

	protected void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	protected void setItems(ItemStack[] items) {
		this.items = items;
	}
	
	protected void setShowCaseItem(ItemStack showcaseItem){
		this.showcaseItem = showcaseItem;
	}

	protected void setPrice(double price) {
		this.price = price;
	}

	protected void setBeforeKit(IKit beforeKit) {
		this.beforeKit = beforeKit;
	}

	protected void setSlot(int slot) {
		this.slot = slot;
	}
	
	protected void setPermission(String perm){
		this.perm = new Permission(perm);
	}
	
}
