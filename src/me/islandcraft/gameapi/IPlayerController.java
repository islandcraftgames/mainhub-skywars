package me.islandcraft.gameapi;

import javax.annotation.Nonnull;

import org.bukkit.entity.Player;

public abstract class IPlayerController {
	
	public abstract IGamePlayer getI(@Nonnull Player bukkitPlayer);

}
