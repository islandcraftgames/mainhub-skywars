package me.islandcraft.gameapi;

public enum IGameState {
	WAITING, PLAYING, ENDING;
}
