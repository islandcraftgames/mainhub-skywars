package me.islandcraft.gameapi;

import org.bukkit.ChatColor;

public enum ITeam {
	RED(ChatColor.RED, "team.red"), BLUE(ChatColor.BLUE, "team.blue"), GREEN(ChatColor.GREEN, "team.green"), YELLOW(ChatColor.YELLOW, "team.yellow"), NEUTRAL(
			ChatColor.WHITE, "team.neutral");

	private final ChatColor color;
	private final String code;

	private ITeam(ChatColor color, String code) {
		this.color = color;
		this.code = code;
	}

	public ChatColor getColor() {
		return this.color;
	}
	
	public String getLanguageCode(){
		return this.code;
	}
}
