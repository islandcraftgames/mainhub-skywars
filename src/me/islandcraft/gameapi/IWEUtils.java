package me.islandcraft.gameapi;

import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.primesoft.asyncworldedit.AsyncWorldEditMain;
import org.primesoft.asyncworldedit.api.blockPlacer.IBlockPlacer;
import org.primesoft.asyncworldedit.api.blockPlacer.IBlockPlacerListener;
import org.primesoft.asyncworldedit.api.blockPlacer.IJobEntryListener;
import org.primesoft.asyncworldedit.blockPlacer.BlockPlacerPlayer;
import org.primesoft.asyncworldedit.blockPlacer.entries.JobEntry;
import org.primesoft.asyncworldedit.playerManager.PlayerEntry;
import org.primesoft.asyncworldedit.worldedit.AsyncCuboidClipboard;
import org.primesoft.asyncworldedit.worldedit.AsyncEditSessionFactory;
import org.primesoft.asyncworldedit.worldedit.ThreadSafeEditSession;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

@SuppressWarnings({ "deprecation", "static-access" })
public class IWEUtils {

	private final IGame game;
	private final Location origin;
	private final CuboidClipboard clipboard;
	private World world;
	private IBlockPlacer bPlacer;
	private IJobEntryListener stateListener;
	private IBlockPlacerListener listener;
	private static Random random = new Random();

	public IWEUtils(IGame game, Location origin, CuboidClipboard clipboard) {
		this.game = game;
		this.origin = origin;
		this.clipboard = clipboard;
		this.world = origin.getWorld();
		this.game.setWEUtil(this);
		pasteSchematic();
	}

	public boolean pasteSchematic() {
		AsyncWorldEditMain aweMain = (AsyncWorldEditMain) Bukkit.getPluginManager().getPlugin("AsyncWorldEdit");
		WorldEditPlugin worldEdit = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
		AsyncEditSessionFactory factory = (AsyncEditSessionFactory) worldEdit.getWorldEdit().getInstance()
				.getEditSessionFactory();

		bPlacer = aweMain.getBlockPlacer();
		stateListener = new IJobEntryListener() {
			@Override
			public void jobStateChanged(JobEntry job) {
				PlayerEntry pEntry = job.getPlayer();
				String name = job.getName();

				System.out.println("State: " + name + " " + pEntry.getName() + " " + pEntry.getUUID().toString() + "  "
						+ job.getStatus() + " " + job.isTaskDone());
			}
		};

		listener = new IBlockPlacerListener() {
			@Override
			public void jobAdded(JobEntry job) {
				PlayerEntry pEntry = job.getPlayer();
				String name = job.getName();

				job.addStateChangedListener(stateListener);

				BlockPlacerPlayer bpp = bPlacer.getPlayerEvents(pEntry);
				game.setBlockPlacerPlayer(bpp);

				System.out.println("JobAdded: " + name + " " + pEntry.getName() + " " + pEntry.getUUID().toString()
						+ "  " + job.getStatus() + " " + job.isTaskDone());

			}

			@Override
			public void jobRemoved(JobEntry job) {
				PlayerEntry pEntry = job.getPlayer();
				String name = job.getName();

				job.removeStateChangedListener(stateListener);

				System.out.println("JobRemoved: " + name + " " + pEntry.getName() + " " + pEntry.getUUID().toString()
						+ "  " + job.getStatus() + " " + job.isTaskDone());
				game.setBuilt(job.isTaskDone());
				// GameController.get().mantainArenas();
			}
		};

		bPlacer.addListener(listener);
		ThreadSafeEditSession es = factory.getThreadSafeEditSession(new BukkitWorld(world), 999999999, null,
				new PlayerEntry("Game - " + random.nextInt(99999999), UUID.randomUUID()));

		PlayerEntry pe = es.getPlayer();

		game.setPlayerEntry(pe);

		AsyncCuboidClipboard cc = new AsyncCuboidClipboard(pe, clipboard);
		try {
			cc.paste(es, new Vector(origin.getBlockX(), origin.getBlockY(), origin.getBlockZ()), true);
		} catch (Exception e) {
			}
		return true;
	}

	public void shutdown() {
		bPlacer.removeListener(listener);
	}

}
