package me.islandcraft.gameapi;

import me.islandcraft.gameapi.controllers.ClassController;

import org.bukkit.entity.Player;

public abstract class IGamePlayer {

	public abstract Player getBukkitPlayer();

	public IGame getGlobalGame() {
		for (IPlayerController ipc : ClassController.getAllPlayerControllers()) {
			IGamePlayer gp = ipc.getI(getBukkitPlayer());
			if (gp.isPlaying())
				return gp.getGame();
		}
		return null;
	}

	public abstract IGame getGame();

	public abstract boolean isPlaying();

	public boolean isGlobalPlaying() {
		for (IPlayerController ipc : ClassController.getAllPlayerControllers()) {
			if (ipc.getI(getBukkitPlayer()).isPlaying())
				return true;
		}
		return false;
	}

}
