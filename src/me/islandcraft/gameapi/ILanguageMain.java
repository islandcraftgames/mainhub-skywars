package me.islandcraft.gameapi;

import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.player.IslandPlayer;
import me.islandcraft.mainhub.player.PlayerManager;

import org.bukkit.entity.Player;

public class ILanguageMain {
	
	public static String get(IslandPlayer ip, String message,
			String[] complements) {
		return LanguageMain.get(ip.getLang(), message, complements);
	}

	public static String get(IGamePlayer gp, String message, String[] complements) {
		return get(PlayerManager.get(gp.getBukkitPlayer()), message,
				complements);
	}

	public static String get(Player p, String message, String[] complements) {
		return get(PlayerManager.get(p), message, complements);
	}

	public static String get(IslandPlayer ip, String message) {
		return LanguageMain.get(ip.getLang(), message, new String[0]);
	}

	public static String get(IGamePlayer gp, String message) {
		return get(PlayerManager.get(gp.getBukkitPlayer()), message,
				new String[0]);
	}

	public static String get(Player p, String message) {
		return get(PlayerManager.get(p), message, new String[0]);
	}

}
