package me.islandcraft.gameapi;

import java.util.List;

public interface Spectable {
	
	public List<IGamePlayer> getSpectators();

}
