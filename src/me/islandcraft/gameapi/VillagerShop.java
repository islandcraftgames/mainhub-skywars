package me.islandcraft.gameapi;

import java.util.List;

import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import me.islandcraft.mainhub.utils.ObjectUtils;
import net.minecraft.server.v1_8_R3.EntityVillager;
import net.minecraft.server.v1_8_R3.PathfinderGoalSelector;

@SuppressWarnings("rawtypes")
public class VillagerShop extends EntityVillager {

	public VillagerShop(World world) {
		super(((CraftWorld) world).getHandle(), 0);

		List goalB = (List) ObjectUtils.getPrivateField("b", PathfinderGoalSelector.class, goalSelector);
		goalB.clear();
	}
	
	public void clearGoals(){
		List goalC = (List) ObjectUtils.getPrivateField("c", PathfinderGoalSelector.class, goalSelector);
		goalC.clear();
		List targetB = (List) ObjectUtils.getPrivateField("b", PathfinderGoalSelector.class, targetSelector);
		targetB.clear();
		List targetC = (List) ObjectUtils.getPrivateField("c", PathfinderGoalSelector.class, targetSelector);
		targetC.clear();
	}
	
	public void m() {
        this.fireProof = true;
        super.m();
    }
   
    @Override
    public void g(double d0, double d1, double d2) {
        return;
    }

}
