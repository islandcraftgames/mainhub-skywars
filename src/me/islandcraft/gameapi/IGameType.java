package me.islandcraft.gameapi;

public enum IGameType {
	UNKNOWN(IGamePlayType.SOLO), SKYWARS(IGamePlayType.SOLO), CAPTURETHEFLAG(IGamePlayType.TEAM), CRAZYRACE(
			IGamePlayType.SOLO), X1(IGamePlayType.SOLO), PAINTWARS(IGamePlayType.TEAM), DEATHMATCHCLASSIC(
					IGamePlayType.SOLO), NAVALBATTLE(IGamePlayType.TEAM), TEAMSKYWARS(
							IGamePlayType.TEAM), EGGWARS(IGamePlayType.TEAM), TNTWARS(IGamePlayType.TEAM);

	private IGamePlayType igpt;

	private IGameType(IGamePlayType igpt) {
		this.igpt = igpt;
	}

	public IGamePlayType getIGamePlayType() {
		return this.igpt;
	}

}
