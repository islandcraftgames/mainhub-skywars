package me.islandcraft.gameapi;

import org.primesoft.asyncworldedit.blockPlacer.BlockPlacerPlayer;
import org.primesoft.asyncworldedit.playerManager.PlayerEntry;

import com.sk89q.worldedit.CuboidClipboard;

@SuppressWarnings("deprecation")
public abstract class IGame {

	public abstract int getSlots();

	public abstract void onGameEnd();

	public abstract java.util.Collection<IGamePlayer> getPlayers();

	public abstract IGameState getState();

	public abstract boolean isFull();

	public abstract int getPlayerCount();

	public abstract CuboidClipboard getSchematic();
	
	public abstract void onTick();

	public abstract void setWEUtil(IWEUtils iweUtils);

	public abstract void setBlockPlacerPlayer(BlockPlacerPlayer bpp);

	public abstract void setBuilt(boolean b);

	public abstract void setPlayerEntry(PlayerEntry pe);
	
	public abstract void onPlayerLeave(IGamePlayer gp);
	
}
