package me.islandcraft.gameapi;

import java.util.List;

import org.bukkit.Location;

public abstract class ITeamGame extends IGame{
	
	public abstract List<IGamePlayer> getTeamIPlayers(ITeam team);
	
	public abstract ITeam getPlayerTeam(IGamePlayer gp);
	
	public abstract Location[] getSpawnPlaces();

}
