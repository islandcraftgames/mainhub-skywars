package net.islandcraftgames.merchantapi;

import org.bukkit.entity.Player;

import net.islandcraftgames.merchantapi.api.MerchantTradeListener;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.InventoryMerchant;
import net.minecraft.server.v1_8_R3.ItemStack;
import net.minecraft.server.v1_8_R3.SlotMerchantResult;

public class SSlotMerchantResult
  extends SlotMerchantResult
{
  private final SMerchant merchant;
  
  public SSlotMerchantResult(EntityPlayer player, SMerchant merchant, InventoryMerchant inventory, int index, int x, int y)
  {
    super(player, merchant, inventory, index, x, y);
    this.merchant = merchant;
  }
  
  public void a(EntityHuman human, ItemStack itemStack)
  {
    this.merchant.onTrade = null;
    

    super.a(human, itemStack);
    if (this.merchant.onTrade != null)
    {
      this.merchant.onTradePlayer = ((EntityPlayer)human);
      
      this.merchant.onTrade.g();
      
      this.merchant.onTradePlayer = null;
      for (MerchantTradeListener handler : this.merchant.handlers) {
        handler.onTrade(this.merchant, this.merchant.onTrade, (Player)human.getBukkitEntity());
      }
    }
  }
}

