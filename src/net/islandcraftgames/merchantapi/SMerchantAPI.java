package net.islandcraftgames.merchantapi;

import org.bukkit.inventory.ItemStack;

import com.google.common.base.Preconditions;

import net.islandcraftgames.merchantapi.api.Merchant;
import net.islandcraftgames.merchantapi.api.MerchantAPI;
import net.islandcraftgames.merchantapi.api.MerchantOffer;

public class SMerchantAPI implements MerchantAPI {
	public Merchant newMerchant(String title) {
		Preconditions.checkNotNull(title, "title");
		return new SMerchant(title, false);
	}

	public Merchant newMerchant(String title, boolean jsonTitle) {
		Preconditions.checkNotNull(title, "title");
		return new SMerchant(title, jsonTitle);
	}

	@SuppressWarnings("deprecation")
	public MerchantOffer newOffer(ItemStack result, ItemStack item1, ItemStack item2) {
		Preconditions.checkNotNull(result, "result");
		Preconditions.checkArgument(result.getTypeId() != 0, "result may not be air");
		Preconditions.checkNotNull(item1, "first item");
		Preconditions.checkArgument(item1.getTypeId() != 0, "first item may not be air");

		return new SMerchantOffer(result.clone(), item1.clone(),
				(item2 == null) || (item2.getTypeId() == 0) ? null : item2.clone());
	}

	public MerchantOffer newOffer(ItemStack result, ItemStack item1) {
		return newOffer(result, item1, null);
	}
}
