package net.islandcraftgames.merchantapi;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;

import com.google.common.base.Optional;

import net.islandcraftgames.merchantapi.api.MerchantOffer;
import net.minecraft.server.v1_8_R3.MerchantRecipe;

@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
public class SMerchantOffer extends MerchantRecipe implements MerchantOffer {
	private final Set<SMerchant> merchants = Collections.newSetFromMap(new WeakHashMap());
	private final org.bukkit.inventory.ItemStack item1;
	private final org.bukkit.inventory.ItemStack item2;
	private final org.bukkit.inventory.ItemStack result;
	private int maxUses = -1;
	private int uses;

	public SMerchantOffer(org.bukkit.inventory.ItemStack result, org.bukkit.inventory.ItemStack item1,
			org.bukkit.inventory.ItemStack item2) {
		super(null, null, null);

		this.result = result;
		this.item1 = item1;
		this.item2 = item2;
	}

	protected void add(SMerchant merchant) {
		this.merchants.add(merchant);
	}

	protected void remove(SMerchant merchant) {
		this.merchants.remove(merchant);
	}

	public org.bukkit.inventory.ItemStack getFirstItem() {
		return this.item1.clone();
	}

	public Optional<org.bukkit.inventory.ItemStack> getSecondItem() {
		if (this.item2 == null) {
			return Optional.absent();
		}
		return Optional.of(this.item2.clone());
	}

	public org.bukkit.inventory.ItemStack getResultItem() {
		return this.result.clone();
	}

	public int getMaxUses() {
		return this.maxUses;
	}

	public void setMaxUses(int uses) {
		if (this.maxUses == uses) {
			return;
		}
		boolean locked0 = isLocked();

		this.maxUses = uses;

		boolean locked1 = isLocked();
		if (locked0 != locked1) {
			for (SMerchant merchant : this.merchants) {
				merchant.sendUpdate();
			}
		}
	}

	public void addMaxUses(int extra) {
		if ((this.maxUses >= 0) && (extra != 0)) {
			setMaxUses(this.maxUses + extra);
		}
	}

	public int getUses() {
		return this.uses;
	}

	public void setUses(int uses) {
		if (this.uses == uses) {
			return;
		}
		boolean locked0 = isLocked();

		this.uses = uses;

		boolean locked1 = isLocked();
		if (locked0 != locked1) {
			for (SMerchant merchant : this.merchants) {
				merchant.sendUpdate();
			}
		}
	}

	public void addUses(int uses) {
		if (uses != 0) {
			setUses(this.uses + uses);
		}
	}

	public boolean isLocked() {
		return (this.maxUses >= 0) && (this.uses >= this.maxUses);
	}

	public net.minecraft.server.v1_8_R3.ItemStack getBuyItem1() {
		return convertSafely(this.item1);
	}

	public net.minecraft.server.v1_8_R3.ItemStack getBuyItem2() {
		return convertSafely(this.item2);
	}

	public boolean hasSecondItem() {
		return this.item2 != null;
	}

	public net.minecraft.server.v1_8_R3.ItemStack getBuyItem3() {
		return convertSafely(this.result);
	}

	public int e() {
		return this.uses;
	}

	public int f() {
		return this.maxUses < 0 ? 2147483647 : this.maxUses;
	}

	public void g() {
		addUses(1);
	}

	public void a(int extra) {
		addMaxUses(extra);
	}

	public boolean h() {
		return isLocked();
	}

	public boolean j() {
		return false;
	}

	public SMerchantOffer clone() {
		org.bukkit.inventory.ItemStack result = this.result.clone();
		org.bukkit.inventory.ItemStack item1 = this.item1.clone();
		org.bukkit.inventory.ItemStack item2 = this.item2 != null ? this.item2.clone() : null;

		SMerchantOffer clone = new SMerchantOffer(result, item1, item2);
		clone.maxUses = this.maxUses;
		clone.uses = this.uses;

		return clone;
	}

	private static net.minecraft.server.v1_8_R3.ItemStack convertSafely(org.bukkit.inventory.ItemStack itemStack) {
		if ((itemStack == null) || (itemStack.getTypeId() == 0) || (itemStack.getAmount() == 0)) {
			return null;
		}
		return CraftItemStack.asNMSCopy(itemStack);
	}
}
