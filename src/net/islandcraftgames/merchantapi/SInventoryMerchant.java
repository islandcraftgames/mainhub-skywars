package net.islandcraftgames.merchantapi;

import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.InventoryMerchant;
import org.bukkit.entity.Player;

public class SInventoryMerchant extends InventoryMerchant {

	public final SMerchant merchant;
	private final EntityPlayer customer;
	private SMerchantSession session;

	public int currentIndex;

	public SInventoryMerchant(EntityPlayer customer, SMerchant merchant) {
		super(customer, merchant);
		this.customer = customer;
		this.merchant = merchant;
	}

	@Override
	public boolean a(EntityHuman human) {
		return this.merchant.hasCustomer((Player) human.getBukkitEntity());
	}

	@Override
	public void d(int i) {
		super.d(i);

		// Catch the current index
		this.currentIndex = i;
	}

	public void setCraftInventory(SCraftInventoryMerchant craftInventory) {
		this.session = new SMerchantSession(this.merchant, craftInventory, this.customer.getBukkitEntity());
	}
	
	@Override
	public SMerchantSession getOwner() {
		if (this.session == null) {
			throw new IllegalStateException("The session is not initialized yet.");
		}
	    return this.session;
	}
}

