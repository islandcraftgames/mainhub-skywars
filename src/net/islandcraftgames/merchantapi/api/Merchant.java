package net.islandcraftgames.merchantapi.api;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import org.bukkit.entity.Player;

public abstract interface Merchant
{
  public abstract String getTitle();
  
  public abstract boolean isTitleJson();
  
  public abstract void setTitle(String paramString, boolean paramBoolean);
  
  public abstract void setTitle(String paramString);
  
  public abstract boolean addListener(MerchantTradeListener paramMerchantTradeListener);
  
  public abstract boolean removeListener(MerchantTradeListener paramMerchantTradeListener);
  
  public abstract Collection<MerchantTradeListener> getListeners();
  
  public abstract void addOffer(MerchantOffer paramMerchantOffer);
  
  public abstract void addOffers(Iterable<MerchantOffer> paramIterable);
  
  public abstract void sortOffers(Comparator<MerchantOffer> paramComparator);
  
  public abstract void removeOffer(MerchantOffer paramMerchantOffer);
  
  public abstract void removeOffers(Iterable<MerchantOffer> paramIterable);
  
  public abstract List<MerchantOffer> getOffers();
  
  public abstract MerchantOffer getOfferAt(int paramInt);
  
  public abstract int getOffersCount();
  
  public abstract void setOfferAt(int paramInt, MerchantOffer paramMerchantOffer);
  
  public abstract void insetOfferAt(int paramInt, MerchantOffer paramMerchantOffer);
  
  public abstract boolean addCustomer(Player paramPlayer);
  
  public abstract boolean removeCustomer(Player paramPlayer);
  
  public abstract boolean hasCustomer(Player paramPlayer);
  
  public abstract Collection<Player> getCustomers();
}

