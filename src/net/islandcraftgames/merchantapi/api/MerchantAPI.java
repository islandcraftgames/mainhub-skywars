package net.islandcraftgames.merchantapi.api;

import javax.annotation.Nullable;
import org.bukkit.inventory.ItemStack;

public abstract interface MerchantAPI {
	public abstract Merchant newMerchant(String paramString);

	public abstract Merchant newMerchant(String paramString, boolean paramBoolean);

	public abstract MerchantOffer newOffer(ItemStack paramItemStack1, ItemStack paramItemStack2,
			@Nullable ItemStack paramItemStack3);

	public abstract MerchantOffer newOffer(ItemStack paramItemStack1, ItemStack paramItemStack2);
}
