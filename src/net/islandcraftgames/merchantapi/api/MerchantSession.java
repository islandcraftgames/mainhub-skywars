package net.islandcraftgames.merchantapi.api;

import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryHolder;

public interface MerchantSession extends InventoryHolder {
	
	Player getCustomer();

	Merchant getMerchant();
}
