package net.islandcraftgames.merchantapi.api;

import com.google.common.base.Optional;
import org.bukkit.inventory.ItemStack;

public abstract interface MerchantOffer
{
  public abstract ItemStack getFirstItem();
  
  public abstract Optional<ItemStack> getSecondItem();
  
  public abstract ItemStack getResultItem();
  
  public abstract int getMaxUses();
  
  public abstract void setMaxUses(int paramInt);
  
  public abstract void addMaxUses(int paramInt);
  
  public abstract int getUses();
  
  public abstract void setUses(int paramInt);
  
  public abstract void addUses(int paramInt);
  
  public abstract boolean isLocked();
  
  public abstract MerchantOffer clone();
}

