package net.islandcraftgames.merchantapi.api;

public abstract interface MerchantInventory extends org.bukkit.inventory.MerchantInventory {
	public abstract MerchantSession getSession();

	public abstract Merchant getMerchant();

	public abstract int getSelectedOfferIndex();

	public abstract MerchantOffer getSelectedOffer();
}
