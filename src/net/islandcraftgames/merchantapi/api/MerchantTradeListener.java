package net.islandcraftgames.merchantapi.api;

import org.bukkit.entity.Player;

public abstract interface MerchantTradeListener
{
  public abstract void onTrade(Merchant paramMerchant, MerchantOffer paramMerchantOffer, Player paramPlayer);
}

