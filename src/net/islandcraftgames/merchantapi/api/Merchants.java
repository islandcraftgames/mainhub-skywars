package net.islandcraftgames.merchantapi.api;

import javax.annotation.Nullable;
import org.bukkit.inventory.ItemStack;

public class Merchants {
	private static MerchantAPI instance;
	private static final String ERROR_INSTANCE_MISSING = "Merchants API is not loaded!";
	private static final String ERROR_SET_INSTANCE = "Merchants API instance can only be set once!";

	public static Merchant newMerchant(String title) {
		if (instance == null) {
			throw new IllegalStateException(ERROR_INSTANCE_MISSING);
		}
		return instance.newMerchant(title);
	}

	public static Merchant newMerchant(String title, boolean jsonTitle) {
		if (instance == null) {
			throw new IllegalStateException(ERROR_INSTANCE_MISSING);
		}
		return instance.newMerchant(title, jsonTitle);
	}

	public static MerchantOffer newOffer(ItemStack result, ItemStack item1, @Nullable ItemStack item2) {
		if (instance == null) {
			throw new IllegalStateException(ERROR_INSTANCE_MISSING);
		}
		return instance.newOffer(result, item1, item2);
	}

	public static MerchantOffer newOffer(ItemStack result, ItemStack item1) {
		if (instance == null) {
			throw new IllegalStateException(ERROR_INSTANCE_MISSING);
		}
		return instance.newOffer(result, item1);
	}

	public static MerchantAPI get() {
		return instance;
	}

	public static void set(MerchantAPI api) {
		if (instance != null) {
			throw new IllegalStateException(ERROR_SET_INSTANCE);
		}
		instance = api;
	}
}
