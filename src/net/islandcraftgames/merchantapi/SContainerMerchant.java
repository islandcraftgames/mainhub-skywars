package net.islandcraftgames.merchantapi;

import java.lang.reflect.Field;

import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftInventoryView;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.ContainerMerchant;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.InventoryMerchant;
import net.minecraft.server.v1_8_R3.Slot;

public class SContainerMerchant extends ContainerMerchant {
	private static Field fieldInventoryMerchant;
	private final SMerchant merchant;
	private CraftInventoryView bukkitEntity;

	public SContainerMerchant(EntityPlayer customer, SMerchant merchant) throws Exception {
		super(customer.inventory, merchant, customer.world);
		this.merchant = merchant;

		if (fieldInventoryMerchant == null) {
			for (Field field : ContainerMerchant.class.getDeclaredFields()) {
				field.setAccessible(true);
				if (field.getType().isAssignableFrom(InventoryMerchant.class)) {
					fieldInventoryMerchant = field;
				}
			}
		}

		final SInventoryMerchant inventory = new SInventoryMerchant(customer, merchant);

		this.setSlot(0, new Slot(inventory, 0, 36, 53));
		this.setSlot(1, new Slot(inventory, 1, 62, 53));
		this.setSlot(2, new SSlotMerchantResult(customer, merchant, inventory, 2, 120, 53));

		fieldInventoryMerchant.setAccessible(true);
		fieldInventoryMerchant.set(this, inventory);

		final SCraftInventoryMerchant craftInventory = new SCraftInventoryMerchant(inventory);
		inventory.setCraftInventory(craftInventory);

		this.bukkitEntity = new CraftInventoryView(customer.getBukkitEntity(), craftInventory, this);
	}

	void setSlot(int index, Slot slot) {
		slot.rawSlotIndex = index;

		this.c.set(index, slot);
	}

	@Override
	public CraftInventoryView getBukkitView() {
		return this.bukkitEntity;
	}

	@Override
	public boolean a(EntityHuman human) {
		return this.merchant.hasCustomer((Player) human.getBukkitEntity());
	}

	@Override
	public void b(EntityHuman human) {
		super.b(human);

		this.merchant.removeCustomer((Player) human.getBukkitEntity());
	}
}

