package net.islandcraftgames.merchantapi;

import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftInventoryMerchant;

import net.islandcraftgames.merchantapi.api.Merchant;
import net.islandcraftgames.merchantapi.api.MerchantInventory;
import net.islandcraftgames.merchantapi.api.MerchantOffer;
import net.islandcraftgames.merchantapi.api.MerchantSession;

public class SCraftInventoryMerchant extends CraftInventoryMerchant implements MerchantInventory {

	public SCraftInventoryMerchant(SInventoryMerchant merchant) {
		super(merchant);
	}

	@Override
	public Merchant getMerchant() {
		return ((SInventoryMerchant) this.inventory).merchant;
	}

	@Override
	public int getSelectedOfferIndex() {
		return ((SInventoryMerchant) this.inventory).currentIndex;
	}

	@Override
	public MerchantOffer getSelectedOffer() {
		return this.getMerchant().getOfferAt(this.getSelectedOfferIndex());
	}

	@Override
	public MerchantSession getSession() {
		return ((SInventoryMerchant) this.inventory).getOwner();
	}
}
