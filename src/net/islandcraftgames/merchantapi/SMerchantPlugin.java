package net.islandcraftgames.merchantapi;

import net.islandcraftgames.merchantapi.api.Merchants;

public class SMerchantPlugin {
	
	public SMerchantPlugin(){
		onEnable();
	}
	
	public void onEnable()
	  {
	    Merchants.set(new SMerchantAPI());
	  }

}
