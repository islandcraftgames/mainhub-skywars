package net.islandcraftgames.merchantapi;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.event.CraftEventFactory;
import org.bukkit.entity.Player;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import io.netty.buffer.Unpooled;
import net.islandcraftgames.merchantapi.api.Merchant;
import net.islandcraftgames.merchantapi.api.MerchantOffer;
import net.islandcraftgames.merchantapi.api.MerchantTradeListener;
import net.minecraft.server.v1_8_R3.Container;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IMerchant;
import net.minecraft.server.v1_8_R3.ItemStack;
import net.minecraft.server.v1_8_R3.MerchantRecipe;
import net.minecraft.server.v1_8_R3.MerchantRecipeList;
import net.minecraft.server.v1_8_R3.PacketDataSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutCustomPayload;
import net.minecraft.server.v1_8_R3.PacketPlayOutOpenWindow;

public class SMerchant implements IMerchant, Merchant {
	private final MerchantRecipeList offers = new MerchantRecipeList();
	private final Set<Player> customers = Sets.newHashSet();
	private String title;
	private boolean jsonTitle;
	private IChatBaseComponent sendTitle;
	protected final Set<MerchantTradeListener> handlers = Sets.newHashSet();
	protected SMerchantOffer onTrade;
	protected EntityPlayer onTradePlayer;

	public SMerchant(String title, boolean jsonTitle) {
		setTitle(title, jsonTitle);
	}

	public String getTitle() {
		return this.title;
	}

	public boolean isTitleJson() {
		return this.jsonTitle;
	}

	public void setTitle(String title, boolean jsonTitle) {
		Preconditions.checkNotNull(title, "title");

		IChatBaseComponent oldTitle = this.sendTitle;
		IChatBaseComponent newTitle;
		if (jsonTitle) {
			try {
				newTitle = IChatBaseComponent.ChatSerializer.a(this.title);
			} catch (Exception e) {
				throw new IllegalArgumentException("invalid json format (" + title + ")", e);
			}
		} else {
			newTitle = org.bukkit.craftbukkit.v1_8_R3.util.CraftChatMessage.fromString(this.title)[0];
		}
		this.sendTitle = newTitle;
		this.jsonTitle = jsonTitle;
		this.title = title;
		if (!this.sendTitle.equals(oldTitle)) {
			sendTitleUpdate();
		}
	}

	public void setTitle(String title) {
		setTitle(title, false);
	}

	public boolean addListener(MerchantTradeListener listener) {
		Preconditions.checkNotNull(listener, "listener");
		return this.handlers.add(listener);
	}

	public boolean removeListener(MerchantTradeListener listener) {
		Preconditions.checkNotNull(listener, "listener");
		return this.handlers.remove(listener);
	}

	public Collection<MerchantTradeListener> getListeners() {
		return Lists.newArrayList(this.handlers);
	}

	public int getOffersCount() {
		return this.offers.size();
	}

	public MerchantOffer getOfferAt(int index) {
		if ((index < 0) || (index >= this.offers.size())) {
			throw new IndexOutOfBoundsException(
					"index (" + index + ") out of bounds min (0) and max (" + this.offers.size() + ")");
		}
		return (MerchantOffer) this.offers.get(index);
	}

	public void setOfferAt(int index, MerchantOffer offer) {
		Preconditions.checkNotNull(offer, "offer");
		if ((index < 0) || (index >= this.offers.size())) {
			throw new IndexOutOfBoundsException(
					"index (" + index + ") out of bounds min (0) and max (" + this.offers.size() + ")");
		}
		SMerchantOffer old = (SMerchantOffer) this.offers.set(index, (MerchantRecipe) offer);
		old.remove(this);

		sendUpdate();
	}

	public void insetOfferAt(int index, MerchantOffer offer) {
		Preconditions.checkNotNull(offer, "offer");
		if ((index < 0) || (index >= this.offers.size())) {
			throw new IndexOutOfBoundsException(
					"index (" + index + ") out of bounds min (0) and max (" + this.offers.size() + ")");
		}
		this.offers.add(index, (MerchantRecipe) offer);
	}

	public void removeOffer(MerchantOffer offer) {
		Preconditions.checkNotNull(offer, "offer");
		if (this.offers.remove(offer)) {
			((SMerchantOffer) offer).remove(this);

			sendUpdate();
		}
	}

	public void removeOffers(Iterable<MerchantOffer> offers) {
		Preconditions.checkNotNull(offers, "offers");
		if (offers.iterator().hasNext()) {
			return;
		}
		if (this.offers.removeAll(Lists.newArrayList(offers))) {
			for (MerchantOffer offer : offers) {
				((SMerchantOffer) offer).remove(this);
			}
			sendUpdate();
		}
	}

	public void addOffer(MerchantOffer offer) {
		Preconditions.checkNotNull(offer, "offer");
		if (this.offers.contains(offer)) {
			return;
		}
		this.offers.add((MerchantRecipe) offer);

		((SMerchantOffer) offer).add(this);

		sendUpdate();
	}

	public void addOffers(Iterable<MerchantOffer> offers) {
		Preconditions.checkNotNull(offers, "offers");
		if (!offers.iterator().hasNext()) {
			return;
		}
		for (MerchantOffer offer : offers) {
			if (!this.offers.contains(offer)) {
				this.offers.add((MerchantRecipe) offer);
				((SMerchantOffer) offer).add(this);
			}
		}
		sendUpdate();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sortOffers(final Comparator<MerchantOffer> comparator) {
		Preconditions.checkNotNull(comparator, "comparator");
		if (this.offers.size() <= 1) {
			return;
		}
		Collections.sort(this.offers, new Comparator() {
			public int compareReciepes(MerchantRecipe arg0, MerchantRecipe arg1) {
				return comparator.compare((MerchantOffer) arg0, (MerchantOffer) arg1);
			}

			@Override
			public int compare(Object o1, Object o2) {
				return compareReciepes((MerchantRecipe) o1, (MerchantRecipe) o2);
			}

		});
		sendUpdate();
	}

	public List<MerchantOffer> getOffers() {
		List<MerchantOffer> offers = Lists.newArrayList();
		for (MerchantRecipe recipe : this.offers) {
			offers.add((MerchantOffer) recipe);
		}
		return offers;
	}

	public boolean addCustomer(Player player) {
		Preconditions.checkNotNull(player, "player");
		if (this.customers.add(player)) {
			EntityPlayer player0 = ((CraftPlayer) player).getHandle();
			Container container0 = null;
			try {
				container0 = new SContainerMerchant(player0, this);
				container0 = CraftEventFactory.callInventoryOpenEvent(player0, container0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (container0 == null) {
				this.customers.remove(player);
				return false;
			}
			int window = player0.nextContainerCounter();

			player0.activeContainer = container0;
			player0.activeContainer.windowId = window;
			player0.activeContainer.addSlotListener(player0);

			player0.playerConnection
					.sendPacket(new PacketPlayOutOpenWindow(window, "minecraft:villager", this.sendTitle, 0));

			PacketDataSerializer content = new PacketDataSerializer(Unpooled.buffer());
			content.writeInt(window);
			this.offers.a(content);

			player0.playerConnection.sendPacket(new PacketPlayOutCustomPayload("MC|TrList", content));

			return true;
		}
		return false;
	}

	public boolean removeCustomer(Player player) {
		Preconditions.checkNotNull(player, "player");
		if (this.customers.remove(player)) {
			player.closeInventory();
			return true;
		}
		return false;
	}

	public boolean hasCustomer(Player player) {
		Preconditions.checkNotNull(player, "player");
		return this.customers.contains(player);
	}

	public Collection<Player> getCustomers() {
		return Lists.newArrayList(this.customers);
	}

	public MerchantRecipeList getOffers(EntityHuman human) {
		return this.offers;
	}

	public IChatBaseComponent getScoreboardDisplayName() {
		return this.sendTitle;
	}

	public void a(MerchantRecipe recipe) {
		this.onTrade = ((SMerchantOffer) recipe);
	}

	public void a_(EntityHuman arg0) {
	}

	public void a_(ItemStack arg0) {
	}

	public EntityHuman v_() {
		return null;
	}

	protected void sendTitleUpdate() {
		Iterator<Player> it = this.customers.iterator();
		while (it.hasNext()) {
			EntityPlayer player0 = ((CraftPlayer) it.next()).getHandle();
			player0.playerConnection.sendPacket(new PacketPlayOutOpenWindow(player0.activeContainer.windowId,
					"minecraft:villager", this.sendTitle, 0));
			player0.updateInventory(player0.activeContainer);
		}
	}

	protected void sendUpdate() {
		if (this.customers.isEmpty()) {
			return;
		}
		if ((this.onTradePlayer != null) && (this.customers.size() <= 1)) {
			return;
		}
		PacketDataSerializer content0 = new PacketDataSerializer(Unpooled.buffer());
		this.offers.a(content0);

		Iterator<Player> it = this.customers.iterator();
		while (it.hasNext()) {
			EntityPlayer player0 = ((CraftPlayer) it.next()).getHandle();
			if (player0 != this.onTradePlayer) {
				PacketDataSerializer content1 = new PacketDataSerializer(Unpooled.buffer());
				content1.writeInt(player0.activeContainer.windowId);
				content1.writeBytes(content0);

				player0.playerConnection.sendPacket(new PacketPlayOutCustomPayload("MC|TrList", content1));
			}
		}
	}
}
